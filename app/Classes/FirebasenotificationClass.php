<?php
namespace App\Classes;
use DB;
use Auth;
class FirebasenotificationClass
{

    protected $table;
    protected $connection;

    public function __construct()
    {
        $this->table      = env('DB_LOG_TABLE', 'errorlog');
        $this->connection = env('DB_LOG_CONNECTION', env('DB_CONNECTION', 'mysql'));
    }  

    public function getErrors() {
        return false;
    }

    public function sendnotification($data) {

       $url = 'https://fcm.googleapis.com/fcm/send';

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));

        $headers = array();
        $headers[] = 'Authorization: key=AAAAZVUILMw:APA91bENjCk3AXHQfk_yZz2PCSpAa80ABJb7QU2LbolPKLCIs8cN3vyrhpb3UnthP9jx38tXQ6BJZh-BlNIntezEocYEjnM7Dn9c6UEtbsKo4AvsW9bWRkmUCotMlaS8aKL-3sCzRppJ';
        $headers[] = 'Content-Type: application/json';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close($ch);

        return $result;
    }

}
