<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
//use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Config;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;

use App\Mail\ForgetPasswordLink;
use DB;
use Mail;
use App\Models\User;
use App\Models\Order;
use App\Models\Offer;


class Admincontroller extends Controller
{
use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

   /* public function __construct()
    {
        $this->middleware('auth');
    }*/

     public function login(request $request)
    {
        return view('admin.login');
        
    }

    public function authenticatelogin(request $request)
    {
      /*  echo "<pre>";
        print_r($_POST);
         echo "<pre>";*/
        $email = $request->email;
        $password = $request->password;

        if(empty($email) || empty($password)){
           return redirect()->intended('login')->with('error','Please fill required fields.');
        }
        if (Auth::attempt(['email' => $email, 'password' => $password, 'role' => 1])) {
           return redirect()->intended('dashboard');

        } else {
            return redirect()->intended('login')->with('error','Login failed.');
        }
        
        
    }

    public function dashboard(request $request)
    {
        if(Auth::user()){
            $hostel_owner_count = User::where('is_deleted','0')->where('isactivation_complete','1')->where('role','2')->get()->count();
            $guest_users_count = User::where('is_deleted','0')->where('role','3')->get()->count();
            $orders_count = Order::where('is_order_deleted','0')->where('order_status','1')->get()->count();
            $offers_count = Offer::where('is_offer_deleted','0')->where('offer_status','1')->get()->count();
        $latest_orders = Order::where('is_order_deleted','0')->where('order_status','1')->orderBy('id','DESC')->take(10)->get();
        $latest_owners = User::where('is_deleted','0')->where('isactivation_complete','1')->where('role','2')->orderBy('id','DESC')->take(8)->get();
        $latest_users = User::where('is_deleted','0')->where('role','3')->orderBy('id','DESC')->take(8)->get();

       /*
        $user_count_bymonth = User::select(DB::raw("(COUNT(*)) as count"),DB::raw("MONTHNAME(created_at) as monthname"))
            ->whereYear('created_at', date('Y'))
            ->groupBy('monthname')
            ->get();*/
        for ($i = 0; $i <= 11; $i++) {
            $month[] = date("m", strtotime( date( 'Y-m-01' )." -$i months"));
            $year[] = date("Y", strtotime( date( 'Y-m-01' )." -$i months"));
            $months[] = date("Y-m", strtotime( date( 'Y-m-01' )." -$i months"));
            /*$countuserbymonth = User::where('is_deleted','0')->where('role','3')->whereYear('created_at',$year)->whereMonth('created_at',$month)->orderBy('id','DESC')->take(8)->get()->count();
            $countownerbymonth = User::where('is_deleted','0')->where('role','2')->whereYear('created_at',$year)->whereMonth('created_at',$month)->orderBy('id','DESC')->take(8)->get()->count();*/

        }
        
      /* echo "<pre>";
        print_r($month);
        print_r($year);
        echo "</pre>";*/

        $j=0;
        foreach($month as $mth) {
           $countuserbymonth[$year[$j].'-'.$mth] = User::where('is_deleted','0')->where('role','3')->whereYear('created_at',$year[$j])->whereMonth('created_at',$mth)->orderBy('id','DESC')->get()->count();
           $countownerbymonth[] = User::where('is_deleted','0')->where('role','2')->where('isactivation_complete','1')->whereYear('created_at',$year[$j])->whereMonth('created_at',$mth)->orderBy('id','DESC')->get()->count();
           $j++;
        }

        /*echo "<pre>";
        print_r($countuserbymonth);
        print_r($countownerbymonth);
        echo "</pre>";*/


        //die();

            $data = [
                        'owner_count'  =>$hostel_owner_count,
                        'guest_count'   => $guest_users_count,
                        'order_count' => $orders_count,
                        'offers_count' => $offers_count,
                        'latest_orders' => $latest_orders,
                        'latest_owners' => $latest_owners,
                        'latest_users' => $latest_users,
                        'countuserbymonth' => $countuserbymonth,
                        'countownerbymonth' => $countownerbymonth
                    ];
            return view('admin.dashboard')->with($data);
        } else {
            return redirect()->intended('login');
        }
        
        
    }

    public function logout()
    {   
        /*$auth_user = $this->guard()->user();
        //User::where('id', $auth_user->id)->update(['isonline' => '0']);
        //Loginhistory::where('id', $auth_user->llhid)->update(['logout_time' => Carbon::now()]);
        $logout = $this->guard()->logout();
        if()*/
            if(Auth::logout()){
                return redirect()->intended('login');
            }else{
                return redirect()->intended('login');
            }
       
    }

    public function forgetpassword(request $request)
    { 
        return view('admin.forgetpassword');

    }

    public function changepassword(request $request)
    { 
        return view('admin.changepassword');

    }

    public function passwordreset(request $request)
    {   
        $user_email = $request->email;

         if(empty($user_email)){
           return redirect()->back()->with('error','Please enter the email in above field.');
        }

        $user_data = User::WHERE('email', $user_email)->WHERE('status', '1')->WHERE('role', '1')->WHERE('is_deleted','0')->first();
        if(!empty($user_data)){
            $name = $user_data->owner_name;
            $email= $user_email;
            $token = sha1(time());

            $Input =  [];
            $Input['token'] = $token;
            $user = User::where('id', $user_data->id)->update($Input);


            $sendemail = Mail::to($email)->send(new ForgetPasswordLink($name,$email,$token));

            /*return response()->json(['status'=>true,'message'=>'Reset Password Link For Change Password Sent to Your Email!','data'=>$sendemail]); */
            return redirect()->back()->with('success','Reset Password Link For Change Password Sent to Your Email.');
           
        } else {
            return redirect()->back()->with('error','Your email is not on our data base. Please Check your email.');
        }
       
    }

    public function resetpassword(request $request)
    {   
        $user_email = $request->email;
        $token = $request->token;
        $user_data = User::WHERE('email', $user_email)->WHERE('token', $token)->WHERE('status', '1')->WHERE('role', '1')->WHERE('is_deleted','0')->first();
        if(!empty($user_data)){

            $new_pass = $request->newpass;
            $c_pass = $request->confpass;
            
            $Input =  [];
            $Input['token'] = "";
            $user = User::where('id', $user_data->id)->update($Input);
            if(!empty($new_pass)) {
                if($new_pass == $c_pass) {

                   $Input['password'] = trim(Hash::make($new_pass));
                }else {
                    return redirect()->back()->with('error','Password Mismatch');
                }

            } else{
                return redirect()->back()->with('error','Please fill required fields.');
            }
           /* $name = $user_data->owner_name;
            $email=$user_email;
            $otp = rand(1000,9999); 
            $sendemail = Mail::to($email)->send(new ForgetPassword($name,$email,$otp));

            $Input =  [];
            $Input['otp'] = $otp;*/
            $Input['updated_at'] = date('Y-m-d h:i:s');
            $update = $user_data->update($Input);
            if($user_data) {
                return redirect()->back()->with('success','Your Password has been changed Successfully.');
            }else{
                return redirect()->back()->with('error','Something went wrong! please check.');
            }

           
        } else {
            return redirect()->intended('forgetpassword')->with('error','Your Token has been expired.Please sent email to change your password.');
        }
       
    }

    public function profiledetails(Request $request)
    {   
        /*$auth_user = $this->guard()->user();
        //User::where('id', $auth_user->id)->update(['isonline' => '0']);
        //Loginhistory::where('id', $auth_user->llhid)->update(['logout_time' => Carbon::now()]);
        $logout = $this->guard()->logout();
        if()*/
            if(Auth::user()){
                return view('admin.profile');
                
            }else{
                return redirect()->intended('login');
            }
       
    }

    public function updateuserdetails(Request $request)
    {   
        $id = $request->user_id;
        $new_pass = $request->newpass;
        $c_pass = $request->confpass;

        if($request->has('oldpass')) {
        $user = User::WHERE('id', $id)->WHERE('status', '1')->WHERE('is_deleted','0')->first();
            if (!Hash::check($request->oldpass, $user->password)) {
                //return response()->json(['status'=>false,'message'=>'The Old password does not match.']);
                return redirect()->back()->with('error','The Old password does not match.');
            } else if($request->oldpass == $request->newpass && strlen($request->oldpass) == strlen($request->newpass)) {
              //return response()->json(['status'=>false,'message'=>'The New password match with old password']);
              return redirect()->back()->with('error','The New password match with old password.');
            }
        }
        
        $Input =  [];
        if(!empty($new_pass)) {
            if($new_pass == $c_pass) {

               $Input['password'] = trim(Hash::make($new_pass));
            }else {
                return redirect()->back()->with('error','Password Mismatch');
            }

        }
        
        $Input['owner_name'] = trim($request->owner_name);
        $Input['email'] = trim($request->email);
        //$Input['phone_code'] = trim($request->phone_code);
        $Input['phone_number'] = trim($request->phone_number);
        $Input['address'] = trim($request->address);
        $Input['city'] = trim($request->city);
        $Input['state'] = trim($request->state);
        $Input['zipcode'] = trim($request->zipcode);
        $Input['gender'] = trim($request->gender);
        $Input['dob'] = trim($request->dob);
        $Input['hostel_name'] = trim($request->hostel_name);
        $Input['hostel_website'] = trim($request->hostel_website);
        $Input['hostel_location'] = trim($request->hostel_location);
        $Input['number_of_beds'] = trim($request->number_of_beds);
        $Input['updated_at'] = date('Y-m-d h:i:s');

        if($request->hasfile('user_img')) {
             $file = $request->file('user_img');
            $filename = $request->file('user_img')->getClientOriginalName();
       
       //Move Uploaded File
           $destinationPath = 'uploads/images';
            if(!file_exists( 'uploads/images/'.$filename )) {
              $file->move($destinationPath,$file->getClientOriginalName());
            }
           //$file->move($destinationPath,$file->getClientOriginalName());
           $Input['user_img'] = $filename;
        }


       $user_data = User::where('id', $id)->update($Input);

       if($user_data) {
           return redirect()->back()->with('success','Profile Updated Succssfully.');
       } else{
           return redirect()->back()->with('error','Something went wrong! please check.');
       }
       
    }

    /* public function guard()
    {
        return Auth::guard();
    }*/


}