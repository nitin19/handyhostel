<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
//use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Config;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;

use DB;
use App\Models\Cart;
use App\Models\Product;

class Cartcontroller extends Controller
{
    public function addproductincart(request $request)
    {
        
        
      $cart_product_id = trim($request->cart_product_id);
      $cart_user_id = trim($request->cart_user_id);
      $cart_quantity = trim($request->cart_quantity);
        
      $cart_d = Cart::where('cart_product_id',$cart_product_id)->where('cart_user_id',$cart_user_id)->first();
      if($cart_d){
        return response()->json(['status'=>false,'message'=>'Product Already Added in Cart!','error'=>'','data'=>'']);
      }


        $Input =  [];
        $Input['cart_product_id'] = trim($cart_product_id);
        $Input['cart_user_id'] = trim($cart_user_id);
        $Input['cart_quantity'] = trim($cart_quantity);
        $Input['created_at'] = date('Y-m-d h:i:s');

       $cart_data = Cart::create($Input);
       if($cart_data) {
        	//$from_email = env('MAIL_FROM_ADDRESS');
            //$email = $request->email;

        	return response()->json(['status'=>true,'message'=>'Product Added In Cart','data'=>$cart_data]);
        } else {
        	return response()->json(['status'=>false,'message'=>'Product Not Added! Something Went wrong','error'=>'','data'=>'']);
        } 
    }

    public function showallcartproductsbyid(request $request)
    {
      $user_id = $request->user_id;
        
        $cartproducts = DB::table('cart')->select('cart.*','product.product_name','product.product_cat','product.product_desc','product.product_img','product.price_to_sale','product.product_cost','product.tax_rate','product.product_unit','product.initial_stock')->join('product','product.id','=','cart.cart_product_id')->where('cart.cart_user_id',$user_id)->get();

        if($cartproducts) {
            //$from_email = env('MAIL_FROM_ADDRESS');
            //$email = $request->email;

            return response()->json(['status'=>true,'message'=>'All cart Products','data'=>$cartproducts]);
        } 
        
      /* $cartproducts = Cart::orderBy('id', 'DESC')->get();
        if($cartproducts) {
            //$from_email = env('MAIL_FROM_ADDRESS');
            //$email = $request->email;

            return response()->json(['status'=>true,'message'=>'All cart Products','data'=>$cartproducts]);
        } */
    }

    public function removeproductincart(request $request)
    {
      $id = $request->cart_id;
        
      $isproduct = Cart::find($id);
      if($isproduct) {

        $isproduct->delete();
        return response()->json(['status'=>true,'message'=>'Product from Cart Deleted Successfully']);
        
      } else {
        return response()->json(['status'=>false,'message'=>'Product Already Deleted']);
      }
    }

    public function incrementcartquantity(Request $request)
    {
      $cart_quantity = $request->cart_quantity;
      $cart_id = $request->cart_id;

      $iscartproduct = Cart::find($cart_id);

      if($iscartproduct) {

        $Input =  [];
        $Input['cart_quantity'] = trim($cart_quantity);
        $Input['updated_at'] = date('Y-m-d h:i:s');

        $cart_data = Cart::where('id', $cart_id)->update($Input);

        $cart_d = Cart::where('id', $cart_id)->first();

        
        return response()->json(['status'=>true,'message'=>'Quantity Value Updated','data'=>$cart_d]);
        
      } else {
        return response()->json(['status'=>false,'message'=>'Cart Product Not Found!']);
      }

    }

    public function decrementcartquantity(Request $request)
    {
      $cart_quantity = $request->cart_quantity;
      $cart_id = $request->cart_id;

      $iscartproduct = Cart::find($cart_id);

      if($iscartproduct) {

        $Input =  [];
        $Input['cart_quantity'] = trim($cart_quantity);
        $Input['updated_at'] = date('Y-m-d h:i:s');

        $cart_data = Cart::where('id', $cart_id)->update($Input);

        $cart_d = Cart::where('id', $cart_id)->first();

        
        return response()->json(['status'=>true,'message'=>'Quantity Value Updated','data'=>$cart_d]);
        
      } else {
        return response()->json(['status'=>false,'message'=>'Cart Product Not Found!']);
      }

    }
   
}