<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Config;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;

use DB;
use App\Models\Category;
use App\Models\User;
use App\Models\Product;
use App\Models\Notification;
use App\Classes\FirebasenotificationClass;

class Categorycontroller extends Controller
{
    public function addcategory(request $request)
    {

        $rules = [
            // 'username' => 'unique:users',
            'category_name' => 'required|unique:category',
    
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) 
        { 
            return response()->json(['status'=>false,'message'=>'Request Failed','error'=>$validator->errors(),'data'=>'']);
        }

        $Input =  [];
        $Input['category_name'] = trim($request->category_name);
        $Input['category_status'] = "1";
        $Input['is_deleted'] = "0";
        $Input['created_at'] = date('Y-m-d h:i:s');

        $file = $request->file('category_img');

        $filename = $request->file('category_img')->getClientOriginalName();
       // Get the contents of the file
       //$contents = $file->openFile()->fread($file->getSize());

       // Store the contents to the database
        $Input['category_img'] = $filename;

        

       //Move Uploaded File
        $destinationPath = 'uploads/images';
        if(!file_exists( 'uploads/images/'.$filename )) {
          $file->move($destinationPath,$file->getClientOriginalName());
        }
       //$file->move($destinationPath,$file->getClientOriginalName());

       $category_data = Category::create($Input);
       if($category_data) {
        	//$from_email = env('MAIL_FROM_ADDRESS');
            //$email = $request->email;

        $users = User::where('status', '1')->where('is_deleted','0')->where('role','=','2')->get();

          foreach($users as $userdt) {

              $notify =  [];
              $notify['notification_type'] = "category_created";
              $notify['notification_title'] = "category created";
              $notify['to_user'] = $userdt->id;
              $notify['from_user'] = '1';
              $notify['table_id'] = $category_data->id;
              $notify['icon'] = "Add_category.png";
              $notify['message'] = "New Category Has been Added!";
              $notify['is_read'] = "0";

              $ndata = Notification::create($notify);

              $to_token = $userdt->device_token;

              $title = 'New Category Created';
              $type = $ndata->notification_type;
              $message = $ndata->message;


              $notification = array('title' =>$title , 'text' => $message);

              $fields = array(
                  'to' => $to_token,
                  'data' => array(
                      "type" => $type,
                      "title" => $title,
                      "message" => $message,
                      "notification_id" => $ndata->id,
                      "product_id" => $product_data->id
                  ),
                  'notification' => $notification,
                  'priority' => 'high',
              );
               

              $notificationClass = new FirebasenotificationClass();
              $notification_response = $notificationClass->sendnotification($fields);


          }


        	return response()->json(['status'=>true,'message'=>'Category Added Successfully','data'=>$category_data]);
        } else {
        	return response()->json(['status'=>false,'message'=>'Category Not Added! Something Went wrong','error'=>$validator->errors(),'data'=>'']);
        } 
    }

    public function showcategories(request $request)
    {
        //$categories = Category::all();
        $categories = Category::where('category_status','1')->where('is_deleted','0')->orderBy('category_name')->get();
        if($categories) {
            //$from_email = env('MAIL_FROM_ADDRESS');
            //$email = $request->email;

            return response()->json(['status'=>true,'message'=>'All Categories','data'=>$categories]);
        } 
    }

    public function getcategorieswithproductcount(request $request)
    {
        //$categories = Category::all();
      $role = $request->role;
      $user_id = $request->user_id;
      /*if($role == "1") {
          $products = Product::where('product_cat',$id)->where('is_deleted','0')->orderBy('id', 'DESC')->get()->count();
      }else if($role == "2") {
          $products = Product::where('product_cat',$id)->where('user_id',$user_id)->where('is_deleted','0')->orderBy('id', 'DESC')->get()->count();
      }else if($role == "3") {
          $user = DB::table('user')->select('user.*')->where('id',$user_id)->get()->first();
          
          $products = Product::where('product_cat',$id)->where('user_id',$user->invited_from)->where('product_status','1')->where('is_deleted','0')->orderBy('id', 'DESC')->get()->count();
      }*/


        $categories = Category::where('category_status','1')->where('is_deleted','0')->orderBy('category_name')->get();
        $catarr = array();
        foreach($categories as $category) {
          $id = $category->id;
          if($role == "1") {
              $products_count = Product::where('product_cat',$id)->where('is_deleted','0')->orderBy('id', 'DESC')->get()->count();
              $category['product_count'] = $products_count;
          }else if($role == "2") {
              $products_count = Product::where('product_cat',$id)->where('user_id',$user_id)->where('is_deleted','0')->orderBy('id', 'DESC')->get()->count();
              $category['product_count'] = $products_count;
          }else if($role == "3") {
              $user = DB::table('user')->select('user.*')->where('id',$user_id)->get()->first();
              
              $products_count = Product::where('product_cat',$id)->where('user_id',$user->invited_from)->where('product_status','1')->where('is_deleted','0')->orderBy('id', 'DESC')->get()->count();
              $category['product_count'] = $products_count;
          }
          array_push($catarr,$category);
        }
        
        if($catarr) {
            //$from_email = env('MAIL_FROM_ADDRESS');
            //$email = $request->email;

            return response()->json(['status'=>true,'message'=>'All Categories','data'=>$catarr]);
        } 
    }

    public function getcategoryDetails(Request $request, $id) {
    
      $iscategory = Category::find($id);
      
      if($iscategory) {
        //$product = Product::where('id',$id)->where('product_status','1')->where('is_deleted','0')->get();
        return response()->json(['status'=>true,'message'=>'category details','error'=>'','data'=>$iscategory], 200);
      } else {
        return response()->json(['status'=>false,'message'=>'category not found','error'=>'','data'=>''], 400);
      }
    }

     public function updatecategoryDetails(Request $request, $id) {
    
      $iscategory = Category::find($id);

        $rules = [
            // 'username' => 'unique:users',
            'category_name' => 'required',
    
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) 
        { 
            return response()->json(['status'=>false,'message'=>'Request Failed','error'=>$validator->errors(),'data'=>'']);
        }

        $Input =  [];
        $Input['category_name'] = trim($request->category_name);
        $Input['category_status'] = "1";
        $Input['is_deleted'] = "0";
        $Input['updated_at'] = date('Y-m-d h:i:s');
        
         if ($request->has('category_img')){
             $file = $request->file('category_img');

            $filename = $request->file('category_img')->getClientOriginalName();
           // Get the contents of the file
           //$contents = $file->openFile()->fread($file->getSize());

           // Store the contents to the database
            $Input['category_img'] = $filename;

            

           //Move Uploaded File
           $destinationPath = 'uploads/images';
          if(!file_exists( 'uploads/images/'.$filename )) {
            $file->move($destinationPath,$file->getClientOriginalName());
          }
          //$file->move($destinationPath,$file->getClientOriginalName());
         }
        


       $cat_data = Category::where('id', $id)->update($Input);
       $cat_details = Category::where('id', $id)->first()->toArray();
       if($cat_details) {
            //$from_email = env('MAIL_FROM_ADDRESS');
            //$email = $request->email;

        return response()->json(['status'=>true,'message'=>'Category Updated Successfully','data'=>$cat_details]);
        } else {
        return response()->json(['status'=>false,'message'=>'Category Not Updated! Something Went wrong','error'=>$validator->errors(),'data'=>'']);
        } 
      
      
    }

    public function deletecategory(request $request,$id)
    {
        $iscategory = Category::find($id);
        if($iscategory) {

        $category = Category::where('id',$id)->where('category_status','1')->where('is_deleted','0')->get();
        if($category) {
            $Input =  [];
            $Input['is_deleted'] = "1";

           $cat_data = Category::where('id', $id)->update($Input);
           $cat_details = Category::where('id', $id)->first()->toArray();
           if($cat_details) {
                //$from_email = env('MAIL_FROM_ADDRESS');
                //$email = $request->email;

            return response()->json(['status'=>true,'message'=>'Category Deleted Successfully','data'=>$cat_details]);
            } else {
            return response()->json(['status'=>false,'message'=>'Category Not Deleted! Something Went wrong','error'=>$validator->errors(),'data'=>'']);
            } 

        
        } 
        
      } else {
        return response()->json(['status'=>false,'message'=>'Category not found','error'=>'','data'=>''], 400);
      }
        
    }

    public function searchcategory(request $request)
    {
        $srch_category = trim($request->category_name);
       
        $category_details = Category::where('category_name','LIKE','%'.$srch_category.'%')->where('category_status','1')->where('is_deleted','0')->orderBy('id', 'DESC')->get();
         if($category_details) {
            return response()->json(['status'=>true,'message'=>'Search Categories','data'=>$category_details]);
        } else {
             return response()->json(['status'=>false,'message'=>'No Category found!','data'=>'']);
        }
      
        
    }

   
}