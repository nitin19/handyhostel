<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Config;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;

use Illuminate\Support\Str;
use App\Mail\SendInvitation;
use App\Mail\AcceptInvitation;
use DB;
use Mail;
use App\Models\Invite;
use App\Models\User;
use App\Models\Notification;
use App\Models\Emailtemplate;

class Invitecontroller extends Controller
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /* public function __construct()
    {
        $this->middleware('auth');
    }*/

     
    public function inviteuser(request $request)
    {   
       $email = $request->email;

        $rules = [
            // 'username' => 'unique:users',
            'email' => 'required|unique:user|email',
        ];
 
        $messages = [
            // 'username.unique'   =>  'That user name is already in use.',
            'email.unique'   =>  'That email address is already in use.',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) 
        { 
            return response()->json(['status'=>false,'message'=>'Email address already in use','error'=>$validator->errors(),'data'=>'']);
        }
        
        if(empty($request->hostel_name)){
             return response()->json(['status'=>false,'message'=>'Hostel Name Is Required! Please Complete Your Profile.','error'=>'','data'=>'']);
        }

          
        $password = "user@123";
        $Input =  [];
        $Input['email'] = trim($email);
        $Input['password'] = trim(Hash::make($password));
        $Input['hostel_name'] = $request->hostel_name;
        $Input['invited_from'] = trim($request->user_id);
        $Input['is_invited'] = "1";
        $Input['isactivation_complete'] = "1";
        $Input['token'] = sha1(time());
        $Input['device_token'] = "";
        $Input['owner_name'] = "";
        $Input['phone_code'] = "";
        $Input['phone_number'] = "";
        $Input['address'] = "";
        $Input['city'] = "";
        $Input['state'] = "";
        $Input['zipcode'] = "";
        $Input['gender'] = "";
        $Input['dob'] = "";
        $Input['otp'] = "";
        $Input['user_img'] = "";
        $Input['hostel_website'] = "";
        $Input['hostel_location'] = "";
        $Input['number_of_beds'] = "";
        $Input['bed_number'] = "";
        $Input['active_code'] = "";
        $Input['status'] = '1';
        $Input['is_deleted'] = '0';
        $Input['role'] = "3";
         $Input['created_at'] = date('Y-m-d h:i:s');
          
        $user = User::create($Input);
       
         if($user) {

         /* $notify =  [];
          $notify['notification_type'] = "registered";
          $notify['to_user'] = trim($user->invited_from);
          $notify['from_user'] = $user->id;
          $notify['message'] = "";
          $notify['is_read'] = "0";

        $data = Notification::create($notify);*/
        $email_template = Emailtemplate::where('id','1')->first();
        $email_text = $email_template->email_text;

         	$sendemail = Mail::to($email)->send(new AcceptInvitation($email,$password,$email_text));
         	return response()->json(['status'=>true,'message'=>'Mail is Sent to Invite User! Thanks.','error'=>'','data'=>$user,'mailsend'=>$sendemail]);
         }else {

            return response()->json(['status'=>false,'message'=>'Invitation Failed! Something Went wrong','error'=>$validator->errors(),'data'=>'']);
         }
  

      

        /*$rules = [
            // 'username' => 'unique:users',
            'email' => 'required|unique:user|email',
        ];
 
        $messages = [
            // 'username.unique'   =>  'That user name is already in use.',
            'email.unique'   =>  'That email address is already in use.',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) 
        { 
            return response()->json(['status'=>false,'message'=>'Email address already in use','error'=>$validator->errors(),'data'=>'']);
        }

        $Input =  [];
        $Input['email'] = trim($request->email);
        $Input['token'] = strtolower(Str::random(15));
        $Input['from_user'] = trim($request->user_id);
        $Input['hostel_name'] = trim($request->hostel_name);
        
        $invited_user = Invite::create($Input);

         if($invited_user) { 
            $email = trim($request->email);
            $token = $invited_user['token'];
            $sendemail = Mail::to($email)->send(new SendInvitation($email,$token));

            return response()->json(['status'=>true,'message'=>'Mail is Sent to Invite User! Thanks.','error'=>'','data'=>$invited_user,'mailsend'=>$sendemail]);
         }else {

            return response()->json(['status'=>false,'message'=>'Invitation Failed! Something Went wrong','error'=>$validator->errors(),'data'=>'']);
         }*/
    }

    public function acceptinvitation(request $request,$token)
    {
      $email = $request->email;

      $rules = [
            // 'username' => 'unique:users',
            'email' => 'required|unique:user|email',
        ];
 
        $messages = [
            // 'username.unique'   =>  'That user name is already in use.',
            'email.unique'   =>  'That email Invitation is already Accepted.',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) 
        { 
            return response()->json(['status'=>false,'message'=>'That email Invitation is already Accepted.','error'=>$validator->errors(),'data'=>'']);
        }

      $invited_user = Invite::where('email', $email)->where('token', $token)->first();
      if($invited_user) {
          
        $password = "user@123";
        $Input =  [];
        $Input['email'] = trim($email);
        $Input['password'] = trim(Hash::make($password));
        $Input['hostel_name'] = $invited_user->hostel_name;
        $Input['invited_from'] = $invited_user->from_user;
        $Input['is_invited'] = "1";
        $Input['isactivation_complete'] = "1";
        $Input['token'] = sha1(time());
        $Input['device_token'] = "";
        $Input['owner_name'] = "";
        $Input['phone_code'] = "";
        $Input['phone_number'] = "";
        $Input['address'] = "";
        $Input['city'] = "";
        $Input['state'] = "";
        $Input['zipcode'] = "";
        $Input['gender'] = "";
        $Input['dob'] = "";
        $Input['otp'] = "";
        $Input['user_img'] = "";
        $Input['hostel_website'] = "";
        $Input['hostel_location'] = "";
        $Input['number_of_beds'] = "";
        $Input['active_code'] = "";
        $Input['status'] = '1';
        $Input['is_deleted'] = '0';
        $Input['role'] = "3";
         $Input['created_at'] = date('Y-m-d h:i:s');
          
        $user = User::create($Input);
        

        $Invite =  [];
        $Invite['token'] = ' ';
        $update = $invited_user->update($Invite);
           
        $sendemail = Mail::to($email)->send(new AcceptInvitation($email,$password));

        /*$data = array(
            "email" => $email,
            "password" => $password,

        );*/

        //return view('invitation.accept')->with($data);
        return response()->json(['status'=>true,'message'=>'Invitation Accepted! Now You Are Registered Successfully. Please Check your Email for Login Details.']);

      } else {
        return response()->json(['status'=>false,'message'=>'Invitation Failed! Something Went wrong','data'=>'']);
      }

    }
        
}
