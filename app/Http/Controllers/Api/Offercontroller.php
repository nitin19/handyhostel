<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Config;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;


use App\Models\Offer;
use App\Models\User;
use App\Models\Product;
use App\Models\Notification;
use App\Classes\FirebasenotificationClass;
use DB;

class Offercontroller extends Controller
{
    public function addoffer(request $request)
    {

        $rules = [
            // 'username' => 'unique:users',
            'offer_name' => 'required|unique:offer',
            'offer_desc' => 'required',
            'offer_price' => 'required',
            'offer_start_time' => 'required',
            'offer_end_time' => 'required',
            'offer_start_date' => 'required',
            'offer_end_date' => 'required',
    
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) 
        { 
            return response()->json(['status'=>false,'message'=>'Request Failed','error'=>$validator->errors(),'data'=>'']);
        }

        $Input =  [];
        $Input['offer_name'] = trim($request->offer_name);
        $Input['offer_desc'] = trim($request->offer_desc);
        $Input['offer_user_id'] = trim($request->offer_user_id);
        $Input['offer_product_id'] = trim($request->offer_product_id);
        $Input['offer_price'] = trim($request->offer_price);
        $Input['offer_status'] = "1";
        $Input['offer_start_date'] = trim($request->offer_start_date);
        $Input['offer_end_date'] = trim($request->offer_end_date);
      //  $Input['offer_start_time'] = trim($request->offer_start_time);
      //  $Input['offer_end_time'] = trim($request->offer_end_time);

        $offer_start_time = trim($request->offer_start_time);
        $stFirst2Char = substr($offer_start_time, 0, 2);
        $stLast2Char = substr($offer_start_time, -2);
        if($stFirst2Char == '00') {
            $offer_start_time_Arr = explode(':', $offer_start_time);
            $stStr1 = $offer_start_time_Arr[1];
            $offer_start_time_new = '12:'.$stStr1;
            $Input['offer_start_time'] = $offer_start_time_new;
        } else {
            $Input['offer_start_time'] = trim($request->offer_start_time);
        }

        $offer_end_time = trim($request->offer_end_time);
        $etFirst2Char = substr($offer_end_time, 0, 2);
        $etLast2Char = substr($offer_end_time, -2);
        if($etFirst2Char == '00') {
            $offer_end_time_Arr = explode(':', $offer_end_time);
            $etStr1 = $offer_end_time_Arr[1];
            $offer_end_time_new = '12:'.$etStr1;
            $Input['offer_end_time'] = $offer_end_time_new;
        } else {
            $Input['offer_end_time'] = trim($request->offer_end_time);
        }

        $Input['is_offer_deleted'] = "0";
        $Input['created_at'] = date('Y-m-d h:i:s');

        
        if($request->hasfile('offer_img')) {
          $file = $request->file('offer_img');
        $filename = $request->file('offer_img')->getClientOriginalName();
       // Get the contents of the file
       //$contents = $file->openFile()->fread($file->getSize());

       // Store the contents to the database
        $Input['offer_img'] = $filename;

        
   
       //Move Uploaded File
       $destinationPath = 'uploads/images';
       if(!file_exists( 'uploads/images/'.$filename )) {
          $file->move($destinationPath,$file->getClientOriginalName());
        }
       //$file->move($destinationPath,$file->getClientOriginalName());
        }else {
          $prod_id = trim($request->offer_product_id);
          $product = Product::where('id',$prod_id)->get()->first();
          $Input['offer_img'] = $product->product_img;
        } 

       $offer_data = Offer::create($Input);
       if($offer_data) {
        	//$from_email = env('MAIL_FROM_ADDRESS');
            //$email = $request->email;
          $userdata = User::where('id', $offer_data->offer_user_id)->first();
          if($userdata->role == '1'){
            $users = User::where('status', '1')->where('is_deleted','0')->where('role','!=','1')->get();

            foreach($users as $userdt) {

              $notify =  [];
              $notify['notification_type'] = "offer_created";
              $notify['notification_title'] = "offer created";
              $notify['to_user'] = $userdt->id;
              $notify['from_user'] = '1';
              $notify['table_id'] = $offer_data->id;
              $notify['icon'] = "offers.png";
              $notify['message'] = "New Offer Has been Added!";
              $notify['is_read'] = "0";

              $ndata = Notification::create($notify);

              $to_token = $userdt->device_token;

              $title = 'New Offer Created';
              $type = $ndata->notification_type;
              $message = $ndata->message;


              $notification = array('title' =>$title , 'text' => $message);

              $fields = array(
                  'to' => $to_token,
                  'data' => array(
                      "type" => $type,
                      "title" => $title,
                      "message" => $message,
                      "notification_id" => $ndata->id,
                      "offer_id" => $offer_data->id
                  ),
                  'notification' => $notification,
                  'priority' => 'high',
              );
               

              $notificationClass = new FirebasenotificationClass();
              $notification_response = $notificationClass->sendnotification($fields);


            }

          } else if($userdata->role == '2'){
            $users = User::where('invited_from',$userdata->id)->where('status', '1')->where('is_deleted','0')->where('role','=','3')->get();

            foreach($users as $userdt) {

              $notify =  [];
              $notify['notification_type'] = "offer_created";
              $notify['notification_title'] = "offer created";
              $notify['to_user'] = $userdt->id;
              $notify['from_user'] = $userdata->id;
              $notify['table_id'] = $offer_data->id;
              $notify['icon'] = "offers.png";
              $notify['message'] = "New Offer Has been Added!";
              $notify['is_read'] = "0";

              $ndata = Notification::create($notify);

              $to_token = $userdt->device_token;

              $title = 'New Offer Created';
              $type = $ndata->notification_type;
              $message = $ndata->message;


              $notification = array('title' =>$title , 'text' => $message);

              $fields = array(
                  'to' => $to_token,
                  'data' => array(
                      "type" => $type,
                      "title" => $title,
                      "message" => $message,
                      "notification_id" => $ndata->id,
                      "offer_id" => $offer_data->id
                  ),
                  'notification' => $notification,
                  'priority' => 'high',
              );
               

              $notificationClass = new FirebasenotificationClass();
              $notification_response = $notificationClass->sendnotification($fields);


            }



          }

            /*$notify =  [];
            $notify['notification_type'] = "Offer created";
            $notify['to_user'] = $offer_data->offer_user_id;
            $notify['from_user'] = '1';
            $notify['message'] = "Your Order has been Cancelled";
            $notify['is_read'] = "0";

            $ndata = Notification::create($notify);

            $touserdata = User::where('id', $ndata->to_user)->first();

            $to_token = $touserdata->device_token;

            $title = 'Order Cancelled';
            $type = $ndata->notification_type;
            $message = $ndata->message;


            $notification = array('title' =>$title , 'text' => $message);

            $fields = array(
                'to' => $to_token,
                'data' => array(
                    "type" => $type,
                    "title" => $title,
                    "message" => $message,
                    "notification_id" => $ndata->id,
                    "prder_id" => $order_detail->id
                ),
                'notification' => $notification,
                'priority' => 'high',
            );
             

            $notificationClass = new FirebasenotificationClass();
            $notification_response = $notificationClass->sendnotification($fields);*/

        	return response()->json(['status'=>true,'message'=>'Offer Added Successfully','data'=>$offer_data]);
        } else {
        	return response()->json(['status'=>false,'message'=>'Offer Not Added! Something Went wrong','error'=>$validator->errors(),'data'=>'']);
        } 
    }

    public function showoffers(request $request)
    {
        $role = $request->role;
        $user_id = $request->user_id;

        if($role == "1") {
          $offers = Offer::where('offer_status','1')->where('is_offer_deleted','0')->orderBy('id', 'DESC')->get();
          if($offers) {
              return response()->json(['status'=>true,'message'=>'All Offers','data'=>$offers]);
          } else {
              return response()->json(['status'=>false,'message'=>'Offers Not Found.','data'=>'']);
          }
        } else  if($role == "2"){
          $offers = Offer::where('offer_user_id',$user_id)->where('offer_status','1')->where('is_offer_deleted','0')->orderBy('id', 'DESC')->get();

          $currnt_date = date('Y-m-d');
          $offerarr = array();
          foreach($offers as $offer) {
            
                $start_time = date("H:i", strtotime($offer->offer_start_time));
                $end_time = date("H:i", strtotime($offer->offer_end_time));

                $end_date_time = $offer->offer_end_date." ".$end_time;
                $start_date_time = $offer->offer_start_date." ".$start_time;
                $offer_current_date_time = date('Y-m-d H:i');
                $offer_end_date_time = date($end_date_time);
                $offer_start_date_time = date($start_date_time);
                
                $time1 = date_create($offer_current_date_time); 
                $time2 = date_create($offer_end_date_time);
                $time3 = date_create($offer_start_date_time); 
                $timeinterval = date_diff($time1, $time2);
                $starttimeinterval = date_diff($time1, $time3);

                $time_left = $timeinterval->format("%H:%i");
                $start_time_left = $starttimeinterval->format("%H:%i"); 

                $datetime1 = date_create($currnt_date); 
                $datetime2 = date_create($offer->offer_end_date);
                $datetime3 = date_create($offer->offer_start_date); 
                $interval = date_diff($datetime1, $datetime2);
                $start_interval = date_diff($datetime1, $datetime3); 
                $days_left = $interval->format("%R%a");


                $start_days_left = $start_interval->format("%R%a");

                if($start_days_left>=0) {
                  $start_offer_days = trim($start_days_left,'+');
                  $start_offer_time = $start_time_left;
                }else {
                  $start_offer_days = '0';
                  $start_offer_time = '0';
                }

                if($days_left>=0) {
                  $offer['start_days_left'] = $start_offer_days;
                  $offer['start_time_left'] = $start_offer_time;
                  $offer['days_left'] = trim($days_left,'+');
                  $offer['time_left'] = $time_left;
                  array_push($offerarr,$offer);
                }


                
            //if($currnt_date >= $offer->offer_start_date && $currnt_date <= $offer->offer_end_date){
             // $c_time = date('H:i');
              //if($c_time >= $offer->offer_start_time && $c_time <= $offer->offer_end_time) {
               // array_push($offerarr,$offer);
             // }


                
             // } 
          }
        if($offerarr) {
            //$from_email = env('MAIL_FROM_ADDRESS');
            //$email = $request->email;

            return response()->json(['status'=>true,'message'=>'All Offers','data'=>$offerarr]);
        } 

        }
        else if($role == "3") {
          $user = DB::table('user')->select('user.*')->where('id',$user_id)->get()->first();

          $offers = Offer::where('offer_user_id',$user->invited_from)->where('offer_status','1')->where('is_offer_deleted','0')->orderBy('id', 'DESC')->get();

          $currnt_date = date('Y-m-d');
          $offerarr = array();
          foreach($offers as $offer) {
                $start_time = date("H:i", strtotime($offer->offer_start_time));
                $end_time = date("H:i", strtotime($offer->offer_end_time));

                $end_date_time = $offer->offer_end_date." ".$end_time;
                $start_date_time = $offer->offer_start_date." ".$start_time;
                $offer_current_date_time = date('Y-m-d H:i');
                $offer_end_date_time = date($end_date_time);
                $offer_start_date_time = date($start_date_time);
                
                $time1 = date_create($offer_current_date_time); 
                $time2 = date_create($offer_end_date_time);
                $time3 = date_create($offer_start_date_time); 
                $timeinterval = date_diff($time1, $time2);
                $starttimeinterval = date_diff($time1, $time3);

                $time_left = $timeinterval->format("%H:%i");
                $start_time_left = $starttimeinterval->format("%H:%i"); 

                $datetime1 = date_create($currnt_date); 
                $datetime2 = date_create($offer->offer_end_date);
                $datetime3 = date_create($offer->offer_start_date); 
                $interval = date_diff($datetime1, $datetime2);
                $start_interval = date_diff($datetime1, $datetime3); 
                $days_left = $interval->format("%R%a");


                $start_days_left = $start_interval->format("%R%a");

                if($start_days_left>=0) {
                  $start_offer_days = trim($start_days_left,'+');
                  $start_offer_time = $start_time_left;
                }else {
                  $start_offer_days = '0';
                  $start_offer_time = '0';
                }

                if($days_left>=0) {
                  $offer['start_days_left'] = $start_offer_days;
                  $offer['start_time_left'] = $start_offer_time;
                  $offer['days_left'] = trim($days_left,'+');
                  $offer['time_left'] = $time_left;
                  array_push($offerarr,$offer);
                }
               
           // } 
                
            //if($currnt_date >= $offer->offer_start_date && $currnt_date <= $offer->offer_end_date){
             // $c_time = date('H:i');
              //if($c_time >= $offer->offer_start_time && $c_time <= $offer->offer_end_time) {
                
             // }


                
             // } 
          }

          if($offerarr) {
              //$from_email = env('MAIL_FROM_ADDRESS');
              //$email = $request->email;

              return response()->json(['status'=>true,'message'=>'All Offers','data'=>$offerarr]);
          } else {
              return response()->json(['status'=>false,'message'=>'Offers Not Found.','data'=>'']);
          }


          /*if($offers) {
              //$from_email = env('MAIL_FROM_ADDRESS');
              //$email = $request->email;

              return response()->json(['status'=>true,'message'=>'All Offers','data'=>$offers]);
          } else {
              return response()->json(['status'=>false,'message'=>'Offers Not Found.','data'=>'']);
          }*/


          
        }
      }


  public function shownewoffers(request $request)
    {
        $role = $request->role;
        $user_id = $request->user_id;

        if($role == "1") {
          $offers = Offer::where('offer_status','1')->where('is_offer_deleted','0')->orderBy('id', 'DESC')->get();
          if($offers) {
              return response()->json(['status'=>true,'message'=>'All Offers','data'=>$offers]);
          } else {
              return response()->json(['status'=>false,'message'=>'Offers Not Found.','data'=>'']);
          }
        } else  if($role == "2"){
          $offers = Offer::where('offer_user_id',$user_id)->where('offer_status','1')->where('is_offer_deleted','0')->orderBy('id', 'DESC')->get();

          $currnt_date = date('Y-m-d');
          $offerarr = array();
          foreach($offers as $offer) {
            
              $offer_start_date = $offer->offer_start_date;
              $offer_start_time = date("H:i:s", strtotime($offer->offer_start_time));
              $offer_start_date_time = $offer->offer_start_date.' '.$offer_start_time;
            
              $OfrStartdate = strtotime($offer_start_date_time);
              $diffOfrStart = $OfrStartdate-time();
              $daysOfrStart=floor($diffOfrStart/(60*60*24));
              $hoursOfrStart=floor(($diffOfrStart-$daysOfrStart*60*60*24)/(60*60));
              $minutesOfrStart = floor(($diffOfrStart-$daysOfrStart*60*60*24  
                          - $hoursOfrStart*60*60)/ 60);


              $offer_end_date = $offer->offer_end_date;
              $offer_end_time = date("H:i:s", strtotime($offer->offer_end_time));
              $offer_end_date_time = $offer->offer_end_date.' '.$offer_end_time;
            
              $OfrEnddate = strtotime($offer_end_date_time);
              $diffOfrEnd = $OfrEnddate-time();
              $daysOfrEnd=floor($diffOfrEnd/(60*60*24));
              $hoursOfrEnd=floor(($diffOfrEnd-$daysOfrEnd*60*60*24)/(60*60));
              $minutesOfrEnd = floor(($diffOfrEnd-$daysOfrEnd*60*60*24  
                          - $hoursOfrEnd*60*60)/ 60);

              if($daysOfrStart >= 0) {
                //echo '>>>>'.$offer->id.'=='.$offer_start_date_time.'<br/>';
                // echo "$days days $hours hours remain<br />";

                  $offer['start_days_left'] = $daysOfrStart;
                  $offer['start_time_left'] = $hoursOfrStart.':'.$minutesOfrStart;

                  $offer['days_left'] = $daysOfrEnd;
                  $offer['time_left'] = $hoursOfrEnd.':'.$minutesOfrEnd;
                  array_push($offerarr,$offer);
              }
              
            /*if($offer->offer_end_date >= $currnt_date) {

              $offer_start_date = $offer->offer_start_date;
              $offer_start_time = date("H:i:s", strtotime($offer->offer_start_time));
              $offer_start_date_time = $offer->offer_start_date.' '.$offer_start_time;

              $offer_end_date = $offer->offer_end_date;
              $offer_end_time = date("H:i:s", strtotime($offer->offer_end_time));
              $offer_end_date_time = $offer->offer_end_date.' '.$offer_end_time;

              $OfrStartdate = strtotime($offer_start_date_time);
              $OfrEnddate = strtotime($offer_end_date_time);

              $diff = $OfrEnddate-$OfrStartdate;
              $days=floor($diff/(60*60*24));
              $hours=floor(($diff-$days*60*60*24)/(60*60));
              $minutes = floor(($diff-$days*60*60*24  
                          - $hours*60*60)/ 60);

              $diffOfrStart = $OfrStartdate-time();
              $daysOfrStart=floor($diffOfrStart/(60*60*24));
              $hoursOfrStart=floor(($diffOfrStart-$daysOfrStart*60*60*24)/(60*60));
              $minutesOfrStart = floor(($diffOfrStart-$daysOfrStart*60*60*24  
                          - $hoursOfrStart*60*60)/ 60);
             
                if($days >= 0) {
                 
                  $offer['start_days_left'] = $daysOfrStart;
                  $offer['start_time_left'] = $hoursOfrStart.':'.$minutesOfrStart;

                  $offer['days_left'] = $days;
                  $offer['time_left'] = $hours.':'.$minutes;

                  array_push($offerarr,$offer);
                }

            }*/
            
          }
        if($offerarr) {

            return response()->json(['status'=>true,'message'=>'All Offers','data'=>$offerarr]);
        } 

        }
        else if($role == "3") {
          $user = DB::table('user')->select('user.*')->where('id',$user_id)->get()->first();

          $offers = Offer::where('offer_user_id',$user->invited_from)->where('offer_status','1')->where('is_offer_deleted','0')->orderBy('id', 'DESC')->get();

          $currnt_date = date('Y-m-d');
          $offerarr = array();
          foreach($offers as $offer) {

              $offer_start_date = $offer->offer_start_date;
              $offer_start_time = date("H:i:s", strtotime($offer->offer_start_time));
              $offer_start_date_time = $offer->offer_start_date.' '.$offer_start_time;
            
              $OfrStartdate = strtotime($offer_start_date_time);
              $diffOfrStart = $OfrStartdate-time();
              $daysOfrStart=floor($diffOfrStart/(60*60*24));
              $hoursOfrStart=floor(($diffOfrStart-$daysOfrStart*60*60*24)/(60*60));
              $minutesOfrStart = floor(($diffOfrStart-$daysOfrStart*60*60*24  
                          - $hoursOfrStart*60*60)/ 60);


              $offer_end_date = $offer->offer_end_date;
              $offer_end_time = date("H:i:s", strtotime($offer->offer_end_time));
              $offer_end_date_time = $offer->offer_end_date.' '.$offer_end_time;
            
              $OfrEnddate = strtotime($offer_end_date_time);
              $diffOfrEnd = $OfrEnddate-time();
              $daysOfrEnd=floor($diffOfrEnd/(60*60*24));
              $hoursOfrEnd=floor(($diffOfrEnd-$daysOfrEnd*60*60*24)/(60*60));
              $minutesOfrEnd = floor(($diffOfrEnd-$daysOfrEnd*60*60*24  
                          - $hoursOfrEnd*60*60)/ 60);

              if($daysOfrStart >= 0) {
                //echo '>>>>'.$offer->id.'=='.$offer_start_date_time.'<br/>';
                // echo "$days days $hours hours remain<br />";

                  $offer['start_days_left'] = $daysOfrStart;
                  $offer['start_time_left'] = $hoursOfrStart.':'.$minutesOfrStart;

                  $offer['days_left'] = $daysOfrEnd;
                  $offer['time_left'] = $hoursOfrEnd.':'.$minutesOfrEnd;
                  array_push($offerarr,$offer);
              }
              
            /*if($offer->offer_end_date >= $currnt_date) {

              $offer_start_date = $offer->offer_start_date;
              $offer_start_time = date("H:i:s", strtotime($offer->offer_start_time));
              $offer_start_date_time = $offer->offer_start_date.' '.$offer_start_time;

              $offer_end_date = $offer->offer_end_date;
              $offer_end_time = date("H:i:s", strtotime($offer->offer_end_time));
              $offer_end_date_time = $offer->offer_end_date.' '.$offer_end_time;

              $OfrStartdate = strtotime($offer_start_date_time);
              $OfrEnddate = strtotime($offer_end_date_time);

              $diff = $OfrEnddate-$OfrStartdate;
              $days=floor($diff/(60*60*24));
              $hours=floor(($diff-$days*60*60*24)/(60*60));
              $minutes = floor(($diff-$days*60*60*24  
                          - $hours*60*60)/ 60);

              $diffOfrStart = $OfrStartdate-time();
              $daysOfrStart=floor($diffOfrStart/(60*60*24));
              $hoursOfrStart=floor(($diffOfrStart-$daysOfrStart*60*60*24)/(60*60));
              $minutesOfrStart = floor(($diffOfrStart-$daysOfrStart*60*60*24  
                          - $hoursOfrStart*60*60)/ 60);
             
                if($days >= 0) {
                 
                  $offer['start_days_left'] = $daysOfrStart;
                  $offer['start_time_left'] = $hoursOfrStart.':'.$minutesOfrStart;

                  $offer['days_left'] = $days;
                  $offer['time_left'] = $hours.':'.$minutes;

                  array_push($offerarr,$offer);
                }

            }*/

          }

          if($offerarr) {

              return response()->json(['status'=>true,'message'=>'All Offers','data'=>$offerarr]);
          } else {
              return response()->json(['status'=>false,'message'=>'Offers Not Found.','data'=>'']);
          }

        }
      }    

    public function getofferDetails(Request $request, $id) {
    
      //$isoffer = Offer::find($id);

      $isofferproduct=DB::table('offer')->select('offer.*','product.product_name','product.product_desc','product.product_cat','product.product_img','product.product_unit','product.initial_stock','product.price_to_sale','product.product_cost','product.tax_rate')->join('product','product.id','=','offer.offer_product_id')->where('offer.id',$id)->first();
          $currnt_date = date('Y-m-d');
          /*$start_time = date("H:i", strtotime($isofferproduct->offer_start_time));
          $end_time = date("H:i", strtotime($isofferproduct->offer_end_time));

          $end_date_time = $isofferproduct->offer_end_date." ".$end_time;
          $offer_current_date_time = date('Y-m-d H:i');
          $offer_end_date_time = date($end_date_time);
          
          $time1 = date_create($offer_current_date_time); 
          $time2 = date_create($offer_end_date_time); 
          $timeinterval = date_diff($time1, $time2);
          $time_left = $timeinterval->format("%H:%i"); 

          $datetime1 = date_create($currnt_date); 
          $datetime2 = date_create($isofferproduct->offer_end_date); 
          $interval = date_diff($datetime1, $datetime2); 
          $days_left = $interval->format("%a");
          $isofferproduct->days_left = $days_left;
          $isofferproduct->time_left= $time_left;*/

                $start_time = date("H:i", strtotime($isofferproduct->offer_start_time));
                $end_time = date("H:i", strtotime($isofferproduct->offer_end_time));

                $end_date_time = $isofferproduct->offer_end_date." ".$end_time;
                $start_date_time = $isofferproduct->offer_end_date." ".$start_time;
                $offer_current_date_time = date('Y-m-d H:i');
                $offer_end_date_time = date($end_date_time);
                $offer_start_date_time = date($start_date_time);
                
                $time1 = date_create($offer_current_date_time); 
                $time2 = date_create($offer_end_date_time);
                $time3 = date_create($offer_start_date_time); 
                $timeinterval = date_diff($time1, $time2);
                $starttimeinterval = date_diff($time1, $time3);

                $time_left = $timeinterval->format("%H:%i");
                $start_time_left = $starttimeinterval->format("%H:%i"); 

                $datetime1 = date_create($currnt_date); 
                $datetime2 = date_create($isofferproduct->offer_end_date);
                $datetime3 = date_create($isofferproduct->offer_start_date); 
                $interval = date_diff($datetime1, $datetime2);
                $start_interval = date_diff($datetime1, $datetime3); 
                $days_left = $interval->format("%R%a");


                $start_days_left = $start_interval->format("%R%a");

                if($start_days_left>=0) {
                  $start_offer_days = trim($start_days_left,'+');
                  $start_offer_time = $start_time_left;
                }else {
                  $start_offer_days = '0';
                  $start_offer_time = '0';
                }

                if($days_left>=0) {
                  $isofferproduct->start_days_left = $start_offer_days;
                  $isofferproduct->start_time_left = $start_offer_time;
                  $isofferproduct->days_left = trim($days_left,'+');
                  $isofferproduct->time_left = $time_left;
                  //array_push($offerarr,$offer);
                }


      
      if($isofferproduct) {
        //$product = Product::where('id',$id)->where('product_status','1')->where('is_deleted','0')->get();
        return response()->json(['status'=>true,'message'=>'Offer details','error'=>'','data'=>$isofferproduct], 200);
      } else {
        return response()->json(['status'=>false,'message'=>'Offer not found','error'=>'','data'=>''], 400);
      }
    }

    public function updateofferDetails(Request $request, $id) {
    
      
        $rules = [
            // 'username' => 'unique:users',
            'offer_name' => 'required',
            'offer_desc' => 'required',
            'offer_status' => 'required',
            'offer_price' => 'required',
            'offer_product_id' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) 
        { 
            return response()->json(['status'=>false,'message'=>'Request Failed','error'=>$validator->errors(),'data'=>'']);
        }

        
        $isoffer = Offer::find($id);
        $isoffer->offer_name = $request->offer_name;
        $isoffer->offer_desc = $request->offer_desc;
        $isoffer->offer_product_id = $request->offer_product_id;
        $isoffer->offer_price = $request->offer_price;
        $isoffer->offer_status = $request->offer_status;
        $isoffer->offer_start_date = trim($request->offer_start_date);
        $isoffer->offer_end_date = trim($request->offer_end_date);
        $isoffer->offer_start_time = trim($request->offer_start_time);
        $isoffer->offer_end_time = trim($request->offer_end_time);
        $isoffer->updated_at = date('Y-m-d h:i:s');

        // Check if a profile image has been uploaded
        if ($request->has('offer_img')) {
             $file = $request->file('offer_img');
            $filename = $request->file('offer_img')->getClientOriginalName();
       
       //Move Uploaded File
            $destinationPath = 'uploads/images';
            if(!file_exists( 'uploads/images/'.$filename )) {
              $file->move($destinationPath,$file->getClientOriginalName());
            }
           //$file->move($destinationPath,$file->getClientOriginalName());
           $isoffer->offer_img = $filename;
        }else {
          $prod_id = trim($request->offer_product_id);
          $product = Product::where('id',$prod_id)->get()->first();
          $isoffer->offer_img = $product->product_img;
        } 
        // Persist user record to database
        $isoffer->save();

        return response()->json(['status'=>true,'message'=>'Offer Updated Successfully','data'=>$isoffer]);
      
    }

    public function deleteoffer(request $request,$id)
    {
        $isoffer = Offer::find($id);
        if($isoffer) {

        $offer = Offer::where('id',$id)->where('offer_status','1')->where('is_offer_deleted','0')->get();
        if($offer) {
            $Input =  [];
            $Input['is_offer_deleted'] = "1";

           $offer_data = Offer::where('id', $id)->update($Input);
           $offer_detail = Offer::where('id', $id)->first()->toArray();
           if($offer_detail) {
                //$from_email = env('MAIL_FROM_ADDRESS');
                //$email = $request->email;

            return response()->json(['status'=>true,'message'=>'Offer Deleted Successfully','data'=>$offer_detail]);
            } else {
            return response()->json(['status'=>false,'message'=>'Offer Not Deleted! Something Went wrong','error'=>$validator->errors(),'data'=>'']);
            } 

        
        } 
        
      } else {
        return response()->json(['status'=>false,'message'=>'Offer not found','error'=>'','data'=>''], 400);
      }
        
    }

    public function searchoffer(request $request)
    {
        $srch_offer = trim($request->srch_offer);
        $role = $request->role;
        $user_id = $request->user_id;

       
        
        if($role == "1") {
          $offer_details = Offer::where('offer_name','LIKE','%'.$srch_offer.'%')->where('offer_status','1')->where('is_offer_deleted','0')->orderBy('id', 'DESC')->get();
            if($offer_details) {
                return response()->json(['status'=>true,'message'=>'Searched Offers','data'=>$offer_details]);
            } else {
                 return response()->json(['status'=>false,'message'=>'No Category found!','data'=>'']);
            }
        } else if($role == "2") {
          $offer_details = Offer::where('offer_user_id',$user_id)->where('offer_name','LIKE','%'.$srch_offer.'%')->where('offer_status','1')->where('is_offer_deleted','0')->orderBy('id', 'DESC')->get();
            if($offer_details) {
                return response()->json(['status'=>true,'message'=>'Searched Offers','data'=>$offer_details]);
            } else {
                 return response()->json(['status'=>false,'message'=>'No Category found!','data'=>'']);
            }

        } else if($role == "3") {
          $user = DB::table('user')->select('user.*')->where('id',$user_id)->get()->first();

          $offer_details = Offer::where('offer_user_id',$user->invited_from)->where('offer_name','LIKE','%'.$srch_offer.'%')->where('offer_status','1')->where('is_offer_deleted','0')->orderBy('id', 'DESC')->get();
            if($offer_details) {
                return response()->json(['status'=>true,'message'=>'Searched Offers','data'=>$offer_details]);
            } else {
                 return response()->json(['status'=>false,'message'=>'No Category found!','data'=>'']);
            }
        } 
    }

    public function offerproduct(request $request)
    {
        $offer_id = $request->offer_id;
        
        $isofferproduct=DB::table('offer')->select('offer.*','product.product_name','product.product_desc','product.product_cat','product.product_img','product.product_unit','product.initial_stock','product.price_to_sale','product.product_cost','product.tax_rate')->join('product','product.id','=','offer.offer_product_id')->where('offer.id',$offer_id)->first();
        
        if($isofferproduct) {
            //$from_email = env('MAIL_FROM_ADDRESS');
            //$email = $request->email;

            return response()->json(['status'=>true,'message'=>'offer Product','data'=>$isofferproduct]);
        } else{
            return response()->json(['status'=>true,'false'=>'offer Product not found','data'=>$isofferproduct]);
        }

       
        
        
    }

    public function getrecentoffers(request $request)
    {
        $role = $request->role;
        $user_id = $request->user_id;

        if($role == "1") {
          $offers = Offer::where('offer_status','1')->where('is_offer_deleted','0')->orderBy('id', 'DESC')->limit(5)->get();
          if($offers) {
              //$from_email = env('MAIL_FROM_ADDRESS');
              //$email = $request->email;

              return response()->json(['status'=>true,'message'=>'All Offers','data'=>$offers]);
          } else {
              return response()->json(['status'=>false,'message'=>'Offers Not Found.','data'=>'']);
          }
        } else  if($role == "2"){
          $offers = Offer::where('offer_user_id',$user_id)->where('offer_status','1')->where('is_offer_deleted','0')->orderBy('id', 'DESC')->limit(5)->get();
        if($offers) {
            //$from_email = env('MAIL_FROM_ADDRESS');
            //$email = $request->email;

            return response()->json(['status'=>true,'message'=>'All Offers','data'=>$offers]);
        } 

        }else if($role == "3") {
          $user = DB::table('user')->select('user.*')->where('id',$user_id)->get()->first();

          $offers = Offer::where('offer_user_id',$user->invited_from)->where('offer_status','1')->where('is_offer_deleted','0')->orderBy('id', 'DESC')->limit(5)->get();

          $currnt_date = date('Y-m-d');
          $offerarr = array();
          foreach($offers as $offer) {

            if($currnt_date >= $offer->offer_start_date && $currnt_date <= $offer->offer_end_date){
              $c_time = date('H:i');
              //if($c_time >= $offer->offer_start_time && $c_time <= $offer->offer_end_time) {
                array_push($offerarr,$offer);
             // }

                
              } 
          }

          if($offerarr) {
          
              return response()->json(['status'=>true,'message'=>'All Offers','data'=>$offerarr]);
          } else {
            return response()->json(['status'=>false,'message'=>'Offers Not Found.','data'=>'']);
          }
          
        }
        
    }

    public function getoffersbyhighdiscount(request $request)
    {
        $role = $request->role;
        $user_id = $request->user_id;

        if($role == "1") {
          $offers = Offer::where('offer_status','1')->where('is_offer_deleted','0')->orderBy('offer_price', 'DESC')->get();
          if($offers) {
              //$from_email = env('MAIL_FROM_ADDRESS');
              //$email = $request->email;

              return response()->json(['status'=>true,'message'=>'All Offers','data'=>$offers]);
          } else {
              return response()->json(['status'=>false,'message'=>'Offers Not Found.','data'=>'']);
          }
        } else  if($role == "2"){
          $offers = Offer::where('offer_user_id',$user_id)->where('offer_status','1')->where('is_offer_deleted','0')->orderBy('offer_price', 'DESC')->get();
        if($offers) {
            //$from_email = env('MAIL_FROM_ADDRESS');
            //$email = $request->email;

            return response()->json(['status'=>true,'message'=>'All Offers','data'=>$offers]);
        } 

        }else if($role == "3") {
          $user = DB::table('user')->select('user.*')->where('id',$user_id)->get()->first();

          $offers = Offer::where('offer_user_id',$user->invited_from)->where('offer_status','1')->where('is_offer_deleted','0')->orderBy('offer_price', 'DESC')->get();

          $currnt_date = date('Y-m-d');
          $offerarr = array();
          foreach($offers as $offer) {

            if($currnt_date >= $offer->offer_start_date && $currnt_date <= $offer->offer_end_date){
              $c_time = date('H:i');
              //if($c_time >= $offer->offer_start_time && $c_time <= $offer->offer_end_time) {
                array_push($offerarr,$offer);
             // }

                
              } 
          }

          if($offerarr) {
          
              return response()->json(['status'=>true,'message'=>'All Offers','data'=>$offerarr]);
          } else {
            return response()->json(['status'=>false,'message'=>'Offers Not Found.','data'=>'']);
          }
          
        }
        
    }

     public function getoffersbylowdiscount(request $request)
    {
        $role = $request->role;
        $user_id = $request->user_id;

        if($role == "1") {
          $offers = Offer::where('offer_status','1')->where('is_offer_deleted','0')->orderBy('offer_price', 'ASC')->get();
          if($offers) {
              //$from_email = env('MAIL_FROM_ADDRESS');
              //$email = $request->email;

              return response()->json(['status'=>true,'message'=>'All Offers','data'=>$offers]);
          } else {
              return response()->json(['status'=>false,'message'=>'Offers Not Found.','data'=>'']);
          }
        } else  if($role == "2"){
          $offers = Offer::where('offer_user_id',$user_id)->where('offer_status','1')->where('is_offer_deleted','0')->orderBy('offer_price', 'ASC')->get();
        if($offers) {
            //$from_email = env('MAIL_FROM_ADDRESS');
            //$email = $request->email;

            return response()->json(['status'=>true,'message'=>'All Offers','data'=>$offers]);
        } 

        }else if($role == "3") {
          $user = DB::table('user')->select('user.*')->where('id',$user_id)->get()->first();

          $offers = Offer::where('offer_user_id',$user->invited_from)->where('offer_status','1')->where('is_offer_deleted','0')->orderBy('offer_price', 'ASC')->get();

          $currnt_date = date('Y-m-d');
          $offerarr = array();
          foreach($offers as $offer) {

            if($currnt_date >= $offer->offer_start_date && $currnt_date <= $offer->offer_end_date){
              $c_time = date('H:i');
              //if($c_time >= $offer->offer_start_time && $c_time <= $offer->offer_end_time) {
                array_push($offerarr,$offer);
             // }

                
              } 
          }

          if($offerarr) {
          
              return response()->json(['status'=>true,'message'=>'All Offers','data'=>$offerarr]);
          } else {
            return response()->json(['status'=>false,'message'=>'Offers Not Found.','data'=>'']);
          }
          
        }
        
    }

}