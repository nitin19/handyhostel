<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Config;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;

use DB;
use App\Models\Order;
use App\Models\Product;
use App\Models\Cart;
use App\Models\User;
use App\Models\Notification;
use App\Classes\FirebasenotificationClass;

Class Ordercontroller extends Controller
{
    public function createorder(request $request)
    {

        $rules = [
            // 'username' => 'unique:users',
          'order_product_id' => 'required',
          'order_user_id' => 'required',
          'price' => 'required',
          'quantity' => 'required',
    
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) 
        { 
            return response()->json(['status'=>false,'message'=>'Order Failed','error'=>$validator->errors(),'data'=>'']);
        }

        $lastorder = DB::table('order')->latest('id')->first();
        
        /*if(empty($lastorder)) {
          $order_display_id = 1;
        }else{
          $last_id = $lastorder->id;
          $order_display_id = $last_id + 1;
        }*/
        
        $order_display_id = 'OR_'.time();
        

        $product_ids = explode(',',trim($request->order_product_id));
        $cat_ids = explode(',',trim($request->product_cat_id));
        $prod_quantities = explode(',',trim($request->order_prod_quantity));
        $prod_prices = explode(',',trim($request->order_prod_price));
        $i =0;
       


        if(count($product_ids) > 0) {

        	foreach($product_ids as $p_ids){

        		$Input =  [];
		        $Input['order_product_id'] = $p_ids;
		        $Input['order_offer_id'] = trim($request->order_offer_id);
		        $Input['product_cat_id'] = $cat_ids[$i];
		        $Input['order_user_id'] = trim($request->order_user_id);
		        $Input['price'] = trim($request->price);
		        $Input['quantity'] = trim($request->quantity);
            $Input['order_prod_quantity'] = $prod_quantities[$i];
            $Input['order_prod_price'] = $prod_prices[$i];
		        $Input['order_status'] = "0";
		        $Input['payment_status'] = "0";
		        $Input['is_order_deleted'] = "0";
		        $Input['payment_method'] = trim($request->payment_method);
		        $Input['is_order_cancelled'] = "0";
		        $Input['cancel_date'] = "";
		        $Input['cancel_reason'] = "";
		        $Input['is_order_returned'] = "0";
		        $Input['return_date'] = "";
		        $Input['return_reason'] = "";
		        $Input['card_number'] = "";
		        $Input['exp_mnth'] = "";
		        $Input['exp_yr'] = "";
		        $Input['transaction_id'] = "";
		        $Input['order_address_details'] = "";
            $Input['order_display_id'] = $order_display_id;
		        $Input['additional_notes'] = "";


		        $Input['created_at'] = date('Y-m-d h:i:s');


		       $order_data = Order::create($Input);

        $iscartproduct = Cart::where('cart_product_id',$p_ids)->where('cart_user_id',$request->order_user_id)->first();
          if($iscartproduct) {
            $iscartproduct->delete();
          }
          

            $i++;
        	}
        }

      
           
        
       if($order_data) {
      
          $notify =  [];
          $notify['notification_type'] = "order_created";
          $notify['notification_title'] = "order created";
          $notify['to_user'] = '1';
          $notify['from_user'] = $order_data->order_user_id;
          $notify['table_id'] = $order_data->order_display_id;
          $notify['icon'] = "new_order.png";
          $notify['message'] = "New Order Has been Created Successfully";
          $notify['is_read'] = "0";

          $ndata = Notification::create($notify);

          $touserdata = User::where('id', $ndata->to_user)->first();

          $to_token = $touserdata->device_token;

          $title = 'New Order';
          $type = $ndata->notification_type;
          $message = $ndata->message;


          $notification = array('title' =>$title , 'text' => $message);

          $fields = array(
              'to' => $to_token,
              'data' => array(
                  "type" => $type,
                  "title" => $title,
                  "message" => $message,
                  "notification_id" => $ndata->id,
                  "order_id" => $order_data->id
              ),
              'notification' => $notification,
              'priority' => 'high',
          );
           

          $notificationClass = new FirebasenotificationClass();
          $notification_response = $notificationClass->sendnotification($fields);

          $userdata = User::where('id', $order_data->order_user_id)->first();

          $notify1 =  [];
          $notify1['notification_type'] = "order_created";
          $notify1['notification_title'] = "order created";
          $notify1['to_user'] = $userdata->invited_from;
          $notify1['from_user'] = $order_data->order_user_id;
          $notify1['table_id'] = $order_data->order_display_id;
          $notify1['icon'] = "new_order.png";
          $notify1['message'] = "New Order Has been Created Successfully";
          $notify1['is_read'] = "0";

          $ndata1 = Notification::create($notify1);

          $touserdata1 = User::where('id', $ndata1->to_user)->first();

          $to_token1 = $touserdata1->device_token;

          $title1 = 'New Order';
          $type1 = $ndata1->notification_type;
          $message1 = $ndata1->message;


          $notification1 = array('title' =>$title1 , 'text' => $message1);

          $fields1 = array(
              'to' => $to_token1,
              'data' => array(
                  "type" => $type1,
                  "title" => $title1,
                  "message" => $message1,
                  "notification_id" => $ndata1->id,
                  "order_id" => $order_data->id
              ),
              'notification' => $notification1,
              'priority' => 'high',
          );
           

          $notificationClass1 = new FirebasenotificationClass();
          $notification_response1 = $notificationClass1->sendnotification($fields1);

        	return response()->json(['status'=>true,'message'=>'Order Created Successfully','data'=>$order_data,'adminnotification_data' => $notification_response,'ownernotification_data' => $notification_response1]);
        } else {
        	return response()->json(['status'=>false,'message'=>'Order Not Created! Something Went wrong','error'=>$validator->errors(),'data'=>'']);
        } 
    }

    public function getallordersbyrole(request $request)
    {

        $role = $request->role;
        $user_id = $request->user_id;

        if($role == "1") {
        $orders=DB::table('order')->select('order.*','product.product_name','product.product_desc','product.product_img','user.owner_name','user.email','user.phone_number','user.address')->join('product','product.id','=','order.order_product_id')->join('user','user.id','=','order.order_user_id')->where('order.is_order_deleted','0')->where('order.payment_status','1')->orderBy('order.id','DESC')->get();
          if($orders) {
              //$from_email = env('MAIL_FROM_ADDRESS');
              //$email = $request->email;

              return response()->json(['status'=>true,'message'=>'All Orders','data'=>$orders]);
          }else {
            return response()->json(['status'=>false,'message'=>'No Order Found.','data'=>'']);
          }

        } else if($role == "3"){
           $orders=DB::table('order')->select('order.*','product.product_name','product.product_desc','product.product_img','user.owner_name','user.email','user.phone_number','user.address')->join('product','product.id','=','order.order_product_id')->join('user','user.id','=','order.order_user_id')->where('order.order_user_id',$user_id)->where('order.is_order_deleted','0')->where('order.payment_status','1')->orderBy('order.id', 'DESC')->get();
        
          if($orders) {
              //$from_email = env('MAIL_FROM_ADDRESS');
              //$email = $request->email;

              return response()->json(['status'=>true,'message'=>'All Orders','data'=>$orders]);
          } else {
            return response()->json(['status'=>false,'message'=>'No Order Found.','data'=>'']);
          } 

        }else if($role == "2"){
          $orders=DB::table('order')->select('order.*','product.product_name','product.product_desc','product.product_img','user.owner_name','user.email','user.phone_number','user.address')->join('product','product.id','=','order.order_product_id')->join('user','user.id','=','order.order_user_id')->where('product.user_id',$user_id)->where('order.is_order_deleted','0')->where('order.payment_status','1')->orderBy('order.id', 'DESC')->get();
          if($orders) {
              //$from_email = env('MAIL_FROM_ADDRESS');
              //$email = $request->email;

              return response()->json(['status'=>true,'message'=>'All Orders','data'=>$orders]);
          } else {
            return response()->json(['status'=>false,'message'=>'No Order Found.','data'=>'']);
          } 

        }
        
    }


    public function deleteorder(request $request,$id)
    {
        $isorder = Order::find($id);
        if($isorder) {

        $order = Order::where('id',$id)->where('order_status','1')->where('is_order_deleted','0')->get();
        if($order) {
            $Input =  [];
            $Input['is_order_deleted'] = "1";

           $order_data = Order::where('id', $id)->update($Input);
           $order_detail = Order::where('id', $id)->first()->toArray();
           if($order_detail) {
                //$from_email = env('MAIL_FROM_ADDRESS');
                //$email = $request->email;

            return response()->json(['status'=>true,'message'=>'Order Deleted Successfully','data'=>$order_detail]);
            } else {
            return response()->json(['status'=>false,'message'=>'Order Not Deleted! Something Went wrong','error'=>$validator->errors(),'data'=>'']);
            } 

        
        } 
        
      } else {
        return response()->json(['status'=>false,'message'=>'Order not found','error'=>'','data'=>''], 400);
      }
        
    }

    public function getorderDetails(Request $request, $id) {
      
       $isorder=DB::table('order')->select('order.*','product.product_name','product.product_desc','product.product_img','user.owner_name','user.email','user.phone_number','user.address')->join('product','product.id','=','order.order_product_id')->join('user','user.id','=','order.order_user_id')->where('order.id',$id)->where('order.payment_status','1')->get()->first();
      if($isorder) {
        //$product = Product::where('id',$isorder->order_product_id)->get();
        return response()->json(['status'=>true,'message'=>'Order details','error'=>'','data'=>$isorder], 200);
      } else {
        return response()->json(['status'=>false,'message'=>'Order not found','error'=>'','data'=>''], 400);
      }
    }

    public function cancelorder(request $request,$id)
    {
        $user_id = $request->user_id;
        $role = $request->role;
        $isorder = Order::find($id);
        if($isorder) {

        $order = Order::where('order_display_id',$isorder->order_display_id)->where('order_status','1')->where('is_order_deleted','0')->get();
        if($order) {
          foreach($order as $od) {
            $Input =  [];
            $Input['is_order_cancelled'] = "1";
            $Input['cancel_date'] = date('Y-m-d h:i:s');
            $Input['cancel_reason'] = "";
            
           $order_data = Order::where('id', $od->id)->update($Input);
          }
          
           $order_detail = Order::where('id', $id)->first();
           if($order_detail) {
                //$from_email = env('MAIL_FROM_ADDRESS');
                //$email = $request->email;
            if($role = '1') {
              $notify =  [];
              $notify['notification_type'] = "order_cancelled";
              $notify['notification_title'] = "order cancelled";
              $notify['to_user'] = $order_detail->order_user_id;
              $notify['from_user'] = $user_id;
              $notify['table_id'] = $order_detail->id;
              $notify['icon'] = "cancel_order.png";
              $notify['message'] = "Your Order has been Cancelled";
              $notify['is_read'] = "0";

              $ndata = Notification::create($notify);

              $touserdata = User::where('id', $ndata->to_user)->first();

              $to_token = $touserdata->device_token;

              $title = 'Order Cancelled';
              $type = $ndata->notification_type;
              $message = $ndata->message;


              $notification = array('title' =>$title , 'text' => $message);

              $fields = array(
                  'to' => $to_token,
                  'data' => array(
                      "type" => $type,
                      "title" => $title,
                      "message" => $message,
                      "notification_id" => $ndata->id,
                      "order_id" => $order_detail->id
                  ),
                  'notification' => $notification,
                  'priority' => 'high',
              );
               

              $notificationClass = new FirebasenotificationClass();
              $notification_response = $notificationClass->sendnotification($fields);


              $user_data = User::where('id', $order_detail->order_user_id)->first();

              $notify1 =  [];
              $notify1['notification_type'] = "order_cancelled";
              $notify1['notification_title'] = "order cancelled";
              $notify1['to_user'] = $user_data->invited_from;
              $notify1['from_user'] = $user_id;
              $notify1['table_id'] = $order_detail->id;
              $notify1['icon'] = "cancel_order.png";
              $notify1['message'] = "Your Order has been Cancelled";
              $notify1['is_read'] = "0";

              $ndata1 = Notification::create($notify1);

              $touserdata1 = User::where('id', $ndata1->to_user)->first();

              $to_token1 = $touserdata1->device_token;

              $title1 = 'Order Cancelled';
              $type1 = $ndata->notification_type;
              $message1 = $ndata->message;


              $notification1 = array('title' =>$title1 , 'text' => $message1);

              $fields1 = array(
                  'to' => $to_token1,
                  'data' => array(
                      "type" => $type1,
                      "title" => $title1,
                      "message" => $message1,
                      "notification_id" => $ndata1->id,
                      "order_id" => $order_detail->id
                  ),
                  'notification' => $notification1,
                  'priority' => 'high',
              );
               

              $notificationClass1 = new FirebasenotificationClass();
              $notification_response1 = $notificationClass1->sendnotification($fields1);

            }else if($role = '2'){

              $notify =  [];
              $notify['notification_type'] = "order_cancelled";
              $notify['notification_title'] = "order cancelled";
              $notify['to_user'] = $order_detail->order_user_id;
              $notify['from_user'] = $user_id;
              $notify['table_id'] = $order_detail->id;
              $notify['icon'] = "cancel_order.png";
              $notify['message'] = "Your Order has been Cancelled";
              $notify['is_read'] = "0";

              $ndata = Notification::create($notify);

              $touserdata = User::where('id', $ndata->to_user)->first();

              $to_token = $touserdata->device_token;

              $title = 'Order Cancelled';
              $type = $ndata->notification_type;
              $message = $ndata->message;


              $notification = array('title' =>$title , 'text' => $message);

              $fields = array(
                  'to' => $to_token,
                  'data' => array(
                      "type" => $type,
                      "title" => $title,
                      "message" => $message,
                      "notification_id" => $ndata->id,
                      "order_id" => $order_detail->id
                  ),
                  'notification' => $notification,
                  'priority' => 'high',
              );
               

              $notificationClass = new FirebasenotificationClass();
              $notification_response = $notificationClass->sendnotification($fields);


              //$user_data =  User::where('id', $order_detail->order_user_id)->first();

              $notify1 =  [];
              $notify1['notification_type'] = "order_cancelled";
              $notify1['notification_title'] = "order cancelled";
              $notify1['to_user'] = '1';
              $notify1['from_user'] = $user_id;
              $notify1['table_id'] = $order_detail->id;
              $notify1['icon'] = "cancel_order.png";
              $notify1['message'] = "Your Order has been Cancelled";
              $notify1['is_read'] = "0";

              $ndata1 = Notification::create($notify1);

              $touserdata1 = User::where('id', $ndata1->to_user)->first();

              $to_token1 = $touserdata1->device_token;

              $title1 = 'Order Cancelled';
              $type1 = $ndata->notification_type;
              $message1 = $ndata->message;


              $notification1 = array('title' =>$title1 , 'text' => $message1);

              $fields1 = array(
                  'to' => $to_token1,
                  'data' => array(
                      "type" => $type1,
                      "title" => $title1,
                      "message" => $message1,
                      "notification_id" => $ndata1->id,
                      "order_id" => $order_detail->id
                  ),
                  'notification' => $notification1,
                  'priority' => 'high',
              );
               

              $notificationClass1 = new FirebasenotificationClass();
              $notification_response1 = $notificationClass1->sendnotification($fields1);

            }else if($role = '3'){
              $user_data =  User::where('id', $order_detail->order_user_id)->first();

              $notify =  [];
              $notify['notification_type'] = "order_cancelled";
              $notify['notification_title'] = "order cancelled";
              $notify['to_user'] = $user_data->invited_from;
              $notify['from_user'] = $user_id;
              $notify['table_id'] = $order_detail->id;
              $notify['icon'] = "cancel_order.png";
              $notify['message'] = "Your Order has been Cancelled";
              $notify['is_read'] = "0";

              $ndata = Notification::create($notify);

              $touserdata = User::where('id', $ndata->to_user)->first();

              $to_token = $touserdata->device_token;

              $title = 'Order Cancelled';
              $type = $ndata->notification_type;
              $message = $ndata->message;


              $notification = array('title' =>$title , 'text' => $message);

              $fields = array(
                  'to' => $to_token,
                  'data' => array(
                      "type" => $type,
                      "title" => $title,
                      "message" => $message,
                      "notification_id" => $ndata->id,
                      "order_id" => $order_detail->id
                  ),
                  'notification' => $notification,
                  'priority' => 'high',
              );
               

              $notificationClass = new FirebasenotificationClass();
              $notification_response = $notificationClass->sendnotification($fields);


              //$user_data =  User::where('id', $order_detail->order_user_id)->first();

              $notify1 =  [];
              $notify1['notification_type'] = "order_cancelled";
              $notify1['notification_title'] = "order cancelled";
              $notify1['to_user'] = '1';
              $notify1['from_user'] = $user_id;
              $notify1['table_id'] = $order_detail->id;
              $notify1['icon'] = "cancel_order.png";
              $notify1['message'] = "Your Order has been Cancelled";
              $notify1['is_read'] = "0";

              $ndata1 = Notification::create($notify1);

              $touserdata1 = User::where('id', $ndata1->to_user)->first();

              $to_token1 = $touserdata1->device_token;

              $title1 = 'Order Cancelled';
              $type1 = $ndata->notification_type;
              $message1 = $ndata->message;


              $notification1 = array('title' =>$title1 , 'text' => $message1);

              $fields1 = array(
                  'to' => $to_token1,
                  'data' => array(
                      "type" => $type1,
                      "title" => $title1,
                      "message" => $message1,
                      "notification_id" => $ndata1->id,
                      "order_id" => $order_detail->id
                  ),
                  'notification' => $notification1,
                  'priority' => 'high',
              );
               

              $notificationClass1 = new FirebasenotificationClass();
              $notification_response1 = $notificationClass1->sendnotification($fields1);



            }
            
            
            return response()->json(['status'=>true,'message'=>'Order Cancelled Successfully','data'=>$order_detail,'notification_data' => $notification_response]);
            } else {
            return response()->json(['status'=>false,'message'=>'Order Not Cancelled! Something Went wrong','error'=>$validator->errors(),'data'=>'']);
            } 

        
        } 
        
      } else {
        return response()->json(['status'=>false,'message'=>'Order not found','error'=>'','data'=>''], 400);
      }
        
    }

     public function returnorder(request $request,$id)
    {
        $user_id = $request->user_id;
        $isorder = Order::find($id);
        if($isorder) {

        $order = Order::where('id',$id)->where('order_user_id',$user_id)->where('order_status','1')->where('is_order_deleted','0')->get();
        if($order) {
            $Input =  [];
            $Input['is_order_returned'] = "1";
            $Input['return_date'] = date('Y-m-d h:i:s');
            $Input['return_reason'] = $request->return_reason;
            
           $order_data = Order::where('id', $id)->update($Input);
           $order_detail = Order::where('id', $id)->first()->toArray();
           if($order_detail) {
                //$from_email = env('MAIL_FROM_ADDRESS');
                //$email = $request->email;

            return response()->json(['status'=>true,'message'=>'Request to return order updated successfully','data'=>$order_detail]);
            } else {
            return response()->json(['status'=>false,'message'=>'Order Not Updated! Something Went wrong','error'=>$validator->errors(),'data'=>'']);
            } 

        
        } 
        
      } else {
        return response()->json(['status'=>false,'message'=>'Order not found','error'=>'','data'=>''], 400);
      }
        
    }

    public function getrecentorders(request $request)
    {
        $role = $request->role;
        $user_id = $request->user_id;
        
        if($role == "1") {
           $rcnt_orders=DB::table('order')->select('order.*','product.product_name','product.product_desc','product.product_img','user.owner_name','user.email','user.phone_number','user.address')->join('product','product.id','=','order.order_product_id')->join('user','user.id','=','order.order_user_id')->where('order.is_order_deleted','0')->where('order.payment_status','1')->orderBy('order.id','DESC')->limit(10)->get();
            if($rcnt_orders) {
              return response()->json(['status'=>true,'message'=>'Recent Products','data'=>$rcnt_orders]);
            } else {
               return response()->json(['status'=>false,'message'=>'No Product found!','data'=>'']);
            } 
        } else if($role == "2"){
          $rcnt_orders=DB::table('order')->select('order.*','product.product_name','product.product_desc','product.product_img','user.owner_name','user.email','user.phone_number','user.address')->join('product','product.id','=','order.order_product_id')->join('user','user.id','=','order.order_user_id')->where('product.user_id',$user_id)->where('order.is_order_deleted','0')->where('order.payment_status','1')->orderBy('order.id', 'DESC')->limit(10)->get();
          if($rcnt_orders) {
            return response()->json(['status'=>true,'message'=>'Recent Products','data'=>$rcnt_orders]);
          } else {
             return response()->json(['status'=>false,'message'=>'No Product found!','data'=>'']);
          } 
        } else if($role == "3"){
          $rcnt_orders=DB::table('order')->select('order.*','product.product_name','product.product_desc','product.product_img','user.owner_name','user.email','user.phone_number','user.address')->join('product','product.id','=','order.order_product_id')->join('user','user.id','=','order.order_user_id')->where('order.order_user_id',$user_id)->where('order.is_order_deleted','0')->where('order.payment_status','1')->orderBy('order.id', 'DESC')->limit(10)->get();
           //$rcnt_products = Product::where('product_status','1')->where('is_deleted','0')->orderBy('id', 'DESC')->limit(10)->get();
            if($rcnt_orders) {
              return response()->json(['status'=>true,'message'=>'Recent Products','data'=>$rcnt_orders]);
            } else {
               return response()->json(['status'=>false,'message'=>'No Product found!','data'=>'']);
            } 
        }
        
    }

    public function getordersbyhightotalprice(request $request)
    {
        $role = $request->role;
        $user_id = $request->user_id;
        
        if($role == "1") {
           $rcnt_orders=DB::table('order')->select('order.*','product.product_name','product.product_desc','product.product_img','user.owner_name','user.email','user.phone_number','user.address')->join('product','product.id','=','order.order_product_id')->join('user','user.id','=','order.order_user_id')->where('order.is_order_deleted','0')->where('order.payment_status','1')->orderBy('order.price','DESC')->get();
            if($rcnt_orders) {
              return response()->json(['status'=>true,'message'=>'Recent Products','data'=>$rcnt_orders]);
            } else {
               return response()->json(['status'=>false,'message'=>'No Product found!','data'=>'']);
            } 
        } else if($role == "2"){
          $rcnt_orders=DB::table('order')->select('order.*','product.product_name','product.product_desc','product.product_img','user.owner_name','user.email','user.phone_number','user.address')->join('product','product.id','=','order.order_product_id')->join('user','user.id','=','order.order_user_id')->where('product.user_id',$user_id)->where('order.is_order_deleted','0')->where('order.payment_status','1')->orderBy('order.price', 'DESC')->get();
          if($rcnt_orders) {
            return response()->json(['status'=>true,'message'=>'Recent Products','data'=>$rcnt_orders]);
          } else {
             return response()->json(['status'=>false,'message'=>'No Product found!','data'=>'']);
          } 
        } else if($role == "3"){
          $rcnt_orders=DB::table('order')->select('order.*','product.product_name','product.product_desc','product.product_img','user.owner_name','user.email','user.phone_number','user.address')->join('product','product.id','=','order.order_product_id')->join('user','user.id','=','order.order_user_id')->where('order.order_user_id',$user_id)->where('order.is_order_deleted','0')->where('order.payment_status','1')->orderBy('order.price', 'DESC')->get();
           //$rcnt_products = Product::where('product_status','1')->where('is_deleted','0')->orderBy('id', 'DESC')->limit(10)->get();
            if($rcnt_orders) {
              return response()->json(['status'=>true,'message'=>'Recent Products','data'=>$rcnt_orders]);
            } else {
               return response()->json(['status'=>false,'message'=>'No Product found!','data'=>'']);
            } 
        }
        
    }

    public function getordersbylowtotalprice(request $request)
    {
        $role = $request->role;
        $user_id = $request->user_id;
        
        if($role == "1") {
           $rcnt_orders=DB::table('order')->select('order.*','product.product_name','product.product_desc','product.product_img','user.owner_name','user.email','user.phone_number','user.address')->join('product','product.id','=','order.order_product_id')->join('user','user.id','=','order.order_user_id')->where('order.is_order_deleted','0')->where('order.payment_status','1')->orderBy('order.price','ASC')->get();
            if($rcnt_orders) {
              return response()->json(['status'=>true,'message'=>'Recent Products','data'=>$rcnt_orders]);
            } else {
               return response()->json(['status'=>false,'message'=>'No Product found!','data'=>'']);
            } 
        } else if($role == "2"){
          $rcnt_orders=DB::table('order')->select('order.*','product.product_name','product.product_desc','product.product_img','user.owner_name','user.email','user.phone_number','user.address')->join('product','product.id','=','order.order_product_id')->join('user','user.id','=','order.order_user_id')->where('product.user_id',$user_id)->where('order.is_order_deleted','0')->where('order.payment_status','1')->orderBy('order.price', 'ASC')->get();
          if($rcnt_orders) {
            return response()->json(['status'=>true,'message'=>'Recent Products','data'=>$rcnt_orders]);
          } else {
             return response()->json(['status'=>false,'message'=>'No Product found!','data'=>'']);
          } 
        } else if($role == "3"){
          $rcnt_orders=DB::table('order')->select('order.*','product.product_name','product.product_desc','product.product_img','user.owner_name','user.email','user.phone_number','user.address')->join('product','product.id','=','order.order_product_id')->join('user','user.id','=','order.order_user_id')->where('order.order_user_id',$user_id)->where('order.is_order_deleted','0')->where('order.payment_status','1')->orderBy('order.price', 'ASC')->get();
           //$rcnt_products = Product::where('product_status','1')->where('is_deleted','0')->orderBy('id', 'DESC')->limit(10)->get();
            if($rcnt_orders) {
              return response()->json(['status'=>true,'message'=>'Recent Products','data'=>$rcnt_orders]);
            } else {
               return response()->json(['status'=>false,'message'=>'No Product found!','data'=>'']);
            } 
        }
        
    }


    public function getordersbylastweek(request $request)
    {
        $role = $request->role;
        $user_id = $request->user_id;
        $mygetdate = \Carbon\Carbon::today()->subDays(7);
        if($role == "1") {
          
          //$members = Member::where('created_at', '>=', $mygetdate)->get();
           $rcnt_orders=DB::table('order')->select('order.*','product.product_name','product.product_desc','product.product_img','user.owner_name','user.email','user.phone_number','user.address')->join('product','product.id','=','order.order_product_id')->join('user','user.id','=','order.order_user_id')->where('order.is_order_deleted','0')->where('order.created_at', '>=', $mygetdate)->where('order.payment_status','1')->orderBy('order.created_at','DESC')->get();
            if($rcnt_orders) {
              return response()->json(['status'=>true,'message'=>'Recent Products','data'=>$rcnt_orders]);
            } else {
               return response()->json(['status'=>false,'message'=>'No Product found!','data'=>'']);
            } 
        } else if($role == "2"){
          $rcnt_orders=DB::table('order')->select('order.*','product.product_name','product.product_desc','product.product_img','user.owner_name','user.email','user.phone_number','user.address')->join('product','product.id','=','order.order_product_id')->join('user','user.id','=','order.order_user_id')->where('product.user_id',$user_id)->where('order.is_order_deleted','0')->where('order.created_at', '>=', $mygetdate)->where('order.payment_status','1')->orderBy('order.created_at','DESC')->get();
          if($rcnt_orders) {
            return response()->json(['status'=>true,'message'=>'Recent Products','data'=>$rcnt_orders]);
          } else {
             return response()->json(['status'=>false,'message'=>'No Product found!','data'=>'']);
          } 
        } else if($role == "3"){
          $rcnt_orders=DB::table('order')->select('order.*','product.product_name','product.product_desc','product.product_img','user.owner_name','user.email','user.phone_number','user.address')->join('product','product.id','=','order.order_product_id')->join('user','user.id','=','order.order_user_id')->where('order.order_user_id',$user_id)->where('order.is_order_deleted','0')->where('order.created_at', '>=', $mygetdate)->where('order.payment_status','1')->orderBy('order.created_at','DESC')->get();
           //$rcnt_products = Product::where('product_status','1')->where('is_deleted','0')->orderBy('id', 'DESC')->limit(10)->get();
            if($rcnt_orders) {
              return response()->json(['status'=>true,'message'=>'Recent Products','data'=>$rcnt_orders]);
            } else {
               return response()->json(['status'=>false,'message'=>'No Product found!','data'=>'']);
            } 
        }
        
    }

    public function getordersbylastmonth(request $request)
    {
        $role = $request->role;
        $user_id = $request->user_id;
        $mygetdate = \Carbon\Carbon::today()->subDays(30);
        if($role == "1") {
          
          //$members = Member::where('created_at', '>=', $mygetdate)->get();
           $rcnt_orders=DB::table('order')->select('order.*','product.product_name','product.product_desc','product.product_img','user.owner_name','user.email','user.phone_number','user.address')->join('product','product.id','=','order.order_product_id')->join('user','user.id','=','order.order_user_id')->where('order.is_order_deleted','0')->where('order.created_at', '>=', $mygetdate)->where('order.payment_status','1')->orderBy('order.created_at','DESC')->get();
            if($rcnt_orders) {
              return response()->json(['status'=>true,'message'=>'Recent Products','data'=>$rcnt_orders]);
            } else {
               return response()->json(['status'=>false,'message'=>'No Product found!','data'=>'']);
            } 
        } else if($role == "2"){
          $rcnt_orders=DB::table('order')->select('order.*','product.product_name','product.product_desc','product.product_img','user.owner_name','user.email','user.phone_number','user.address')->join('product','product.id','=','order.order_product_id')->join('user','user.id','=','order.order_user_id')->where('product.user_id',$user_id)->where('order.is_order_deleted','0')->where('order.created_at', '>=', $mygetdate)->where('order.payment_status','1')->orderBy('order.created_at','DESC')->get();
          if($rcnt_orders) {
            return response()->json(['status'=>true,'message'=>'Recent Products','data'=>$rcnt_orders]);
          } else {
             return response()->json(['status'=>false,'message'=>'No Product found!','data'=>'']);
          } 
        } else if($role == "3"){
          $rcnt_orders=DB::table('order')->select('order.*','product.product_name','product.product_desc','product.product_img','user.owner_name','user.email','user.phone_number','user.address')->join('product','product.id','=','order.order_product_id')->join('user','user.id','=','order.order_user_id')->where('order.order_user_id',$user_id)->where('order.is_order_deleted','0')->where('order.created_at', '>=', $mygetdate)->where('order.payment_status','1')->orderBy('order.created_at','DESC')->get();
           //$rcnt_products = Product::where('product_status','1')->where('is_deleted','0')->orderBy('id', 'DESC')->limit(10)->get();
            if($rcnt_orders) {
              return response()->json(['status'=>true,'message'=>'Recent Products','data'=>$rcnt_orders]);
            } else {
               return response()->json(['status'=>false,'message'=>'No Product found!','data'=>'']);
            } 
        }
        
    }

    public function getordersbylastyear(request $request)
    {
        $role = $request->role;
        $user_id = $request->user_id;
        $mygetdate = \Carbon\Carbon::today()->subDays(365);
        
        if($role == "1") {
          
          //$members = Member::where('created_at', '>=', $mygetdate)->get();
           $rcnt_orders=DB::table('order')->select('order.*','product.product_name','product.product_desc','product.product_img','user.owner_name','user.email','user.phone_number','user.address')->join('product','product.id','=','order.order_product_id')->join('user','user.id','=','order.order_user_id')->where('order.is_order_deleted','0')->where('order.created_at', '>=', $mygetdate)->where('order.payment_status','1')->orderBy('order.created_at','DESC')->get();
            if($rcnt_orders) {
              return response()->json(['status'=>true,'message'=>'Recent Products','data'=>$rcnt_orders]);
            } else {
               return response()->json(['status'=>false,'message'=>'No Product found!','data'=>'']);
            } 
        } else if($role == "2"){
          $rcnt_orders=DB::table('order')->select('order.*','product.product_name','product.product_desc','product.product_img','user.owner_name','user.email','user.phone_number','user.address')->join('product','product.id','=','order.order_product_id')->join('user','user.id','=','order.order_user_id')->where('product.user_id',$user_id)->where('order.is_order_deleted','0')->where('order.created_at', '>=', $mygetdate)->where('order.payment_status','1')->orderBy('order.created_at','DESC')->get();
          if($rcnt_orders) {
            return response()->json(['status'=>true,'message'=>'Recent Products','data'=>$rcnt_orders]);
          } else {
             return response()->json(['status'=>false,'message'=>'No Product found!','data'=>'']);
          } 
        } else if($role == "3"){
          $rcnt_orders=DB::table('order')->select('order.*','product.product_name','product.product_desc','product.product_img','user.owner_name','user.email','user.phone_number','user.address')->join('product','product.id','=','order.order_product_id')->join('user','user.id','=','order.order_user_id')->where('order.order_user_id',$user_id)->where('order.is_order_deleted','0')->where('order.created_at', '>=', $mygetdate)->where('order.payment_status','1')->orderBy('order.created_at','DESC')->get();
           //$rcnt_products = Product::where('product_status','1')->where('is_deleted','0')->orderBy('id', 'DESC')->limit(10)->get();
            if($rcnt_orders) {
              return response()->json(['status'=>true,'message'=>'Recent Products','data'=>$rcnt_orders]);
            } else {
               return response()->json(['status'=>false,'message'=>'No Product found!','data'=>'']);
            } 
        }
        
    }
}