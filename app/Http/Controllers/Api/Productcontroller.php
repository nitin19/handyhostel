<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
//use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Config;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;

use DB;
use App\Models\Product;
use App\Models\Category;
use App\Models\City;
use App\Models\User;
use App\Models\Notification;
use App\Models\Trendingproduct;
use App\Classes\FirebasenotificationClass;

class Productcontroller extends Controller
{
    public function addproduct(request $request)
    {
        
        $rules = [
            // 'username' => 'unique:users',
            //'product_name' => 'required|unique:product',
            'product_name' => 'required',
            'product_desc' => 'required',
            'product_cat' => 'required',
            //'product_unit' => 'required',
            'initial_stock' => 'required',
            'price_to_sale' => 'required',
            'product_cost' => 'required',
            'user_id' => 'required',
    
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) 
        { 
            return response()->json(['status'=>false,'message'=>'Request Failed','error'=>$validator->errors(),'data'=>'']);
        }

        $user_id = trim($request->user_id);
        $p_name = trim($request->product_name);
        
        $products = Product::where('user_id',$user_id)->where('is_deleted','0')->get();
        
        foreach($products as $prdct) {
            if(trim($prdct->product_name) == $p_name) {
              return response()->json(['status'=>false,'message'=>'Prodcut Already Available.Please Choose another name','error'=>$validator->errors(),'data'=>'']);
            }

        }


        $Input =  [];
        $Input['user_id'] = trim($request->user_id);
        $Input['product_name'] = trim($request->product_name);
        $Input['product_desc'] = trim($request->product_desc);
        $Input['product_cat'] = trim($request->product_cat);
       
        $Input['product_unit'] = trim($request->product_unit);
        $Input['initial_stock'] = trim($request->initial_stock);
        $Input['price_to_sale'] = trim($request->price_to_sale);
        $Input['product_cost'] = trim($request->product_cost);
        $Input['tax_rate'] = trim($request->tax_rate);
        $Input['number_of_product_sold'] = "0";
        $Input['product_status'] = "1";
        $Input['is_deleted'] = "0";
        $Input['created_at'] = date('Y-m-d h:i:s');
        
        if ($request->hasfile('product_img')) {
            $file = $request->file('product_img');
            $filename = $request->file('product_img')->getClientOriginalName();
       // Get the contents of the file
       //$contents = $file->openFile()->fread($file->getSize());

       // Store the contents to the database
            $Input['product_img'] = $filename;

        
   
       //Move Uploaded File
            $destinationPath = 'uploads/images';
            if(!file_exists( 'uploads/images/'.$filename )) {
              $file->move($destinationPath,$file->getClientOriginalName());
            }
            

        }else {
          $cat_id = trim($request->product_cat);
          $category = Category::where('id',$cat_id)->where('category_status','1')->where('is_deleted','0')->get()->first();
          $Input['product_img'] = $category->category_img;
        }
       

       $product_data = Product::create($Input);
       if($product_data) {
        	//$from_email = env('MAIL_FROM_ADDRESS');
            //$email = $request->email;
         $users = User::where('invited_from', $product_data->user_id)->where('status', '1')->where('isactivation_complete','1')->where('is_deleted','0')->where('role','=','3')->get();

          foreach($users as $userdt) {

              $notify =  [];
              $notify['notification_type'] = "product_created";
              $notify['notification_title'] = "product created";
              $notify['to_user'] = $userdt->id;
              $notify['from_user'] = $product_data->user_id;
              $notify['table_id'] = $product_data->id;
              $notify['icon'] = "product.png";
              $notify['message'] = "New Product Has been Added!";
              $notify['is_read'] = "0";

              $ndata = Notification::create($notify);

              $to_token = $userdt->device_token;

              $title = 'New Product Created';
              $type = $ndata->notification_type;
              $message = $ndata->message;


              $notification = array('title' =>$title , 'text' => $message);

              $fields = array(
                  'to' => $to_token,
                  'data' => array(
                      "type" => $type,
                      "title" => $title,
                      "message" => $message,
                      "notification_id" => $ndata->id,
                      "product_id" => $product_data->id
                  ),
                  'notification' => $notification,
                  'priority' => 'high',
              );
               

              $notificationClass = new FirebasenotificationClass();
              $notification_response = $notificationClass->sendnotification($fields);


            }


        	return response()->json(['status'=>true,'message'=>'Product Added Successfully','data'=>$product_data]);
        } else {
        	return response()->json(['status'=>false,'message'=>'Product Not Added! Something Went wrong','error'=>$validator->errors(),'data'=>'']);
        } 
    }

    public function showproducts(request $request)
    {
        $products = Product::where('product_status','1')->where('is_deleted','0')->orderBy('id', 'DESC')->get();
        if($products) {
            //$from_email = env('MAIL_FROM_ADDRESS');
            //$email = $request->email;

            return response()->json(['status'=>true,'message'=>'All Products','data'=>$products]);
        } 
    }

    public function getproductsbyrole(request $request)
    {
        $role = $request->role;
        $user_id = $request->user_id;

        if($role == "1") {
          $products = Product::where('is_deleted','0')->orderBy('id', 'DESC')->get();
          if($products) {
            return response()->json(['status'=>true,'message'=>'All Products','data'=>$products]);
          } else {
            return response()->json(['status'=>false,'message'=>'No Product Found.','data'=>'']);
          }
        } else if($role == "2"){
          $products = Product::where('user_id',$user_id)->where('is_deleted','0')->orderBy('id', 'DESC')->get();
          if($products) {
            return response()->json(['status'=>true,'message'=>'All Products','data'=>$products]);
          }else {
            return response()->json(['status'=>false,'message'=>'No Product Found.','data'=>'']);
          }
        } else if($role == "3") {
          $user = DB::table('user')->select('user.*')->where('id',$user_id)->get()->first();
          
          //$products = Product::where('user_id',$user->invited_from)->where('product_status','1')->where('is_deleted','0')->orderBy('id', 'DESC')->get();
          $products = Product::where('product_status','1')->where('is_deleted','0')->orderBy('id', 'DESC')->get();
          if($products) {
            return response()->json(['status'=>true,'message'=>'All Products','data'=>$products]);
          } else {
            return response()->json(['status'=>false,'message'=>'No Product Found.','data'=>'']);
          }
        }
        /*$products = Product::where('product_status','1')->where('is_deleted','0')->orderBy('id', 'DESC')->get();
        if($products) {
            //$from_email = env('MAIL_FROM_ADDRESS');
            //$email = $request->email;

            return response()->json(['status'=>true,'message'=>'All Products','data'=>$products]);
        } */
    }

    public function getproductsbycategory(request $request,$id)
    {
        /*$categories = Category::where('category_status','1')->where('is_deleted','0')->orderBy('id', 'DESC')->get();

        foreach($categories as $category) {

          return $category;
            
        }*/
        $role = $request->role;
        $user_id = $request->user_id;

        if($role == "1") {
          $products = Product::where('product_cat',$id)->where('is_deleted','0')->orderBy('id', 'DESC')->get();
          if($products) {
            return response()->json(['status'=>true,'message'=>'Category Products','data'=>$products]);
          } else {
            return response()->json(['status'=>false,'message'=>'No Product Found.','data'=>'']);
          }
        } else if($role == "2") {
          $products = Product::where('product_cat',$id)->where('user_id',$user_id)->where('is_deleted','0')->orderBy('id', 'DESC')->get();
          if($products) {
            return response()->json(['status'=>true,'message'=>'Category Products','data'=>$products]);
          } else {
            return response()->json(['status'=>false,'message'=>'No Product Found.','data'=>'']);
          }

        } else if($role == "3") {
           $user = DB::table('user')->select('user.*')->where('id',$user_id)->get()->first();
          
          $products = Product::where('product_cat',$id)->where('user_id',$user->invited_from)->where('product_status','1')->where('is_deleted','0')->orderBy('id', 'DESC')->get();
           // $products = Product::where('product_cat',$id)->where('product_status','1')->where('is_deleted','0')->orderBy('id', 'DESC')->get();
          if($products) {
            return response()->json(['status'=>true,'message'=>'Category Products','data'=>$products]);
          }else {
            return response()->json(['status'=>false,'message'=>'No Product Found.','data'=>'']);
          } 
        }
        
       
       
    }

    public function getproductDetails(Request $request, $id) {
    
      //$isproduct = Product::find($id)->join('category', 'category.id', '=', 'product.product_cat');
      $isproduct=DB::table('product')->select('product.*','category.category_name')->join('category','category.id','=','product.product_cat')->where('product.id',$id)->get()->first();
      
      if($isproduct) {
        //$product = Product::where('id',$id)->where('product_status','1')->where('is_deleted','0')->get();
        return response()->json(['status'=>true,'message'=>'Product details','error'=>'','data'=>$isproduct], 200);
      } else {
        return response()->json(['status'=>false,'message'=>'Product not found','error'=>'','data'=>''], 400);
      }
    }

    public function updateproductDetails(Request $request, $id) {
    
      $rules = [
            // 'username' => 'unique:users',
            'product_name' => 'required',
            'product_desc' => 'required',
            'product_cat' => 'required',
            'product_unit' => 'required',
            'initial_stock' => 'required',
            'price_to_sale' => 'required',
            'product_cost' => 'required',
            'user_id' => 'required',
    
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) 
        { 
            return response()->json(['status'=>false,'message'=>'Request Failed','error'=>$validator->errors(),'data'=>'']);
        }

        /*$Input =  [];
        $Input['user_id'] = trim($request->user_id);
        $Input['product_name'] = trim($request->product_name);
        $Input['product_desc'] = trim($request->product_desc);
        $Input['product_cat'] = trim($request->product_cat);
       
        $Input['product_unit'] = trim($request->product_unit);
        $Input['initial_stock'] = trim($request->initial_stock);
        $Input['price_to_sale'] = trim($request->price_to_sale);
        $Input['product_cost'] = trim($request->product_cost);
        $Input['product_status'] = trim($request->product_status);
        $Input['is_deleted'] = "0";
        $Input['created_at'] = date('Y-m-d h:i:s');
        
        if ($request->hasfile('product_img')){
            $file = $request->file('product_img');
            $filename = $request->file('product_img')->getClientOriginalName();
       // Get the contents of the file
       //$contents = $file->openFile()->fread($file->getSize());

       // Store the contents to the database
            $Input['product_img'] = $filename;

        
   
       //Move Uploaded File
           $destinationPath = 'uploads/product_images';
           $file->move($destinationPath,$file->getClientOriginalName());
        }*/
        
        $isproduct = Product::find($id);
        $isproduct->product_name = $request->product_name;
        $isproduct->product_desc = $request->product_desc;
        $isproduct->product_cat = $request->product_cat;
        $isproduct->product_unit = $request->product_unit;
        $isproduct->initial_stock = $request->initial_stock;
        $isproduct->price_to_sale = $request->price_to_sale;
        $isproduct->product_cost = $request->product_cost;
        $isproduct->product_status = $request->product_status;
        $isproduct->user_id = $request->user_id;
        $isproduct->tax_rate = $request->tax_rate;

        // Check if a profile image has been uploaded
        if ($request->hasfile('product_img')) {
             $file = $request->file('product_img');
            $filename = $request->file('product_img')->getClientOriginalName();
       
       //Move Uploaded File
           $destinationPath = 'uploads/images';
            if(!file_exists( 'uploads/images/'.$filename )) {
              $file->move($destinationPath,$file->getClientOriginalName());
            }
           //$file->move($destinationPath,$file->getClientOriginalName());
           $isproduct->product_img = $filename;
        } else{
           $cat_id = trim($request->product_cat);
          $category = Category::where('id',$cat_id)->where('category_status','1')->where('is_deleted','0')->get()->first();
          $isproduct->product_img = $category->category_img;
        }
        // Persist user record to database
        $isproduct->save();

        return response()->json(['status'=>true,'message'=>'Product Updated Successfully','data'=>$isproduct]);


      /*$isproduct = Product::find($id);

      if($request->file('product_img') != ''){        
            $path = 'uploads/product_images';


            //upload new file
            $file = $request->file('product_img');
            $filename = $file->getClientOriginalName();
            $file->move($path, $filename);

            //for update in table
            $isproduct->update(['product_img' => $filename]);
      }*/
        

      /* $product_data = Product::where('id', $id)->update($Input);
       $product_detail = Product::where('id', $id)->first()->toArray();
       if($product_detail) {
            //$from_email = env('MAIL_FROM_ADDRESS');
            //$email = $request->email;

        return response()->json(['status'=>true,'message'=>'Product Updated Successfully','data'=>$product_detail]);
        } else {
        return response()->json(['status'=>false,'message'=>'Product Not Updated! Something Went wrong','error'=>$validator->errors(),'data'=>'']);
        } 
*/

      /*$isproduct = Product::find($id);

      if($isproduct) {
        $rules = [
            // 'username' => 'unique:users',
            'product_name' => 'required',
            'product_desc' => 'required',
            'product_cat' => 'required',
            'product_unit' => 'required',
            'initial_stock' => 'required',
            'price_to_sale' => 'required',
            'product_cost' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) 
        { 
            return response()->json(['status'=>false,'message'=>'Request Failed','error'=>$validator->errors(),'data'=>'']);
        }

        $Input =  [];
        if(empty($request->file('product_img'))) {
          $Input['product_img'] = $isproduct['product_img'];
        } else{

           $file = $request->file('product_img');

        $filename = $request->file('product_img')->getClientOriginalName();
       // Get the contents of the file
       //$contents = $file->openFile()->fread($file->getSize());

       // Store the contents to the database
        $Input['product_img'] = $filename;

        

       //Move Uploaded File
       $destinationPath = 'uploads/product_images';
       $file->move($destinationPath,$file->getClientOriginalName());

        }

        if(empty($request->user_id)) {
          $Input['user_id'] = $isproduct['user_id'];
        } else {
           $Input['user_id'] = trim($request->user_id);
        }

        if(empty($request->product_name)) {
          $Input['product_name'] = $isproduct['product_name'];
        } else {
           $Input['product_name'] = trim($request->product_name);
        }

        if(empty($request->product_desc)) {
          $Input['product_desc'] = $isproduct['product_desc'];
        } else {
           $Input['product_desc'] = trim($request->product_desc);
        }

        if(empty($request->product_cat)) {
          $Input['product_cat'] = $isproduct['product_cat'];
        } else {
           $Input['product_cat'] = trim($request->product_cat);
        }

        if(empty($request->product_unit)) {
          $Input['product_unit'] = $isproduct['product_unit'];
        } else {
           $Input['product_unit'] = trim($request->product_unit);
        }

        if(empty($request->initial_stock)) {
          $Input['initial_stock'] = $isproduct['initial_stock'];
        } else {
           $Input['initial_stock'] = trim($request->initial_stock);
        }

        if(empty($request->price_to_sale)) {
          $Input['price_to_sale'] = $isproduct['price_to_sale'];
        } else {
           $Input['price_to_sale'] = trim($request->price_to_sale);
        }

        if(empty($request->product_cost)) {
          $Input['product_cost'] = $isproduct['product_cost'];
        } else {
           $Input['product_cost'] = trim($request->product_cost);
        }
       
  
        $Input['product_status'] = trim($request->product_status);
        $Input['is_deleted'] = "0";
        $Input['updated_at'] = date('Y-m-d h:i:s');

       

       $product_data = Product::where('id', $id)->update($Input);
       $product_detail = Product::where('id', $id)->first()->toArray();
       if($product_detail) {
            //$from_email = env('MAIL_FROM_ADDRESS');
            //$email = $request->email;

        return response()->json(['status'=>true,'message'=>'Product Updated Successfully','data'=>$product_detail]);
        } else {
        return response()->json(['status'=>false,'message'=>'Product Not Updated! Something Went wrong','error'=>$validator->errors(),'data'=>'']);
        } 
      }else {
        return response()->json(['status'=>false,'message'=>'Product not found','error'=>'','data'=>''], 400);
      }
      */
      
    }

    public function deleteproduct(request $request,$id)
    {
        $isproduct = Product::find($id);
        if($isproduct) {

        $product = Product::where('id',$id)->where('is_deleted','0')->get();
        if($product) {
            $Input =  [];
            $Input['is_deleted'] = "1";

           $product_data = Product::where('id', $id)->update($Input);
           $product_detail = Product::where('id', $id)->first()->toArray();
           if($product_detail) {
                //$from_email = env('MAIL_FROM_ADDRESS');
                //$email = $request->email;

            return response()->json(['status'=>true,'message'=>'Product Deleted Successfully','data'=>$product_detail]);
            } else {
            return response()->json(['status'=>false,'message'=>'Product Not Deleted! Something Went wrong','error'=>$validator->errors(),'data'=>'']);
            } 

        
        } 
        
      } else {
        return response()->json(['status'=>false,'message'=>'Product not found','error'=>'','data'=>''], 400);
      }
        
    }

    public function searchproduct(request $request)
    {
        $srch_prdt = trim($request->srch_product);
        $role = $request->role;
        $user_id = $request->user_id;
        
        if($role == "1") {
          $product_details = Product::where('product_name','LIKE', '%'.$srch_prdt.'%')->where('is_deleted','0')->orderBy('id', 'DESC')->get();
          if($product_details) {
            return response()->json(['status'=>true,'message'=>'Search Products','data'=>$product_details]);
          } else {
             return response()->json(['status'=>false,'message'=>'No Product found!','data'=>'']);
          }
        } else if($role == "2") {
          $product_details = Product::where('user_id','=',$user_id)->where('product_name','LIKE', '%'.$srch_prdt.'%')->where('is_deleted','=','0')->orderBy('id', 'DESC')->get();
          if($product_details) {
            return response()->json(['status'=>true,'message'=>'Search Products','data'=>$product_details]);
          } else {
             return response()->json(['status'=>false,'message'=>'No Product found!','data'=>'']);
          }

        } else if($role == "3") {
           $user = DB::table('user')->select('user.*')->where('id',$user_id)->get()->first();
          
          //$products = Product::where('product_cat',$id)->where('user_id',$user->invited_from)->where('product_status','1')->where('is_deleted','0')->orderBy('id', 'DESC')->get();
            $product_details = Product::where('product_name','LIKE', '%'.$srch_prdt.'%')->where('product_status','1')->where('is_deleted','0')->orderBy('id', 'DESC')->get();
          if($product_details) {
            return response()->json(['status'=>true,'message'=>'Search Products','data'=>$product_details]);
          } else {
             return response()->json(['status'=>false,'message'=>'No Product found!','data'=>'']);
          }
        }


        //$product_details = Product::where('product_name','LIKE', '%'.$srch_prdt.'%')->orWhere('product_cat','LIKE', '%'.$srch_prdt.'%')->orWhere('product_unit','LIKE', '%'.$srch_prdt.'%')->orWhere('price_to_sale','LIKE', '%'.$srch_prdt.'%')->where('product_status','LIKE','1')->where('is_deleted','0')->orderBy('id', 'DESC')->get();
        
        /*$product_details = Product::where('product_name','LIKE', '%'.$srch_prdt.'%')->orWhere('price_to_sale','LIKE', '%'.$srch_prdt.'%')->where('product_status','1')->where('is_deleted','0')->orderBy('id', 'DESC')->get();
         if($product_details) {
            return response()->json(['status'=>true,'message'=>'Search Products','data'=>$product_details]);
        } else {
             return response()->json(['status'=>false,'message'=>'No Product found!','data'=>'']);
        }*/
         
    }

    public function getrecentproducts(request $request)
    { 
        $role = $request->role;
        $user_id = $request->user_id;
        
        if($role == "1") {
          $rcnt_products = Product::where('product_status','1')->where('is_deleted','0')->orderBy('id', 'DESC')->limit(10)->get();
            if($rcnt_products) {
              return response()->json(['status'=>true,'message'=>'Recent Products','data'=>$rcnt_products]);
            } else {
               return response()->json(['status'=>false,'message'=>'No Product found!','data'=>'']);
            } 
        } else if($role == "2"){
          $rcnt_products = Product::where('product_status','1')->where('user_id',$user_id)->where('is_deleted','0')->orderBy('id', 'DESC')->limit(10)->get();
          if($rcnt_products) {
            return response()->json(['status'=>true,'message'=>'Recent Products','data'=>$rcnt_products]);
          } else {
             return response()->json(['status'=>false,'message'=>'No Product found!','data'=>'']);
          } 
        } else if($role == "3"){
           $user = DB::table('user')->select('user.*')->where('id',$user_id)->get()->first();
          
          $rcnt_products = Product::where('user_id',$user->invited_from)->where('product_status','1')->where('is_deleted','0')->orderBy('id', 'DESC')->get();
           //$rcnt_products = Product::where('product_status','1')->where('is_deleted','0')->orderBy('id', 'DESC')->limit(10)->get();
            if($rcnt_products) {
              return response()->json(['status'=>true,'message'=>'Recent Products','data'=>$rcnt_products]);
            } else {
               return response()->json(['status'=>false,'message'=>'No Product found!','data'=>'']);
            } 
        }
        
    }

    public function getproductsbyhighprice(request $request)
    {   
        $role = $request->role;
        $user_id = $request->user_id;

        if($role == "1") {
          $rcnt_products = Product::where('product_status','1')->where('is_deleted','0')->orderBy('price_to_sale','DESC')->get();
          if($rcnt_products) {
            return response()->json(['status'=>true,'message'=>'Products from high to low price','data'=>$rcnt_products]);
          } else {
            return response()->json(['status'=>false,'message'=>'No Product found!','data'=>'']);
          } 
        } else if($role == "2"){
          $rcnt_products = Product::where('product_status','1')->where('user_id',$user_id)->where('is_deleted','0')->orderBy('price_to_sale','DESC')->get();
          if($rcnt_products) {
            return response()->json(['status'=>true,'message'=>'Products from high to low price','data'=>$rcnt_products]);
          } else {
            return response()->json(['status'=>false,'message'=>'No Product found!','data'=>'']);
          } 
        } else if($role == "3") {
           $user = DB::table('user')->select('user.*')->where('id',$user_id)->get()->first();
          
          //$products = Product::where('product_cat',$id)->where('user_id',$user->invited_from)->where('product_status','1')->where('is_deleted','0')->orderBy('id', 'DESC')->get();
           $rcnt_products = Product::where('product_status','1')->where('is_deleted','0')->orderBy('price_to_sale','DESC')->get();
          if($rcnt_products) {
            return response()->json(['status'=>true,'message'=>'Products from high to low price','data'=>$rcnt_products]);
          } else {
            return response()->json(['status'=>false,'message'=>'No Product found!','data'=>'']);
          } 
        }

        
    }

    public function getproductsbylowprice(request $request)
    {   
        $role = $request->role;
        $user_id = $request->user_id;

        if($role == "1") {
          $rcnt_products = Product::where('product_status','1')->where('is_deleted','0')->orderBy('price_to_sale','ASC')->get();
          if($rcnt_products) {
            return response()->json(['status'=>true,'message'=>'Products from low to high price','data'=>$rcnt_products]);
          } else {
            return response()->json(['status'=>false,'message'=>'No Product found!','data'=>'']);
          } 
        } else if($role == "2"){
          $rcnt_products = Product::where('product_status','1')->where('user_id',$user_id)->where('is_deleted','0')->orderBy('price_to_sale','ASC')->get();
          if($rcnt_products) {
            return response()->json(['status'=>true,'message'=>'Products from low to high price','data'=>$rcnt_products]);
          } else {
            return response()->json(['status'=>false,'message'=>'No Product found!','data'=>'']);
          } 
        } else if($role == "3") {
           $user = DB::table('user')->select('user.*')->where('id',$user_id)->get()->first();
          
          //$products = Product::where('product_cat',$id)->where('user_id',$user->invited_from)->where('product_status','1')->where('is_deleted','0')->orderBy('id', 'DESC')->get();
           $rcnt_products = Product::where('product_status','1')->where('is_deleted','0')->orderBy('price_to_sale','ASC')->get();
          if($rcnt_products) {
            return response()->json(['status'=>true,'message'=>'Products from low to high price','data'=>$rcnt_products]);
          } else {
            return response()->json(['status'=>false,'message'=>'No Product found!','data'=>'']);
          } 
        }
    }

    public function getallproductsimages(request $request)
    {  
      $user_id = $request->user_id;
      $role = $request->role;

      if($role == 1) {
        $products = DB::table('product')->select('product_img')->distinct()->groupBy('product.product_img')->get();
      //$products = Product::paginate(10);

      return response()->json(['status'=>true,'message'=>'All Products data','pro_data'=>$products]);

      } else {
        $products = DB::table('product')->select('product_img')->where('user_id',$user_id)->distinct()->groupBy('product.product_img')->get();
      //$products = Product::paginate(10);

      return response()->json(['status'=>true,'message'=>'All Products data','pro_data'=>$products]);

      }
      

    }

    public function getallcategoriesimages(request $request)
    {   
      $categories = DB::table('category')->select('category_img')->distinct()->groupBy('category.category_img')->get();
      //$categories = Category::paginate(10);

      return response()->json(['status'=>true,'message'=>'All Categories data','cat_data'=>$categories]);

    }

    public function addviewproducts(request $request)
    {   
        $Input =  [];
        $Input['t_product_id'] = trim($request->t_product_id);
        $Input['t_user_id'] = trim($request->t_user_id);
        $Input['is_visited'] = "1";
        $Input['is_purchased'] = "0";
        //$Input['created_at'] = date('Y-m-d h:i:s');
        

       $product_data = Trendingproduct::create($Input);
       if($product_data) {
          //$from_email = env('MAIL_FROM_ADDRESS');
            //$email = $request->email;

          return response()->json(['status'=>true,'data'=>$product_data]);
        } else {
          return response()->json(['status'=>false,'error'=>$validator->errors(),'data'=>'']);
        } 

    }

    public function updateviewtrendingproducts(request $request)
    {   
      $product_id = trim($request->product_id);
      $user_id = trim($request->user_id);
      $trending_product_data = Trendingproduct::where('t_product_id',$product_id)->where('t_user_id',$user_id)->orderBy('id','desc')->get()->first();
      //return response()->json(['status'=>true,'data'=>$trending_product_data]);

      if($trending_product_data){
        $last_inserted_id = $trending_product_data->id;
        $Input =  [];
        $Input['is_purchased'] = "1";
        $Input['updated_at'] = date('Y-m-d h:i:s');
        

       $product_data = Trendingproduct::where('id',$last_inserted_id)->update($Input);
       /*if($product_data) {
          //$from_email = env('MAIL_FROM_ADDRESS');
            //$email = $request->email;

          return response()->json(['status'=>true,'data'=>$product_data]);
        } else {
          return response()->json(['status'=>false,'error'=>$validator->errors(),'data'=>'']);
        }
*/

      }

    }

    public function getmosttrendingproducts(request $request)
    {   
      $user_id = trim($request->user_id);
      $user = DB::table('user')->select('user.*')->where('id',$user_id)->get()->first();

      $trending_products = Trendingproduct::distinct()->get(['t_product_id']);

      $productsarr = array();
      foreach($trending_products as $tproducts){
        //$product_data = Product::where('id',$tproducts->t_product_id)->where('product_status','1')->where('is_deleted','0')->get();
        $product_data = DB::table('trending_product')
                 ->select(DB::raw('count(*) as product_count'),'t_product_id')
                 ->where('t_product_id', $tproducts->t_product_id)
                 ->groupBy('t_product_id')
                 ->get();
        array_push($productsarr, $product_data);
      }
      rsort($productsarr);
      //$output = array_slice($productsarr, 0, 10);

      $trending_prdts = array();
      foreach($productsarr as $parr){
          $product_id = $parr[0]->t_product_id;
          $isproduct=DB::table('product')->select('product.*')->where('id',$product_id)->where('user_id',$user->invited_from)->where('product_status','1')->where('is_deleted','0')->first();
          array_push($trending_prdts,$isproduct);
      }
       $filter_products = array_filter($trending_prdts);
       $array = array_values($filter_products);
       $output = array_slice($array, 0, 10);

      if($output){
        return response()->json(['status'=>true,'message'=>'Most Trending Products!','data'=>$output]);
      }else {
        return response()->json(['status'=>false,'message'=>'Product Not Found!','error'=>'','data'=>'']);
      }

      /*$isproduct=DB::table('product')->select('product.*')->join('trending_product','trending_product.t_product_id','=','product.id')->distinct()->get(['trending_product.t_product_id']);

      if($isproduct){
        return response()->json(['status'=>true,'message'=>'Most Trending Products!','data'=>$isproduct]);
      }else {
        return response()->json(['status'=>false,'message'=>'Product Not Found!','error'=>$validator->errors(),'data'=>'']);
      }*/
      
     

      
      
    }

    public function getallcities(request $request)
    {   
      $cities = DB::table('city')->orderBy('city_name')->get();
      return response()->json(['status'=>true,'message'=>'All cities','data'=>$cities]);

    }


   
}