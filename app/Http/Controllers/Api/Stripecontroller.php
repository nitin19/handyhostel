<?php 
namespace App\Http\Controllers\Api;
   
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Config;
//use Stripe;
use Stripe\Stripe;
use Stripe\Customer;
use Stripe\Charge;
use App\Models\Order;
use App\Models\User;
use App\Models\Product;
use App\Models\Notification;
use App\Classes\FirebasenotificationClass;

use Session;
//require_once('/home/kv5qv3c3gs2z/public_html/blog/vendor/autoload.php'); 
//require_once('/home/kv5qv3c3gs2z/public_html/blog/vendor/stripe/stripe-php/init.php');

class Stripecontroller extends Controller
{
    /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
     */
    /*public function stripe()
    {
        return view('stripepayment.stripe');
    }*/
  
    /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
     */
    public function stripePost(Request $request)
    {
        //return base_path();
        $totalamount = $request->amount * 100;
        $postal_code = $request->postal_code;
        $phone_number = $request->phone_number;
        $card_holder_name = $request->holder_name;
        $hostel_address = $request->hostel_address;
        $product_name = $request->product_name;
        $order_id = $request->order_id;
        $order_display_id = $request->order_display_id;
        $additional_notes = $request->additional_notes;
        if(empty($additional_notes)){
            $additional_notes = "...";
        }
        $exp_mnth = $request->exp_mnth;
        $exp_yr = $request->exp_yr;
        $desc = "Payment from ".$card_holder_name;

        $order_address_details = array($hostel_address,$postal_code,$phone_number);

        Stripe::setApiKey(env('STRIPE_SECRET'));
        $charges = \Stripe\Charge::create ([
                "amount" => $totalamount,
                "currency" => "eur",
                "source" => $request->stripeToken,
                "description" =>  $desc,
                "metadata" => array(
                            'order_id' => $request->order_display_id,
                            'product_name' => $product_name,
                            'phone_number' => $phone_number,
                            'hostel_adress' => $hostel_address,
                            'postal_code' => $postal_code,
                             
                            )  
        ]);


        $chargeJson = $charges->jsonSerialize();

        $card_number = $charges->source->last4;
        $payment_status = $chargeJson['status'];


        $transactionID = $chargeJson['balance_transaction']; 
        $paidAmount = $chargeJson['amount']; 
        $paidCurrency = $chargeJson['currency']; 
         
        if($payment_status == 'succeeded'){
            
            $order_d = Order::where('order_display_id', $order_display_id)->get()->toArray();
            //return response()->json(['status'=>true,'message'=>'Payment successful!','data'=>$order_d]);


            foreach($order_d as $od) {
                $product_id = $od['order_product_id'];
                $prod_quantity = $od['order_prod_quantity'];
                $prod_price = $od['order_prod_price'];

                //return response()->json(['status'=>true,'message'=>'Payment successful!','data'=>$product_id]);
                $product_d = Product::where('id', $product_id)->first();
                
                $initial_stock = $product_d->initial_stock;
                $available_stock = $initial_stock - $prod_quantity;
                $sold_products = $product_d->number_of_product_sold + $prod_quantity;

                $prdt =  [];
                $prdt['initial_stock'] = $available_stock;
                $prdt['number_of_product_sold'] = $sold_products;
                $pr_data = Product::where('id', $product_id)->update($prdt);

                $Input =  [];
                $Input['order_status'] = "1";
                $Input['payment_status'] = "1";
                $Input['card_number'] = $card_number;
                $Input['exp_mnth'] = $exp_mnth;
                $Input['exp_yr'] = $exp_yr;
                $Input['additional_notes'] = $additional_notes;
                
                //$Input['payment_method'] = " ";
                $Input['transaction_id'] = $transactionID;
                $Input['updated_at'] = date('Y-m-d h:i:s');
                $Input['order_address_details'] = serialize($order_address_details);

                $order_data = Order::where('order_display_id', $order_display_id)->update($Input);



            }

            $order_user_d = Order::where('id', $order_id)->first();

            $userdata = User::where('id', $order_user_d->order_user_id)->first();

            $notify =  [];
            $notify['notification_type'] = "payment";
            $notify['notification_title'] = "payment";
            $notify['to_user'] = $userdata->invited_from;
            $notify['from_user'] = $order_user_d->order_user_id;
            $notify['table_id'] = $order_user_d->id;
            $notify['icon'] = "payment.png";
            $notify['message'] = "Payment Successfull for Order ".$order_user_d->id."";
            $notify['is_read'] = "0";

            $ndata = Notification::create($notify);

            $touserdata = User::where('id', $ndata->to_user)->first();

            $to_token = $touserdata->device_token;

            $title = 'Payment';
            $type = $ndata->notification_type;
            $message = $ndata->message;


            $notification = array('title' =>$title , 'text' => $message);

            $fields = array(
                'to' => $to_token,
                'data' => array(
                    "type" => $type,
                    "title" => $title,
                    "message" => $message,
                    "notification_id" => $ndata->id,
                    "order_id" => $order_user_d->id
                ),
                'notification' => $notification,
                'priority' => 'high',
            );
             

            $notificationClass = new FirebasenotificationClass();
            $notification_response = $notificationClass->sendnotification($fields);
          
        }
  
       // Session::flash('success', 'Payment successful!');
          
        //return back();
        return response()->json(['status'=>true,'message'=>'Payment successful!','data'=>$chargeJson]);
    }

   /* public function stripePost(Request $request)
    {
        //return base_path();
        $user_email = $request->user_email;
        $totalamount = $request->amount * 100;
        $order_id = $request->order_id;
        $stripeToken = $request->stripeToken;

        $cc_num = $request->card_number;
        $last_four = substr ($cc_num, -4);
        $credit_card_num = "**** **** 888".$last_four;
    
        Stripe::setApiKey(env('STRIPE_SECRET'));
        $customer = \Stripe\Customer::create(array(
        'email' => $user_email,
        'source' => $stripeToken,
        ));

        $charges = \Stripe\Charge::create ([
                "customer" => $customer->id,
                "amount" => $totalamount,
                "currency" => "usd",
                "source" => $request->stripeToken,
                "description" =>  $request->product_name,
                'metadata' => array(
                            'order_id' => $request->order_id
                            ) 
        ]);

        $chargeJson = $charges->jsonSerialize();
        $payment_status = $chargeJson['status'];

        $transactionID = $chargeJson['balance_transaction']; 
        $paidAmount = $chargeJson['amount']; 
        $paidCurrency = $chargeJson['currency']; 
         
        if($payment_status == 'succeeded'){
            $Input =  [];
            $Input['order_status'] = "1";
            $Input['payment_status'] = "1";
            $Input['card_number'] = $credit_card_num;
            $Input['exp_mnth'] = $request->exp_mnth;
            $Input['exp_yr'] = $request->exp_yr;
            //$Input['payment_method'] = " ";
            $Input['transaction_id'] = $transactionID;
            $Input['updated_at'] = date('Y-m-d h:i:s');

            $order_data = Order::where('id', $order_id)->update($Input);
          
        }
            
        

       // Session::flash('success', 'Payment successful!');
          
        //return back();
        return response()->json(['status'=>true,'message'=>'Payment successful!','data'=>$chargeJson]);
    }*/
}