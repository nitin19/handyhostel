<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Config;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;

use App\Models\Category;


class CategoryController extends Controller
{
use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

   /* public function __construct()
    {
        $this->middleware('auth');
    }*/

    public function getallcategories(request $request)
    {
      $categories = Category::where('is_deleted','0')->orderBy('category_name','ASC')->get();

      return view('admin.categories')->with('categories',$categories);
        
        
    }

    public function addnewcategory(request $request)
    {
      return view('admin.addcategory');
     
    }

    public function adminaddcategory(request $request)
    {

        if($request->addcategory) {

          if(empty(trim($request->category_name))) {
            return redirect()->back()->with('error',"Please fill required fields.");
          }

	        $Input =  [];
	        $Input['category_name'] = trim($request->category_name);
	        $Input['category_status'] = "1";
	        $Input['is_deleted'] = "0";
	        $Input['created_at'] = date('Y-m-d h:i:s');
           
           if($request->hasfile('category_img')) {
           	$file = $request->file('category_img');

	        $filename = $request->file('category_img')->getClientOriginalName();
	       // Get the contents of the file
	       //$contents = $file->openFile()->fread($file->getSize());

	       // Store the contents to the database
	        $Input['category_img'] = $filename;

	       //Move Uploaded File
	        $destinationPath = 'uploads/images';
	        if(!file_exists( 'uploads/images/'.$filename )) {
	          $file->move($destinationPath,$file->getClientOriginalName());
	        }

          } else {
            return redirect()->back()->with('error',"Please upload image.");
          }
	        
         //$file->move($destinationPath,$file->getClientOriginalName());

            $category_data = Category::create($Input);

            if($category_data) {
            	return redirect()->intended('categories')->with('success','New Category Added Suceesfully.');
            } else {
            	return redirect()->back()->with('error','Something went wrong! Category not Added.');
            }

      }
        
    }

    public function getcategorydetail(request $request, $id)
    {

      $category = Category::where('id',$id)->first();
      return view('admin.updatecategory')->with('category',$category);
        
        
    }

    public function deletecategory(request $request, $id)
    {
      
      $Input =  [];
      $Input['is_deleted'] = '1';
      $category = Category::where('id',$id)->update($Input);
      if($category) {
        return redirect()->back()->with('success','Category Deleted Suceesfully.');
      }else {
        return redirect()->back()->with('error','Something went wrong! Category Not Deleted.');
      }
        
        
    }

    public function updatecategorydetails(Request $request)
    {   
        $id = $request->cat_id;
        
        $Input =  [];
        
        $Input['category_name'] = trim($request->category_name);
        $Input['updated_at'] = date('Y-m-d h:i:s');

        if($request->hasfile('category_img')) {
             $file = $request->file('category_img');
            $filename = $request->file('category_img')->getClientOriginalName();
       
       //Move Uploaded File
           $destinationPath = 'uploads/images';
            if(!file_exists( 'uploads/images/'.$filename )) {
              $file->move($destinationPath,$file->getClientOriginalName());
            }
           //$file->move($destinationPath,$file->getClientOriginalName());
           $Input['category_img'] = $filename;
        }


       $cat_data = Category::where('id', $id)->update($Input);

       if($cat_data) {
           return redirect()->back()->with('success','Category Updated Suceesfully.');
       } else{
           return redirect()->back()->with('success','Something Went wrong! Category not updated.');
       }
       
    }


}