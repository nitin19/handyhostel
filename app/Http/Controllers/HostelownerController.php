<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
//use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Config;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;

use Illuminate\Support\Str;
use App\Mail\SendOwnerDetails;
use App\Classes\ErrorsClass;
use Mail;
use DB;

use App\Models\User;
use App\Models\Emailtemplate;


class HostelownerController extends Controller
{
use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

   /* public function __construct()
    {
        $this->middleware('auth');
    }*/

    public function getallhostelowners(request $request)
    {
      $users = User::where('is_deleted','0')->where('role','2')->where('isactivation_complete','1')->orderBy('id','DESC')->get();

      return view('admin.hostelowners')->with('owners',$users);
        
        
    }

    public function updateuserstatus(request $request)
    {
      $status = $request->status;
      $user_id = $request->userid;

      $Input =  [];
      $Input['status'] = $status;
      $users = User::where('id',$user_id)->update($Input);
      if($users) {
      	return redirect()->back()->with('success','User status updated successfully.');
      }else {
      	return redirect()->back()->with('error','Something Went wrong! User status not updated .');
      }
        
        
    }

    public function getuserprofile(request $request, $id)
    {

      $user = User::where('id',$id)->first();
      return view('admin.userprofile')->with('user',$user);
        
        
    }

    public function deleteuserprofile(request $request, $id)
    {
      
      $Input =  [];
      $Input['is_deleted'] = '1';
      $user = User::where('id',$id)->update($Input);
      if($user) {
        return redirect()->back()->with('success','User Deleted successfully.');
      }else {
        return redirect()->back()->with('error','User not Deleted successfully.');
      }
        
        
    }

    public function addnewhostelowner(request $request)
    {
      
      return view('admin.addnew-owner');
        
        
    }

    public function addnewowner(request $request)
    {
        $user_email = trim($request->email);
        /*$rules = [
            // 'username' => 'unique:users',
            'email' => 'required|unique:user|email',
            'password'=>'required|confirmed',
            'password_confirmation'=>'',
        ];
 
        $messages = [
            // 'username.unique'   =>  'That user name is already in use.',
            'email.unique'   =>  'That email address is already in use.',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) 
        { 
            return redirect()->back()->with('error',$validator->errors());
        }*/
        $user_data = User::WHERE('email', $user_email)->WHERE('is_deleted','0')->first();
        if($user_data) {
           return redirect()->back()->with('error',"Email is already in use.");
        }

        if(empty($request->email) || empty($request->password)) {
          return redirect()->back()->with('error',"Please fill required fields.");
        } else if($request->password != $request->password_confirmation){
          return redirect()->back()->with('error',"Password doesn't match with confirm password.");
        }
       

        $Input =  [];
        $Input['owner_name'] = trim($request->owner_name);
        $Input['email'] = trim($request->email);
        $Input['password'] = trim(Hash::make($request->password));
        $Input['phone_code'] = "";
        $Input['phone_number'] = "";
        $Input['address'] = "";
        $Input['city'] = "";
        $Input['state'] = "";
        $Input['zipcode'] = "";
        $Input['gender'] = "";
        $Input['dob'] = "";
        $Input['otp'] = "";
        $Input['user_img'] = "";
        $Input['hostel_name'] = trim($request->hostel_name);
        $Input['hostel_website'] = trim($request->hostel_website);
        $Input['hostel_location'] = trim($request->hostel_location);
        $Input['number_of_beds'] = trim($request->number_of_beds);
        $Input['bed_number'] = "";
        //$Input['active_code'] = strtolower(Str::random(30));
        $Input['active_code'] = rand(1000,9999);
        $Input['is_invited'] = "0";
        $Input['invited_from'] = "";
        $Input['status'] = "1";
        $Input['isactivation_complete'] = '1';
        $Input['is_deleted'] = '0';
        $Input['role'] = '2';
        $Input['device_token'] = "";
        $Input['created_at'] = date('Y-m-d h:i:s');

        $Input['token'] = sha1(time());
          
        $auth_user = User::create($Input);

        if ($auth_user) 
        { 
            $name = trim($request->owner_name);
            $email = trim($request->email);
            $password = trim($request->password);

            $sendemail = Mail::to($email)->send(new SendOwnerDetails($name,$email,$password));
            return redirect()->intended('hostelowners')->with('success','Hostel Owner Created Successfully.');
        } else {
            return redirect()->back()->with('error','Something Went wrong! User Not Created.');
        }
          
    }

    public function emailtemplate(request $request)
    {
      $email_template = Emailtemplate::where('id','1')->first();

      return view('admin.emailtemplate')->with('email',$email_template);
        
        
    }

    public function updateemailtext(request $request)
    {
      $Input =  [];
      if(empty($request->email_text)) {
           $Input['email_text'] = "";
      } else{
          $Input['email_text'] = $request->email_text;
      }

      
     
      $email = Emailtemplate::where('id','1')->update($Input);
      if($email) {
        return redirect()->back()->with('success','Email Text Updated Successfully.');
      }else {
        return redirect()->back()->with('error','Something Went Wrong! Text Not Updated.');
      }
        
        
    }


}