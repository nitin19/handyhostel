<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Config;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;

use App\Models\Order;
use App\Models\User;
use App\Models\Product;


class OrderController extends Controller
{
use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

   /* public function __construct()
    {
        $this->middleware('auth');
    }*/

    public function getallorders(request $request)
    {
      $orders = Order::where('order_status','1')->where('is_order_deleted','0')->orderBy('id','DESC')->get();

      return view('admin.orders')->with('orders',$orders);
        
        
    }
    public function excel_export_orders(Request $request)
    {
        $allorders = Order::where('order_status','1')->where('is_order_deleted','0')->orderBy('id','DESC')->get();
        $exceldata = array();
            if(count($allorders) > 0){
                foreach($allorders as $orders){
                   $ownername = User::where(['id' => $orders->order_user_id])->pluck('owner_name')->first();
                   $hostel_name = User::where(['id' => $orders->order_user_id])->pluck('hostel_name')->first();
                   $productname = Product::where(['id' => $orders->order_product_id])->pluck('product_name')->first();
                   
                   if($orders->order_status=='1'){
                       $order_status = 'Completed';
                   } else {
                       $order_status = 'Incompleted';
                   }
                   $new_orders_data = [];
                   $new_orders_data['Order Id'] = $orders->order_display_id;
                   $new_orders_data['User'] = $ownername;
                   $new_orders_data['Product'] = $productname;
                   $new_orders_data['Quantity'] = $orders->quantity;
                   $new_orders_data['Hostel Name'] = $hostel_name;
                   $new_orders_data['Status'] = $order_status;
                   array_push( $exceldata,  $new_orders_data);
                }

            }
        $filename = "Export_excel.xls";
        header("Content-Type: application/vnd.ms-excel");
        header("Content-Disposition: attachment; filename=\"$filename\"");
        $isPrintHeader = false;
           if (!empty($exceldata)) {
                foreach ($exceldata as $row) {
                    if (! $isPrintHeader) {
                        echo implode("\t", array_keys($row)) . "\n";
                        $isPrintHeader = true;
                     }
                    echo implode("\t", array_values($row)) . "\n";
                }
            }
        exit();
      
    }
    public function delete_orders($id){
        $delete_order = Order::where('id', $id)->update(['is_order_deleted'=> '1']);
        if($delete_order){
            return back()->with('success','Order deleted successfully!');
        } else {
            return back()->with('error','Order not deleted successfully!');
        }
    }

}