<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Config;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;

use App\Models\Product;


class ProductController extends Controller
{
use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

   /* public function __construct()
    {
        $this->middleware('auth');
    }*/

    public function getallproducts(Request $request)
    {
      $products = Product::where('is_deleted','0')->orderBy('id','DESC')->get();

      return view('admin.products')->with('products',$products);
    }
    public function addproduct(request $request)
    {

      return view('admin.addnewproducts');
    }
    public function addnewproducts(Request $request)
    {
      $Input = [];
      //$Input['user_id']                 = Auth::user()->id;
      $Input['product_name']            = $request->product_name;
      $Input['product_desc']            = $request->product_desc;
      $Input['product_cat']             = $request->product_category;
      $Input['user_id']                 = implode(',',$request->hostel_owner);
      //$Input['product_unit']          = $request->product_unit;
      $Input['product_unit']            = '';
      $Input['initial_stock']           = $request->product_stock;
      $Input['price_to_sale']           = $request->product_sale_price;
      $Input['product_cost']            = $request->product_cost;
      $Input['tax_rate']                = $request->product_tax_rate;
      $Input['number_of_product_sold']  = '0';
      $Input['product_status']          = '1';
      $Input['is_deleted']              = '0';
      $Input['created_at']              = date('Y-m-d h:i:s');
      if($request->hasfile('product_img')) {
        $file = $request->file('product_img');
        $filename = $request->file('product_img')->getClientOriginalName();
        $destinationPath = 'uploads/images';
        if(!file_exists( 'uploads/images/'.$filename )) {
          $file->move($destinationPath,$file->getClientOriginalName());
        }
        $Input['product_img'] = $filename;
      }
      $product = Product::create($Input);
      if($product){
            return redirect()->intended('/addproduct')->with('success','Product  Created Successfully.');
      } else {
            return redirect()->back()->with('error','Something Went wrong! Product Not Created.');;
      }

    }
    public function editproduct($id)
    {
      $productdata = Product::where('id', $id)->first();  
      return view('admin.editproducts', compact('productdata'));
    }
    public function updateproducts(Request $request, $id)
    {
      $Input = [];
      $Input['user_id']                 = Auth::user()->id;
      $Input['product_name']            = $request->edit_product_name;
      $Input['product_desc']            = $request->edit_product_desc;
      $Input['product_cat']             = $request->edit_product_category;
      $Input['user_id']                 = implode(',',$request->edit_hostel_owner);
      $Input['product_unit']            = '';
      //$Input['product_unit']            = $request->edit_product_unit;
      $Input['initial_stock']           = $request->edit_product_stock;
      $Input['price_to_sale']           = $request->edit_product_sale_price;
      $Input['product_cost']            = $request->edit_product_cost;
      $Input['tax_rate']                = $request->edit_product_tax_rate;
/*    $Input['number_of_product_sold']  = '0';
      $Input['product_status']          = '1';
      $Input['is_deleted']              = '0';*/
      $Input['updated_at']              = date('Y-m-d h:i:s');
      if($request->hasfile('edit_product_img')) {
        $file = $request->file('edit_product_img');
        $filename = $request->file('edit_product_img')->getClientOriginalName();
        $destinationPath = 'uploads/images';
        if(!file_exists( 'uploads/images/'.$filename )) {
          $file->move($destinationPath,$file->getClientOriginalName());
        }
        $Input['product_img'] = $filename;
      }
      $product = Product::where('id', $id)->update($Input);
      if($product){
            return redirect()->back()->with('success','Product  Updated Successfully.');
      } else {
            return redirect()->back()->with('error','Something Went wrong! Product Not Updated.');;
      }

    }
    public function excel_export_products(Request $request)
    {
        $allproducts = Product::where('is_deleted','0')->orderBy('id','DESC')->get();
        $exceldata = array();
            if(count($allproducts) > 0){
                foreach($allproducts as $products){
                   $user_ids = explode(',', $products->user_id);
                   $owner_name = DB::table('user')->whereIn('id', $user_ids)->get();
                   $finalusername = '';
                   if($owner_name){
                      foreach($owner_name as $ownername){
                      $finalusername .= $ownername->owner_name.'-';
                      
                      }
                   } else {
                       $finalusername = '';
                   }
                   $finalusername = substr($finalusername, 0, -1);
                   $catname = DB::table('category')->where(['id' => $products->product_cat])->pluck('category_name')->first();
                   
                   if($products->product_status=='1'){
                       $product_status = 'Active';
                   } else {
                       $product_status = 'Deactive';
                   }
                   $product_img = $products->product_img;
                   $path = env('APP_URL');
                   if(empty($product_img)) {
                    $profile_img = $path."/uploads/images/default_product_img.jpeg";
                   } else{
                    $profile_img = $path."/uploads/images/".$product_img ;
                   }  
                   $new_products_data = [];
                   $new_products_data['Image'] = $profile_img;
                   $new_products_data['Name'] = $products->product_name;
                   $new_products_data['Created By'] =  $finalusername;
                   $new_products_data['Description'] = $products->product_desc;
                   $new_products_data['Product Category'] = $catname;
                   $new_products_data['Status'] = $product_status;
                   array_push( $exceldata,  $new_products_data);
                }
            }
        $filename = "Product_Export_excel.xls";
        header("Content-Type: application/vnd.ms-excel");
        header("Content-Disposition: attachment; filename=\"$filename\"");
        $isPrintHeader = false;
           if (!empty($exceldata)) {
                foreach ($exceldata as $row) {
                    if (! $isPrintHeader) {
                        echo implode("\t", array_keys($row)) . "\n";
                        $isPrintHeader = true;
                     }
                    echo implode("\t", array_values($row)) . "\n";
                }
            }
        exit();
      
    }
    public function delete_products($id){
        $delete_products = Product::where('id', $id)->update(['is_deleted'=> '1']);
        if($delete_products){
            return back()->with('success','Product deleted successfully!');
        } else {
            return back()->with('error','Product not deleted successfully!');
        }
    }
}