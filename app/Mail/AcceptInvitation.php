<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AcceptInvitation extends Mailable
{
    use Queueable, SerializesModels;
    public $email;
    public $password;
    public $email_text;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($email,$password,$email_text)
    {
        $this->email = $email;
        $this->password = $password;
        $this->email_text = $email_text;
        // echo $active_code;die;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Handy Hostel Invitation')
                    ->view('mails.acceptinvitation');
    }
}