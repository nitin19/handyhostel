<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendActivationCode extends Mailable
{
    use Queueable, SerializesModels;
    public $email;
    public $name;
    public $active_code;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name, $email,$active_code)
    {
        $this->email = $email;
        $this->name = $name;
        $this->active_code = $active_code;
        // echo $active_code;die;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Your Activation Code')
                    ->view('mails.userActivate');
    }
}