<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendOwnerDetails extends Mailable
{
    use Queueable, SerializesModels;
    public $email;
    public $name;
    public $password;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name, $email,$password)
    {
        $this->email = $email;
        $this->name = $name;
        $this->password = $password;
        // echo $active_code;die;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('HandyHostel Login Details')
                    ->view('mails.ownerdetails');
    }
}