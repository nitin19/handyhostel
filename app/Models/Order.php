<?php

namespace App\models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Order extends Authenticatable
{
    use Notifiable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'order';
    protected $fillable = [
        'order_product_id','order_offer_id','product_cat_id','order_user_id','price','order_status','payment_method','payment_status','quantity','order_prod_quantity','order_prod_price','is_order_deleted','is_order_cancelled','cancel_date','cancel_reason','is_order_returned','return_date','return_reason','additional_notes','card_number','exp_mnth','exp_yr','transaction_id','order_address_details','order_display_id','created_at', 'updated_at',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    /*protected $hidden = [
        'password', 'token',
    ];*/

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
