@include('admin.layouts.header-admin')
@include('admin.layouts.sidebar-admin')
@include('admin.layouts.error-message')
<style>
.form-group {
    width: 30%;
}
</style>
<div class="container-fluid">
    <div class="row page-titles">
    <div class="col-md-6 col-8 align-self-center">
        <h3 class="text-themecolor m-b-0 m-t-0">Add New Category</h3>
    </div>
    </div>

    <div class="add-category-form"> 
        <form action="{{ env('APP_URL') }}/addnewcategory" method="POST" enctype="multipart/form-data">
           @csrf        
            <div class="form-group">
                <label for="title">Category Name</label>
                <input class="form-control" type="text" name="category_name" id="categoryname">
            </div>
            <div class="form-group">
                <label for="body">Category Image</label>
                <input class="form-control" type="file" name="category_img" id="categoryimage">
            </div>
            <input type="submit" name="addcategory" value="ADD" class="btn btn-success">
        </form>
    </div>       
   
</div>
           
@include('admin.layouts.footer-admin')
