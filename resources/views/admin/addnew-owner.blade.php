@include('admin.layouts.header-admin')
@include('admin.layouts.sidebar-admin')
@include('admin.layouts.error-message')
<style>
.form-group {
    width: 30%;
}
</style>
<div class="container-fluid">
    <div class="row page-titles">
    <div class="col-md-6 col-8 align-self-center">
        <h3 class="text-themecolor m-b-0 m-t-0">New Hostel Owner</h3>
    </div>
    </div>

    <div class="add-category-form"> 
        <form action="{{ env('APP_URL') }}/addnewowner" method="POST" enctype="multipart/form-data">
           @csrf        
            <div class="form-group">
                <label for="title">Owner Name</label>
                <input class="form-control" type="text" name="owner_name" id="ownername">
            </div>
            <div class="form-group">
                <label for="body">Email</label>
                <input class="form-control" type="text" name="email" id="email">
            </div>
            <div class="form-group">
                <label for="body">Password</label>
                <input class="form-control" type="password" name="password" id="password">
            </div>
            <div class="form-group">
                <label for="body">Confirm Password</label>
                <input class="form-control" type="password" name="password_confirmation" id="password">
            </div>
            <!-- <div class="form-group">
                <label for="body">Phone Number</label>
                <input class="form-control" type="text" name="phone_number" id="phone_number">
            </div>
            <div class="form-group">
                <label for="body">Address</label>
                <input class="form-control" type="text" name="address" id="address">
            </div>
            <div class="form-group">
                <label for="body">City</label>
                <input class="form-control" type="text" name="city" id="city">
            </div>
            <div class="form-group">
                <label for="body">State</label>
                <input class="form-control" type="text" name="state" id="state">
            </div>
            <div class="form-group">
                <label for="body">Zipcode</label>
                <input class="form-control" type="text" name="zipcode" id="zipcode">
            </div>
            <div class="form-group">
                <label for="body">Gender</label>
                <input class="form-control" type="radio" name="gender" value="Male" class="gender">Male
                <input class="form-control" type="radio" name="gender" value="Female" class="gender">Feale
            </div> -->
           <!--  <div class="form-group">
                <label for="body">Date Of Birth</label>
                <input class="form-control" type="text" name="dob" id="dob">
            </div>
            <div class="form-group">
                <label for="body">User Image</label>
                <input class="form-control" type="file" name="user_img" id="user_img">
            </div> -->
            <div class="form-group">
                <label for="body">Hostel Name</label>
                <input class="form-control" type="text" name="hostel_name" id="hostel_name">
            </div>
            <div class="form-group">
                <label for="body">Hostel Website</label>
                <input class="form-control" type="text" name="hostel_website" id="hostel_website">
            </div>
            <div class="form-group">
                <label for="body">Hostel Location</label>
                <input class="form-control" type="text" name="hostel_location" id="hostel_location">
            </div>
            <div class="form-group">
                <label for="body">Number Of Beds </label>
                <input class="form-control" type="text" onkeypress="return isNumberKey(event)" name="number_of_beds" id="number_of_beds">
            </div>
            <input type="submit" name="addnewowner" value="ADD" class="btn btn-success">
        </form>
    </div>       
   
</div>
<script>
    function isNumberKey(evt){
        var charCode = (evt.which) ? evt.which : evt.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }
</script>
           
@include('admin.layouts.footer-admin')
