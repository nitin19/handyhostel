@include('admin.layouts.header-admin')
@include('admin.layouts.sidebar-admin')
@include('admin.layouts.error-message')

<div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-6 col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">Categories</h3>
            <a class="btn btn-info" role="button" href="{{ env('APP_URL') }}/addcategory">ADD NEW</a>
        </div>
    </div>
   
    <div class="table-row">
        <table id="myTable">   
    <thead>        
        <tr>  
            <th>Image</th>          
            <th>Name</th>         
            <th>Status</th>
            <th>Action</th>  
        </tr>   
    </thead> 
    <tbody>  
        <?php foreach($categories as $category) {  ?>
        <tr>     
            <td><img style="width:30px" src="{{ env('APP_URL') }}/uploads/images/{{ $category->category_img }}"></td>   
            <td>{{ $category->category_name }}</td>  
            <td><?php if($category->category_status == '1'){ echo "Active";} else { echo "Inactive"; } ?></td> 
            <td><i style="color:#009efb!important" class="fa fa-eye" data-toggle="modal" data-target="#myModal_{{ $category->id }}" aria-hidden="true"></i>
             <a href="{{ env('APP_URL') }}/category/{{ $category->id }}"><i style="color:#009efb!important" class="fa fa-edit"  aria-hidden="true"></i></a>
              <a href="{{ env('APP_URL') }}/deletecategory/{{ $category->id }}"><i style="color:#009efb!important" class="fa fa-trash"  aria-hidden="true"></i></a>
            </td>
            <div class="modal fade" id="myModal_{{ $category->id }}" role="dialog">
                <div class="modal-dialog">
                
                  <!-- Modal content-->
                  <div class="modal-content">
                    <div class="modal-header">
                      <h4 class="modal-title">{{ $category->category_name }}</h4>
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                      <p>Name: {{ $category->category_name }}</p>
                      <p><img style="width:40px" src="{{ env('APP_URL') }}/uploads/images/{{ $category->category_img }}"></p>
                      
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                  </div>
                  
                </div>
            </div>    
              
        </tr>
    <?php } ?>
    </tbody>
</table> 
    </div>
    
   
   
</div>
           
@include('admin.layouts.footer-admin')
