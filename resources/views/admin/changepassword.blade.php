<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Login</title>

  <!-- Custom fonts for this template-->
    <link rel="icon" type="image/png" sizes="16x16" href="{{ env('APP_URL') }}/public/admin/assets/images/favicon.png">
    <title>Handy Hostel</title>
    <!-- Bootstrap Core CSS -->
    <link href="{{ env('APP_URL') }}/public/admin/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="{{ env('APP_URL') }}/public/admin/css/style.css" rel="stylesheet">
    <!-- You can change the theme colors from here -->
    <link href="{{ env('APP_URL') }}/public/admin/css/colors/blue.css" id="theme" rel="stylesheet">

</head>

<body class="bg-gradient-primary">

  <div class="container">
    @if (session('success'))
    <div class="container">
        <div class="alert alert-success alert-dismissable">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
          <p>{{{ session('success') }}}</p>
        </div>
    </div>
    @endif

    @if (session('error'))
    <div class="container">
        <div class="alert alert-danger alert-dismissable">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
          <p>{{{ session('error') }}}</p>
        </div>
    </div>
    @endif
    <!-- Outer Row -->
    <div class="row justify-content-center main-boot-2">

      <div class="col-xl-10 col-lg-12 col-md-9">

        <div class="card o-hidden border-0 shadow-lg my-5">
          <div class="card-body p-0 border-color">
            <!-- Nested Row within Card Body -->
            <div class="row">
              <div class="col-lg-6 d-none d-lg-block bg-password-image">
                 <img src="https://handyhostel.com/wp-content/uploads/2019/11/Handy_hostel_updated.png" class="img-responsive">
              </div>
              <div class="col-lg-6">
                <div class="p-5 main-form-box">
                  <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-2 mb-4 heading-pera">Forgot Your Password?</h1>
                    <!-- <p class="mb-4">We get it, stuff happens. Just enter your email address below and we'll send you a link to reset your password!</p> -->
                  </div>
                  <form class="user" method="post" action="{{ env('APP_URL') }}/resetpassword">
                    @csrf
                    
                      <input type="hidden" name="email" value="{{ $_GET['email'] }}">
                      <input type="hidden" name="token" value="{{ $_GET['token'] }}">
              
                    <div class="form-group">
                      <input type="password" name="newpass" class="form-control form-control-user form-3" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Enter New Password...">
                      <i class="fa fa-eye-slash eye" aria-hidden="true"></i>
                    </div>
                    <div class="form-group">
                      <input type="password" name="confpass" class="form-control form-control-user form-3" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Enter Confirm Password...">
                      <i class="fa fa-eye-slash eye2" aria-hidden="true"></i>
                    </div> 
                    <input type="submit" name="resetpass" Value="Reset Password" class="btn btn-primary btn-user btn-block">
                    
                  </form>
                  <hr>
                  <div class="text-center">
                    <a class="small small-2" href="{{ env('APP_URL') }}/login">Go Back</a>
                  </div>
                  <!-- <div class="text-center">
                    <a class="small" href="register.html">Create an Account!</a>
                  </div>
                  <div class="text-center">
                    <a class="small" href="login.html">Already have an account? Login!</a>
                  </div> -->
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>

    </div>

  </div>

</body>
<script src="{{ env('APP_URL') }}/public/admin/assets/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
  
    <script src="{{ env('APP_URL') }}/public/admin/assets/plugins/bootstrap/js/bootstrap.min.js"></script>

</html>
