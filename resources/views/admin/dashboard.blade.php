@include('admin.layouts.header-admin')
@include('admin.layouts.sidebar-admin')
@include('admin.layouts.error-message')

<div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-6 col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">Dashboard</h3>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <!-- Row -->
    <div class="top-part">
    <div class="row">
        <!-- Column -->
        <div class="col-sm-3">
          <div class="main-box-color">
            <div class="row">
              <div class="col-sm-4">
                <div class="icon-box">
                  <i class="fa fa-shopping-bag" aria-hidden="true"></i>
                </div>
              </div>
              <div class="col-sm-8">
                <h2>Total Orders</h2>
                <p><b>{{ $order_count }}</b></p>
              </div>
            </div>
          </div>
        </div>
        <!-- Column -->
       
        <!-- Column -->
        <div class="col-sm-3">
          <div class="main-box-color">
            <div class="row">
              <div class="col-sm-4">
                <div class="icon-box2">
                  <i class="fa fa-user m-r-10" aria-hidden="true"></i>
                </div>
              </div>
              <div class="col-sm-8">
                <h2>Hostel Owners</h2>
                <p><b>{{ $owner_count }}</b> </p>
              </div>
            </div>
          </div>
        </div>
        <!-- Column -->

        <!-- Column -->
        <div class="col-sm-3">
          <div class="main-box-color">
            <div class="row">
              <div class="col-sm-4">
                <div class="icon-box3">
                  <i class="fa fa-users" aria-hidden="true"></i>
                </div>
              </div>
              <div class="col-sm-8">
                <h2>Guest Users</h2>
                <p><b>{{ $guest_count }}</b></p>
              </div>
            </div>
          </div>
        </div>
        <!-- Column -->
 
        <!-- Column -->
        <div class="col-sm-3">
          <div class="main-box-color">
            <div class="row">
              <div class="col-sm-4">
                <div class="icon-box4">
                  <i class="fa fa-thumbs-up" aria-hidden="true"></i>
                </div>
              </div>
              <div class="col-sm-8">
                <h2>Offers</h2>
                <p><b>{{ $offers_count }}</b></p>
              </div>
            </div>
          </div>
        </div>
        <!-- Column -->

    </div>
    </div>

    <!-- Start...................admin-list -->

<!-- Start.........................latest-members -->
<section class="latest-members">
<div class="row">
        <div class="col-sm-4 col-xs-3">
          <div class="back-color"> 
              <div class="card">
              <div class="card-header border-transparent">
                    <h3 class="card-title">Latest Orders</h3>
              </div>

              </div>
              <!-- /.card-header -->
              <div class="card-body p-0">
                <div class="table-responsive">
                  <table class="table m-0">
                    <thead>
                    <tr>
                      <th>Order Number</th>
                      <th>Price</th> 
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach($latest_orders as $lt_order) { ?>
                      <tr>
                      <td>{{ $lt_order->order_display_id }}</td>
                      <td>{{ $lt_order->order_prod_price}}</td>
                     <!--  <td>{{ \App\Models\Product::where(['id' => $lt_order->order_product_id])->pluck('product_name')->first() }}</td> -->
                      </tr>
                    <?php } ?>
                  
                    </tbody>
                  </table>
                </div>
                <!-- /.table-responsive -->
              </div>
              <!-- /.card-body -->
             <div class="card-footer text-center">
                    <a href="{{ env('APP_URL') }}/orders">View All Orders</a>
             </div>
              <!-- /.card-footer -->
            </div>
          </div>
  
  <div class="col-sm-4 col-xs-12">
      <div class="card">
                  <div class="card-header">
                      <h3 class="card-title">Latest Owners</h3>
                  </div>
                  <!-- /.card-header -->
                  <div class="card-body p-0">
                    <ul class="users-list clearfix">
                      <?php foreach($latest_owners as $lt_owner) { 
                    if(empty($lt_owner->user_img)){
                      $user_img = 'default_user_img.jpeg';
                    } else{
                      $user_img = $lt_owner->user_img;
                    }   

                    if(empty($lt_owner->owner_name)) {
                      $owner_name = "N/A";
                    }  else{
                       $name = explode(" ",$lt_owner->owner_name);
                      $owner_name = $name[0];
                    } 
                ?>
                      <li>
                         <img src="{{ env('APP_URL') }}/uploads/images/{{ $user_img }}" alt="User Image">
                        <a class="users-list-name" href="{{ env('APP_URL') }}/user/{{ $lt_owner->id }}">{{ $owner_name }}</a><br>
                        <span class="users-list-date">
                        <?php if($lt_owner->created_at == date('Y-m-d h:i:s')) {
                          echo "Today";
                        }else if($lt_owner->created_at == date('Y-m-d h:i:s',strtotime("-1 days"))){
                          echo "Yesterday";
                        }else{
                          echo date('d, M', strtotime($lt_owner->created_at));
                        }

                        ?>
                          
                        </span>
                      </li>
                    <?php } ?>
                    </ul>
                    <!-- /.users-list -->
                  </div>
                  <!-- /.card-body -->
                  <div class="card-footer text-center">
                    <a href="{{ env('APP_URL') }}/hostelowners">View All Owners</a>
                  </div>
                  <!-- /.card-footer -->
                </div>
  </div>
  
  
  <div class="col-sm-4 col-xs-12">
    <div class="card">
                  <div class="card-header">
                      <h3 class="card-title">Latest Guest Users</h3>
                  </div>
                  <!-- /.card-header -->
                  <div class="card-body p-0">
                    <ul class="users-list clearfix">

                      <?php foreach($latest_users as $lt_user) { 
                    if(empty($lt_user->user_img)){
                      $user_img = 'default_user_img.jpeg';
                    } else{
                      $user_img = $lt_user->user_img;
                    } 

                    if(empty($lt_user->owner_name)) {
                      $owner_name = "N/A";
                    }  else{
                       $name = explode(" ",$lt_user->owner_name);
                      $owner_name = $name[0];
                    } 
                ?>
                      <li>
                        <img src="{{ env('APP_URL') }}/uploads/images/{{ $user_img }}" alt="User Image">
                        <a class="users-list-name" href="{{ env('APP_URL') }}/user/{{ $lt_user->id }}">{{ $owner_name }}</a><br>
                        <span class="users-list-date">
                          <?php if($lt_user->created_at == date('Y-m-d h:i:s')) {
                          echo "Today";
                        }else if($lt_user->created_at == date('Y-m-d h:i:s',strtotime("-1 days"))){
                          echo "Yesterday";
                        }else{
                          echo date('d, M', strtotime($lt_user->created_at));
                        }

                        ?>
                        </span>
                      </li>
                    <?php } ?>
                    </ul>
                    <!-- /.users-list -->
                  </div>
                  <!-- /.card-body -->
                  <div class="card-footer text-center">
                    <a href="{{ env('APP_URL') }}/guestusers">View All Users</a>
                  </div>
                  <!-- /.card-footer -->
                </div>
  </div>
</div>
</section>
<!-- End...........................latest-members -->

<?php
$j=0;
$bararr[] = ['Months', 'Hostel Owners', 'Guest Users'];
foreach($countuserbymonth as $key => $value) {
   $bararr[] = [$key,$countownerbymonth[$j],$value];
   $j++;
}
/*echo "<pre>";
print_r($bararr);
echo "<pre>";*/
?>

    <!--Start...........bar-chart  -->
    <section class="bar-chart">
     <main>
      <h2>Users Chart</h2>
      <h5>Users</h5>
      <div id="bar-chart"></div>
     </main>
    </section>
    <!--End.............bar-chart  -->


    <!-- End.....................admin-list -->
    
    <!-- Row -->
    <!-- Row -->
   

</div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
 <script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script>
google.load("visualization", "1", {packages:["corechart"]});
google.setOnLoadCallback(drawCharts);
function drawCharts() {
  
  // BEGIN BAR CHART
  /*
  // create zero data so the bars will 'grow'
  var barZeroData = google.visualization.arrayToDataTable([
    ['Day', 'Page Views', 'Unique Views'],
    ['Sun',  0,      0],
    ['Mon',  0,      0],
    ['Tue',  0,      0],
    ['Wed',  0,      0],
    ['Thu',  0,      0],
    ['Fri',  0,      0],
    ['Sat',  0,      0]
  ]);
    */
   /* var theMonths = new Array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
    var now = new Date();
    
    var montharr = [];
    for (var i = 0; i < 12; i++) {
      var future = new Date(now.getFullYear(), now.getMonth() + i, 1);
      var month = theMonths[future.getMonth()];
      var year = future.getFullYear();
      //console.log(month, year);
      montharr.push(month);

    }
    console.log(montharr);*/




  // actual bar chart data
  var bararr = '<?php echo json_encode($bararr); ?>';
  var mainarr = JSON.parse(bararr);
 /*var barData = google.visualization.arrayToDataTable([
  ["Months","Hostel Owners","Guest Users"],
  ["2019-11",0,0],
  ["2019-10",0,0],
  ["2019-09",0,0],
  ["2019-08",0,0],
  ["2019-07",0,0],
  ["2019-06",0,0],
  ["2019-05",0,0],
  ["2019-04",0,0],
  ["2019-03",0,0],
  ["2019-02",0,0],
  ["2019-01",0,0],
  ["2019-12",14,6]]);*/
  var barData = google.visualization.arrayToDataTable(mainarr);
  //console.log(barData);
  // set bar chart options
  var barOptions = {
    width: 900,
    focusTarget: 'category',
    backgroundColor: 'transparent',
    colors: ['cornflowerblue', 'tomato'],
    fontName: 'Open Sans',
    chartArea: {
      left: 50,
      top: 10,
      width: '100%',
      height: '70%'
    },
    bar: {
      groupWidth: '80%'
    },
    hAxis: {
      textStyle: {
        fontSize: 11
      }
    },
    vAxis: {
      minValue: 0,
      maxValue: 100,
      baselineColor: '#DDD',
      gridlines: {
        color: '#DDD',
        count: 4
      },
      textStyle: {
        fontSize: 11
      }
    },
    legend: {
      position: 'bottom',
      textStyle: {
        fontSize: 12
      }
    },
    animation: {
      duration: 1200,
      easing: 'out',
            startup: true
    }
  };
  // draw bar chart twice so it animates
  var barChart = new google.visualization.ColumnChart(document.getElementById('bar-chart'));
  //barChart.draw(barZeroData, barOptions);
  barChart.draw(barData, barOptions);
  
}
</script>

@include('admin.layouts.footer-admin')
