@include('admin.layouts.header-admin')
@include('admin.layouts.error-message')
@include('admin.layouts.sidebar-admin')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<div class="container-fluid">
    <div class="row page-titles">
    <div class="col-md-6 col-8 align-self-center">
        <h3 class="text-themecolor m-b-0 m-t-0">Edit Product</h3>
    </div>
    </div>

    <div class="add-product-form"> 
        <form action="{{ env('APP_URL') }}/updateproduct/{{$productdata->id}}" method="POST" enctype="multipart/form-data">
           @csrf        
            <div class="form-group">
                <label for="title">Product Name</label>
                <input class="form-control" type="text" name="edit_product_name" id="edit_product_name" value="{{$productdata->product_name}}">
            </div>
            <div class="form-group">
                <label for="body">Product Description</label>
                <textarea class="form-control" name="edit_product_desc" id="edit_product_desc">{{$productdata->product_desc}}</textarea>
            </div>
            <div class="form-group">
                <label for="body">Product Category</label>
                <?php
                    $product_category = DB::table('category')->where('is_deleted','0')->get();
                ?>
                <select class="form-control" id="edit_product_category" name="edit_product_category">
                    <option value="">Select Product Category</option>
                @foreach($product_category as $product_category_data)
                    <option value="{{$product_category_data->id}}" <?php if($productdata->product_cat==$product_category_data->id){ echo 'selected';}?>>{{$product_category_data->category_name}}</option>
                @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="body">Hostel Name</label>
                <?php
                $user_ids_arr = explode(',', $productdata->user_id);
                ?>
                <select id="edit_hostel_owner" name="edit_hostel_owner[]" class="form-control js-example-basic-multiple" multiple="multiple">
                    <option value="">Select Hostel Owner</option>
                    <?php
                        $hostel_owner_data = DB::table('user')
                            ->where('is_deleted','0')
                            ->where('role','2')
                            ->where('isactivation_complete','1')
                            ->orderBy('id','DESC')
                            ->get();
                    ?>
                    @foreach($hostel_owner_data as $hostelownerdata)
                        <option value="{{$hostelownerdata->id}}" 
                        <?php if(in_array($hostelownerdata->id , $user_ids_arr)){ echo 'selected';}?>>{{$hostelownerdata->owner_name}}</option>
                    @endforeach
                </select>
            </div>
            <!--<div class="form-group">
                <label for="body">Product Unit</label>
                <select class="form-control" id="edit_product_unit" name="edit_product_unit">
                    <option value="">Select Product Unit</option>
                    <option value="sale" <?php //if($productdata->product_unit){echo 'selected';}?>>Sale</option>
                </select>
            </div>-->
            <div class="form-group">
                <label for="body">Product Stock</label>
                <input class="form-control" type="text" name="edit_product_stock" id="edit_product_stock" value="{{$productdata->initial_stock}}">
            </div>
            <div class="form-group">
                <label for="body">Product Sale Price</label>
                <input class="form-control" type="text" name="edit_product_sale_price" id="edit_product_sale_price" value="{{$productdata->price_to_sale}}">
            </div>
            <div class="form-group">
                <label for="body">Product Cost</label>
                <input class="form-control" type="text" name="edit_product_cost" id="edit_product_cost" value="{{$productdata->product_cost}}">
            </div>
            <div class="form-group">
                <label for="body">Product Tax Rate</label>
                <input class="form-control" type="text" name="edit_product_tax_rate" id="edit_product_tax_rate" value="{{$productdata->tax_rate}}">
            </div>
            <div class="product_img_sec">
                <img src="{{url('/')}}/uploads/images/{{$productdata->product_img}}" style="width:150px">
            </div>
            <div class="form-group">
                <label for="body">Product Image</label>
                <input class="form-control" type="file" name="edit_product_img" id="edit_product_img">
            </div>
            <input type="submit" name="editproducts" value="Update" class="btn btn-success">
        </form>
    </div>       
   
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js" defer></script>
<script>
$(document).ready(function() {
    $('.js-example-basic-multiple').select2();
});
</script>
<style>
.select2-selection__rendered li {
    margin-bottom: 5px;
}
</style>
@include('admin.layouts.footer-admin')
