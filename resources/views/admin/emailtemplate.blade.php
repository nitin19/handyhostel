@include('admin.layouts.header-admin')
@include('admin.layouts.sidebar-admin')
@include('admin.layouts.error-message')

<style>
.form-group {
    width: 30%;
}
</style>
<div class="container-fluid">
    <div class="row page-titles">
    <div class="col-md-6 col-8 align-self-center">
        <h3 class="text-themecolor m-b-0 m-t-0">Email Template Text</h3>
    </div>
    </div>

    <div class="add-category-form"> 
        <form action="{{ env('APP_URL') }}/addemailtext" method="POST" enctype="multipart/form-data">
           @csrf        
           <!--  <div class="form-group">
                <label for="title">Owner Name</label>
                <input class="form-control" type="text" name="owner_name" id="ownername">
            </div> -->
            <label for="body">Email Text</label>
            <div class="form-group">
                
                <textarea class="form-control" style="width:70%;height:30%;"  name="email_text">{{ $email->email_text }}</textarea>
            </div>
           
            <input type="submit" name="addemailtext" value="UPDATE" class="btn btn-success">
        </form>
    </div>       
   
</div>
           
@include('admin.layouts.footer-admin')
