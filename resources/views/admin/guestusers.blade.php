@include('admin.layouts.header-admin')
@include('admin.layouts.sidebar-admin')
@include('admin.layouts.error-message')
<div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-6 col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">Guest Users</h3>
        </div>
    </div>
   
    <div class="table-row">
        <table id="myTable">   
    <thead>        
        <tr> 
            <th>Image</th>           
            <th>Name</th>         
            <th>Email</th>          
            <th>Hostel Name</th> 
            <th>Phone</th>
            <th>Invited By</th>
            <th>Status</th>  
            <th>Action</th>
        </tr>   
    </thead> 
    <tbody>  
        <?php foreach($guestusers as $user) {  
            $user_img = $user->user_img;
            $path = env('APP_URL');
            if(empty($user_img)) {
                
                $profile_img = $path."/uploads/images/default_user_img.jpeg";
            } else{
                $profile_img = $path."/uploads/images/".$user->user_img ;
            }

            if(empty($user->owner_name)) {
                
                $user_name = "N/A";
            } else{
                $user_name = $user->owner_name;
            }

        ?>
         <tr>
            <td><img style="width:30px" src="{{ $profile_img }}"></td>       
            <td>{{ $user_name }}</td>
            <td>{{ $user->email }}</td>   
            <td>{{ $user->hostel_name }}</td>
            <td>{{ $user->phone_number }}</td>
            <td>{{ \App\Models\User::where(['id' => $user->invited_from])->pluck('owner_name')->first() }}</td>
            <td><?php
                if($user->status == '1'){  ?>
                    <form method="post" action="{{ $path }}/updateuserstatus">
                    @csrf
                    <input type="hidden" name="status" value="0">
                    <input type="hidden" name="userid" value="{{ $user->id }}">
                    <input class="btn btn-success" name="submit" type="submit" value="Active">
                    </form>

                <?php } else {  ?>
                    <form method="post" action="{{ $path }}/updateuserstatus">
                    @csrf
                    <input type="hidden" name="status" value="1">
                    <input type="hidden" name="userid" value="{{ $user->id }}">
                    <input class="btn btn-danger" name="submit" type="submit" value="InActive">
                    </form>
              
                <?php }  
             ?></td>
            <td><i style="color:#009efb!important" class="fa fa-eye" data-toggle="modal" data-target="#myModal_{{ $user->id }}" aria-hidden="true"></i>
              <a href="{{ env('APP_URL') }}/user/{{ $user->id }}"><i style="color:#009efb!important" class="fa fa-edit"  aria-hidden="true"></i></a>
              <a href="{{ env('APP_URL') }}/deleteuser/{{ $user->id }}" onclick="return confirm('Are you sure to delete this User?')"><i style="color:#009efb!important" class="fa fa-trash"  aria-hidden="true"></i></a>
            </td>
            <div class="modal fade" id="myModal_{{ $user->id }}" role="dialog">
                <div class="modal-dialog">
                <?php  
                   $user_data = \App\Models\User::where(['id' => $user->id])->first()

                ?>
                  <!-- Modal content-->
                  <div class="modal-content">
                    <div class="modal-header">
                      <h4 class="modal-title">{{ $user_data->owner_name }}</h4>
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                      <p>Name: {{ $user_data->owner_name }}</p>
                      <p>Email: {{ $user_data->email }}</p>
                      <p>Phone: {{ $user_data->phone_number }}</p>
                      <p>Hostel Name: {{ $user_data->hostel_name }}</p>
                      <p>Invited By: {{ \App\Models\User::where(['id' => $user_data->invited_from])->pluck('owner_name')->first() }}</p>
                      <p>Address: {{ $user_data->address }}</p>
                      <p>City: {{ $user_data->city }}</p>
                      <p>State: {{ $user_data->state }}</p>
                      <p>Zip Code: {{ $user_data->zipcode }}</p>
                      <p>Gender: {{ $user_data->gender }}</p>
                      <p>Date Of Birth: {{ $user_data->dob }}</p>
                      <p>Number Of Beds: {{ $user_data->number_of_beds }}</p>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                  </div>
                  
                </div>
            </div>   
        </tr>
    <?php } ?>
    </tbody>
</table> 
    </div>
    
   
   
</div>
           
@include('admin.layouts.footer-admin')
