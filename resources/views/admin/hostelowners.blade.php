@include('admin.layouts.header-admin')
@include('admin.layouts.sidebar-admin')
@include('admin.layouts.error-message')

<div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-6 col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">Hostel Owners</h3>
            <a class="btn btn-info" role="button" href="{{ env('APP_URL') }}/addhostelowner">ADD NEW</a>
        </div>
    </div>
   
    <div class="table-row">
        <table id="myTable">   
    <thead>        
        <tr>
            <th>Image</th>            
            <th>Name</th>         
            <th>Email</th>          
            <th>Hostel Name</th> 
            <th>Phone</th>
            <th>Status</th> 
            <th>Action</th> 
        </tr>   
    </thead> 
    <tbody>  
        <?php foreach($owners as $owner) {  
        $user_img = $owner->user_img;
        $path = env('APP_URL');
        if(empty($user_img)) {
            
            $profile_img = $path."/uploads/images/default_user_img.jpeg";
        } else{
            $profile_img = $path."/uploads/images/".$owner->user_img ;
        }
        ?>
        <tr>  
            <td><img style="width:30px" src="{{ $profile_img }}"></td>     
            <td>{{ $owner->owner_name }}</td>
            <td>{{ $owner->email }}</td>   
            <td>{{ $owner->hostel_name }}</td>
            <td>{{ $owner->phone_number }}</td>
            <td><?php 
            if($owner->status == '1'){  ?>
                 <form method="post" action="{{ $path }}/updateuserstatus">
                @csrf
                <input type="hidden" name="status" value="0">
                <input type="hidden" name="userid" value="{{ $owner->id }}">
                <input class="btn btn-success" name="submit" type="submit" value="Active">
                </form>

            <?php } else {  ?>
                <form method="post" action="{{ $path }}/updateuserstatus">
                @csrf
                <input type="hidden" name="status" value="1">
                <input type="hidden" name="userid" value="{{ $owner->id }}">
                <input class="btn btn-danger" name="submit" type="submit" value="InActive">
                </form>
          
            <?php } 
            ?></td>
            <td>
              <i style="color:#009efb!important" class="fa fa-eye" data-toggle="modal" data-target="#myModal_{{ $owner->id }}" aria-hidden="true"></i>
              <a href="{{ env('APP_URL') }}/user/{{ $owner->id }}"><i style="color:#009efb!important" class="fa fa-edit"  aria-hidden="true"></i></a>
              <a href="{{ env('APP_URL') }}/deleteuser/{{ $owner->id }}" onclick="return confirm('Are you sure to delete this User?')"><i style="color:#009efb!important" class="fa fa-trash"  aria-hidden="true"></i></a>

            </td>
            <div class="modal fade" id="myModal_{{ $owner->id }}" role="dialog">
                <div class="modal-dialog">
               <?php  
               $user_data = \App\Models\User::where(['id' => $owner->id])->first() 
               ?>
                  <!-- Modal content-->
                  <div class="modal-content">
                    <div class="modal-header">
                      <h4 class="modal-title">{{ $user_data->owner_name }}</h4>
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                      <p>Name: {{ $user_data->owner_name }}</p>
                      <p>Email: {{ $user_data->email }}</p>
                      <p>Phone: {{ $user_data->phone_number }}</p>
                      <p>Hostel Name: {{ $user_data->hostel_name }}</p>
                      <p>Address: {{ $user_data->address }}</p>
                      <p>City: {{ $user_data->city }}</p>
                      <p>State: {{ $user_data->state }}</p>
                      <p>Zip Code: {{ $user_data->zipcode }}</p>
                      <p>Gender: {{ $user_data->gender }}</p>
                      <p>Date Of Birth: {{ $user_data->dob }}</p>
                      <p>Hostel Website: {{ $user_data->hostel_website }}</p>
                      <p>Hostel Location: {{ $user_data->hostel_location }}</p>
                      <p>Number Of Beds: {{ $user_data->number_of_beds }}</p>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                  </div>
                  
                </div>
            </div>    
        </tr>
    <?php } ?>
    </tbody>
</table> 
    </div>
    
   
   
</div>
           
@include('admin.layouts.footer-admin')
