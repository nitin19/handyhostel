@if (session('success'))
    <div class="container" id="success-messages">
        <div class="alert alert-success alert-dismissable">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
          <p>{{{ session('success') }}}</p>
        </div>
    </div>
    @endif

    @if (session('error'))
    <div class="container" id="success-messages">
        <div class="alert alert-danger alert-dismissable">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
          <p>{{{ session('error') }}}</p>
        </div>
    </div>
    @endif