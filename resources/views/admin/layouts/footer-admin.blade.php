            <footer class="footer text-center">
                © 2019 Handyhostel.com
            </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="{{ env('APP_URL') }}/public/admin/assets/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="{{ env('APP_URL') }}/public/admin/assets/plugins/bootstrap/js/tether.min.js"></script>
    <script src="{{ env('APP_URL') }}/public/admin/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="{{ env('APP_URL') }}/public/admin/js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="{{ env('APP_URL') }}/public/admin/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="{{ env('APP_URL') }}/public/admin/js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="{{ env('APP_URL') }}/public/admin/assets/plugins/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <!--Custom JavaScript -->
    <script src="{{ env('APP_URL') }}/public/admin/js/custom.min.js"></script>
    <!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->
    <!-- Flot Charts JavaScript -->
    <script src="{{ env('APP_URL') }}/public/admin/assets/plugins/flot/jquery.flot.js"></script>
    <script src="{{ env('APP_URL') }}/public/admin/assets/plugins/flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
    <script src="{{ env('APP_URL') }}/public/admin/js/flot-data.js"></script>
    <!-- ============================================================== -->
    <!-- Style switcher -->
    <!-- ============================================================== -->
    <script src="{{ env('APP_URL') }}/public/admin/assets/plugins/styleswitcher/jQuery.style.switcher.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script>
    $(document).ready( function () {
        $('#myTable').DataTable();

        $("#changepasswordbtn").click(function(){
          console.log('test');
          $("#password-change-cont").toggle();
        });
    } );

    $('#success-messages').delay(2000).fadeOut();

    /*setTimeout(function() {
        $('#success-messages').fadeOut('fast');
    }, 5000);*/
    </script>
</body>

</html>