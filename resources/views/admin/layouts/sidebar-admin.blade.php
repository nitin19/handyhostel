<aside class="left-sidebar">
            <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <li>
                    <a href="{{ env('APP_URL') }}/dashboard" class="waves-effect"><i class="fa fa-clock-o m-r-10" aria-hidden="true"></i>Dashboard</a>
                </li>
                <li>
                    <a href="{{ env('APP_URL') }}/hostelowners" class="waves-effect"><i class="fa fa-user m-r-10" aria-hidden="true"></i>Hostel Owners</a>
                </li>
                <li>
                    <a href="{{ env('APP_URL') }}/guestusers" class="waves-effect"><i class="fa fa-user m-r-10" aria-hidden="true"></i>Guest Users</a>
                </li>
                <li>
                    <a href="{{ env('APP_URL') }}/categories" class="waves-effect"><i class="fa fa-list-alt m-r-10" aria-hidden="true"></i>Categories</a>
                </li>
                <li>
                    <a href="{{ env('APP_URL') }}/products" class="waves-effect"><i class="fa fa-product-hunt m-r-10" aria-hidden="true"></i>Products</a>
                </li>
                <li>
                    <a href="{{ env('APP_URL') }}/offers" class="waves-effect"><i class="fa fa-clock-o m-r-10" aria-hidden="true"></i>Offers</a>
                </li>
                <li>
                    <a href="{{ env('APP_URL') }}/orders" class="waves-effect"><i class="fa fa-first-order m-r-10" aria-hidden="true"></i>Orders</a>
                </li>
                <li>
                    <a href="{{ env('APP_URL') }}/emailtemplate" class="waves-effect"><i class="fa fa-envelope m-r-10" aria-hidden="true"></i>Email Template</a>
                </li>
            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</aside>
<div class="page-wrapper">