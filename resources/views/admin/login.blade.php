<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Login</title>

  <!-- Custom fonts for this template-->
    <link rel="icon" type="image/png" sizes="16x16" href="{{ env('APP_URL') }}/public/admin/assets/images/favicon.png">
    <title>Handy Hostel</title>
    <!-- Bootstrap Core CSS -->
    <link href="{{ env('APP_URL') }}/public/admin/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="{{ env('APP_URL') }}/public/admin/css/style.css" rel="stylesheet">
    <!-- You can change the theme colors from here -->
    <link href="{{ env('APP_URL') }}/public/admin/css/colors/blue.css" id="theme" rel="stylesheet">

</head>

<body class="bg-gradient-primary">

  <div class="container">
    <!-- Outer Row -->
    <div class="row justify-content-center main-boot">

      <div class="col-xl-10 col-lg-12 col-md-9">

        <div class="card o-hidden border-0 shadow-lg my-5">
          <div class="card-body p-0 border-color">
            <!-- Nested Row within Card Body -->
            <div class="row">
              <div class="col-lg-6 d-none d-lg-block bg-login-image">
                <img src="https://handyhostel.com/wp-content/uploads/2019/11/Handy_hostel_updated.png" class="img-responsive">
              </div>
              <div class="col-lg-6">
                  
                <div class="p-5 main-form-box">
                  @if (session('success'))
                      {{ session('success') }}
                  @endif

                  
                  <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4 heading-pera">Welcome Back!</h1>
                  </div>
                  <form class="user" method="post" action="{{ env('APP_URL') }}/loginadmin">
                    @csrf
                    <div class="form-group">
                      <input type="email" name="email" class="form-control form-control-user form-3" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Email">
                    </div>
                    <div class="form-group">
                      <input type="password" name="password" class="form-control form-control-user form-3" id="exampleInputPassword" placeholder="Password">
                      <i class="fa fa-eye-slash eye3" onclick="myFunction()" aria-hidden="true"></i>
                    </div>
                    <!-- <div class="form-group">
                      <div class="custom-control custom-checkbox small">
                        <input type="checkbox" class="custom-control-input" id="customCheck">
                        <label class="custom-control-label" for="customCheck">Remember Me</label>
                      </div>
                    </div> -->
                    @if (session('error'))
                      {{ session('error') }}
                  @endif
                    <input type="submit" name="login" value="LOGIN" class="btn btn-primary btn-user btn-block">
                  
                   <!-- <hr>
                    <a href="index.html" class="btn btn-google btn-user btn-block">
                      <i class="fab fa-google fa-fw"></i> Login with Google
                    </a>
                    <a href="" class="btn btn-facebook btn-user btn-block">
                      <i class="fab fa-facebook-f fa-fw"></i> Login with Facebook
                    </a> -->
                  </form>
                  <hr>
                  <div class="text-center">
                    <a class="small small-2" href="{{ env('APP_URL') }}/forgetpassword">Forgot Password?</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>

    </div>

  </div>
</body>
<script src="{{ env('APP_URL') }}/public/admin/assets/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
  
    <script src="{{ env('APP_URL') }}/public/admin/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script>
      function myFunction() {
        var x = document.getElementById("exampleInputPassword");
        if (x.type === "password") {
          x.type = "text";
        } else {
          x.type = "password";
        }
      }
    </script>
    
</html>
