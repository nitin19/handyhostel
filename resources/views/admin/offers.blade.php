<@include('admin.layouts.header-admin')

@include('admin.layouts.sidebar-admin')

<div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-6 col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">Offers</h3>
        </div>
    </div>
   
    <div class="table-row">
        <table id="myTable">   
    <thead>        
        <tr> 
            <th>Image</th>            
            <th>Name</th>
            <th>Created By</th>
            <th>Product Name</th>         
            <th>Description</th>          
            <th>Price</th>
            <th>Status</th>
            <th>Action</th>  
        </tr>   
    </thead> 
    <tbody>  
        <?php foreach($offers as $offer) { 
            $offer_img = $offer->offer_img;
            $path = env('APP_URL');
            if(empty($offer_img)) {
                
                $profile_img = $path."/uploads/images/default_product_img.jpeg";
            } else{
                $profile_img = $path."/uploads/images/".$offer_img ;
            }   

        ?>
        <tr>    
            <td><img style="width:30px" src="{{ $profile_img }}"></td>   
            <td>{{ $offer->offer_name }}</td>
            <td>{{ \App\Models\User::where(['id' => $offer->offer_user_id])->pluck('owner_name')->first() }}</td>
            <td>{{ \App\Models\Product::where(['id' => $offer->offer_product_id])->pluck('product_name')->first() }}</td>
            <td>{{ $offer->offer_desc }}</td>   
            <td>{{ $offer->offer_price }}</td>
            <td><?php if($offer->offer_status == '1'){ echo "Active";} else { echo "Inactive"; } ?></td>
            <td><i style="color:#009efb!important" class="fa fa-eye" data-toggle="modal" data-target="#myModal_{{ $offer->id }}" aria-hidden="true"></i></td>
            <div class="modal fade" id="myModal_{{ $offer->id }}" role="dialog">
                <div class="modal-dialog">
                
                  <!-- Modal content-->
                  <div class="modal-content">
                    <div class="modal-header">
                      <h4 class="modal-title">{{ $offer->offer_name }}</h4>
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                      <p>Name: {{ $offer->offer_name }}</p>
                      <p>Created By: {{ \App\Models\User::where(['id' => $offer->offer_user_id])->pluck('owner_name')->first() }}</p>
                      <p>Product Name: {{ \App\Models\Product::where(['id' => $offer->offer_product_id])->pluck('product_name')->first() }}</p>
                      <p>Offer Description: {{ $offer->offer_desc }}</p>
                      <p>Offer Price: {{ $offer->offer_price }}</p>
                      <p>Offer Start Date: {{ $offer->offer_start_date }}</p>
                      <p>Offer End Date: {{ $offer->offer_end_date }}</p>
                      <p>Offer Start Time: {{ $offer->offer_start_time }}</p>
                      <p>Offer End Time: {{ $offer->offer_end_time }}</p>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                  </div>
                  
                </div>
            </div>     
        </tr>
    <?php } ?>
    </tbody>
</table> 
    </div>
    
   
   
</div>
           
@include('admin.layouts.footer-admin')
