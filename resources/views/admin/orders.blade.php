@include('admin.layouts.header-admin')
@include('admin.layouts.error-message')
@include('admin.layouts.sidebar-admin')

<div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-6 col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">Orders</h3>
            <a class="excel_btn" href="{{url('excelexportorders')}}">Excel</a>
        </div>
    </div>

    <div class="table-row">
        <table id="myTable">   
    <thead>        
        <tr>            
            <th>Order Id</th>         
            <th>User</th>          
            <th>Product</th> 
            <th>Quantity</th>
            <th>Hostal Email</th>
            <th>Created DateTime</th>
            <th>Status</th>
            <th>Action</th>  
        </tr>   
    </thead> 
    <tbody>  
        <?php foreach($orders as $order) {  ?>
        <tr>       
            <td>{{ $order->order_display_id }}</td>
            <td>{{ \App\Models\User::where(['id' => $order->order_user_id])->pluck('owner_name')->first() }}</td>   
            <td>{{ \App\Models\Product::where(['id' => $order->order_product_id])->pluck('product_name')->first() }}</td>
            <td>{{ $order->quantity }}</td>
            <td>{{ \App\Models\User::where(['id' => $order->order_user_id])->pluck('email')->first() }}</td>
            <td>{{ $order->created_at }}</td>
            <td><?php if($order->order_status == '1'){ echo "Completed";} else { echo "InCompleted"; } ?></td> 
            <td><i style="color:#009efb!important" class="fa fa-eye" data-toggle="modal" data-target="#myModal_{{ $order->id }}" aria-hidden="true"></i><a href="{{ url('/deleteorders/'.$order->id)}}"><i style="color:#009efb!important" class="fa fa-trash" aria-hidden="true"></i></a></td>
            <div class="modal fade" id="myModal_{{ $order->id }}" role="dialog">
                <div class="modal-dialog">
                
                  <!-- Modal content-->
                  <div class="modal-content">
                    <div class="modal-header">
                      <h4 class="modal-title">Order Details</h4>
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                      <p>Order ID: {{ $order->order_display_id }}</p>
                      <p>Order By: {{ \App\Models\User::where(['id' => $order->order_user_id])->pluck('owner_name')->first() }}</p>
                      <p>Product Name: {{ \App\Models\Product::where(['id' => $order->order_product_id])->pluck('product_name')->first() }}</p>
                      <p>Quantity: {{ $order->quantity }}</p>
                      
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                  </div>
                  
                </div>
            </div>  
        </tr>
    <?php } ?>
    </tbody>
</table> 
    </div>
    
   
   
</div>
@include('admin.layouts.footer-admin')
