@include('admin.layouts.header-admin')
@include('admin.layouts.error-message')
@include('admin.layouts.sidebar-admin')

<div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-6 col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">Products</h3>
             <a class="add_product_btn" href="{{url('/addproduct')}}">Add Products</a>
             <a class="excel_btn" href="{{url('excelexportproducts')}}">Excel</a>
        </div>
    </div>
   
    <div class="table-row">
        <table id="myTable">   
    <thead>        
        <tr>
            <th>Image</th>            
            <th>Name</th>         
            <th>Created By</th>          
            <th>Description</th>
            <th>Product Category</th> 
            <th>Status</th> 
            <th>Action</th> 
        </tr>   
    </thead> 
    <tbody>  
        <?php foreach($products as $product) {
        $product_img = $product->product_img;
        $path = env('APP_URL');
        if(empty($product_img)) {
            
            $profile_img = $path."/uploads/images/default_product_img.jpeg";
        } else{
            $profile_img = $path."/uploads/images/".$product_img ;
        }  

        ?>
        <tr>   
             <td><img style="width:30px" src="{{ $profile_img }}"></td>    
            <td>{{ $product->product_name }}</td>
            <td>{{ \App\Models\User::where(['id' => $product->user_id])->pluck('owner_name')->first() }}</td>   
            <td>{{ $product->product_desc }}</td>
            <td>{{ \App\Models\Category::where(['id' => $product->product_cat])->pluck('category_name')->first() }}</td>
            <td><?php if($product->product_status == '1'){ echo "Active";} else { echo "Inactive"; } ?></td>
            <td><i style="color:#009efb!important" class="fa fa-eye" data-toggle="modal" data-target="#myModal_{{ $product->id }}" aria-hidden="true"></i><a href="{{ url('/editproduct/'.$product->id)}}"><i style="color:#009efb!important" class="fa fa-edit" aria-hidden="true"></i></a><a href="{{ url('/deleteproducts/'.$product->id)}}"><i style="color:#009efb!important" class="fa fa-trash" aria-hidden="true"></i></a></td>
            <div class="modal fade" id="myModal_{{ $product->id }}" role="dialog">
                <div class="modal-dialog">
                
                  <!-- Modal content-->
                  <div class="modal-content">
                    <div class="modal-header">
                      <h4 class="modal-title">{{ $product->product_name }}</h4>
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                      <p>Name: {{ $product->product_name }}</p>
                      <p>Created By: {{ \App\Models\User::where(['id' => $product->user_id])->pluck('owner_name')->first() }}</p>
                      <p>Product Category: {{ \App\Models\Category::where(['id' => $product->product_cat])->pluck('category_name')->first() }}</p>
                      <p>Product Description: {{ $product->product_desc }}</p>
                     
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                  </div>
                  
                </div>
            </div>   
        </tr>
    <?php } ?>
    </tbody>
</table> 
    </div>
    
   
   
</div>
           
@include('admin.layouts.footer-admin')
