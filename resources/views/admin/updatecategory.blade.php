@include('admin.layouts.header-admin')
@include('admin.layouts.sidebar-admin')
@include('admin.layouts.error-message')
<style>

</style>
<div class="container-fluid">
    <div class="row page-titles">
    <div class="col-md-6 col-8 align-self-center">
        <h3 class="text-themecolor m-b-0 m-t-0">Category</h3>
    </div>
    </div>
    <div class="profile-container"> 
    	<div class="row">
                    <!-- Column -->
            <div class="col-lg-4 col-xlg-3 col-md-5">
                <div class="card">
                    <div class="card-block">
                        <center class="m-t-30"> <img src="{{ env('APP_URL') }}/uploads/images/{{ $category->category_img }}" class="img-circle" width="150" />
                            <h4 class="card-title m-t-10">{{ $category->category_name }}</h4>
                           <!--  <div class="row text-center justify-content-md-center">
                                <div class="col-4"><a href="javascript:void(0)" class="link"><i class="icon-people"></i> <font class="font-medium">254</font></a></div>
                                <div class="col-4"><a href="javascript:void(0)" class="link"><i class="icon-picture"></i> <font class="font-medium">54</font></a></div>
                            </div> -->
                        </center>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <!-- Column -->
            <div class="col-lg-8 col-xlg-9 col-md-7">
                <div class="card">
                    <div class="card-block">
                        <form class="form-horizontal form-material" method="post" action="{{ env('APP_URL') }}\updatecategory" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label class="col-md-12">Category Name</label>
                                <div class="col-md-12">
                                    <input type="hidden" placeholder=" " name="cat_id" value="{{ $category->id }}" class="form-control form-control-line">
                                    <input type="text" placeholder=" " name="category_name" value="{{ $category->category_name }}" class="form-control form-control-line">
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-md-12">Image</label>
                                <div class="col-md-12">
                                    <input type="file" placeholder="" name="category_img" class="form-control form-control-line">
                                </div>
                            </div>
                           
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input type="submit" name="updatecat" value="Update Category" class="btn btn-success">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- Column -->
        </div>

        
    </div>       
   
</div>
           
@include('admin.layouts.footer-admin')
