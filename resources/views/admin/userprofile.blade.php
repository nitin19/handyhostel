@include('admin.layouts.header-admin')
@include('admin.layouts.sidebar-admin')
@include('admin.layouts.error-message')
<style>

</style>
<div class="container-fluid">
    <div class="row page-titles">
    <div class="col-md-6 col-8 align-self-center">
        <h3 class="text-themecolor m-b-0 m-t-0">Profile</h3>
    </div>
    </div>
    <div class="profile-container"> 
    	<div class="row">
                    <!-- Column -->
           <!--  <div class="col-lg-4 col-xlg-3 col-md-5">
                <div class="card">
                    <div class="card-block">

                        <center class="m-t-30"> 
                           <?php if(empty($user->user_img)){ ?>
                               <img src="{{ env('APP_URL') }}/uploads/images/default_user_img.jpeg" class="img-circle" width="150" />

                            <?php } else { ?>
                               <img src="{{ env('APP_URL') }}/uploads/images/{{ $user->user_img }}" class="img-circle" width="150" />
                            <?php } ?>
                            <h4 class="card-title m-t-10">{{ $user->owner_name }}</h4>
                            <h6 class="card-subtitle">Admin</h6>
                        </center>
                    </div>
                </div>
            </div> -->
            <!-- Column -->
            <!-- Column -->
            <div class="col-lg-8 col-xlg-9 col-md-7">
                <div class="card">
                    <div class="card-block">
                        <form class="form-horizontal form-material" method="post" action="{{ env('APP_URL') }}\updateuser" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label class="col-md-12">Full Name</label>
                                <div class="col-md-12">
                                    <input type="hidden" placeholder=" " name="user_id" value="{{ $user->id }}" class="form-control form-control-line">
                                    <input type="text" placeholder=" " name="owner_name" value="{{ $user->owner_name }}" class="form-control form-control-line">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="example-email" class="col-md-12">Email</label>
                                <div class="col-md-12">
                                    <input type="email" name="email" placeholder="" value="{{ $user->email }}" class="form-control form-control-line" name="email" id="example-email" readonly>
                                </div>
                            </div>
                           <!--  <div class="form-group">
                                <label class="col-md-12">Password</label>
                                <div class="col-md-12">
                                    <input type="password" value="password" class="form-control form-control-line">
                                </div>
                            </div> -->
                            <div class="form-group">
                                <label class="col-md-12">Phone No</label>
                                <div class="col-md-12">
                                    <input type="text" placeholder="" value="{{ $user->phone_number }}" name="phone_number" class="form-control form-control-line">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12">Image</label>
                                <div class="col-md-12">
                                    <input type="file" placeholder="" name="user_img" class="form-control form-control-line">
                                </div>
                            </div>
	                        <div class="change-password-container" id="password-change-cont" style="display:none;">
	                            <div class="form-group">
	                                <label class="col-md-12">New Password</label>
	                                <div class="col-md-12">
	                                    <input type="password" placeholder="" name="newpass" value="" class="form-control form-control-line">
	                                </div>
	                            </div>
	                             <div class="form-group">
	                                <label class="col-md-12">Confirm Password</label>
	                                <div class="col-md-12">
	                                    <input type="password" placeholder="" name="confpass" value="" class="form-control form-control-line">
	                                </div>
	                            </div>
	                        </div>
                           
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input type="submit" name="updateadmin" value="Update Profile" class="btn btn-success">
                                    <a href="javascript:void(0);" id="changepasswordbtn" class="btn btn-success">Change Password</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- Column -->
        </div>

        
    </div>       
   
</div>
           
@include('admin.layouts.footer-admin')
