<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
}); */
Route::group(['middleware' => ['api']], function ($router) {

	Route::post('/register','Api\Usercontroller@register');
	Route::post('/login','Api\Usercontroller@login');
	Route::get('logout', 'Api\Usercontroller@logout');
	Route::post('/userActivation', 'Api\Usercontroller@userActivation');
	Route::post('/forgetpassword', 'Api\Usercontroller@forgetpassword');
	Route::post('/resetpassword', 'Api\Usercontroller@Resetpassword')->name('password.update');
	Route::post('/resetuserpassword', 'Api\Usercontroller@passwordReset');
	Route::post('/allusers', 'Api\Usercontroller@getallusers');
    Route::post('/checkotpforgetpassword', 'Api\Usercontroller@checkforgetpasswordotp');
	//Route::get('/currentuser', 'Api\Usercontroller@getcurrentuser');
	Route::get('/user/{id}','Api\Usercontroller@getuserDetails');
    Route::post('/updateuser/{id}','Api\Usercontroller@updateuserDetails');
    Route::post('/deleteuser/{id}','Api\Usercontroller@deleteuser');
    Route::post('/searchuser','Api\Usercontroller@searchuser');

	Route::post('/addcategory','Api\Categorycontroller@addcategory');
	Route::get('/showallcategories','Api\Categorycontroller@showcategories');
	Route::get('/category/{id}','Api\Categorycontroller@getcategoryDetails');
    Route::post('/updatecategory/{id}','Api\Categorycontroller@updatecategoryDetails');
    Route::post('/deletecategory/{id}','Api\Categorycontroller@deletecategory');
    Route::post('/searchcategory','Api\Categorycontroller@searchcategory');
    Route::post('/getcategorywithcount','Api\Categorycontroller@getcategorieswithproductcount');

	Route::post('/addproduct','Api\Productcontroller@addproduct');
	Route::get('/showallproducts','Api\Productcontroller@showproducts');
    Route::get('/product/{id}','Api\Productcontroller@getproductDetails');
    Route::post('/updateproduct/{id}','Api\Productcontroller@updateproductDetails');
    Route::post('/deleteproduct/{id}','Api\Productcontroller@deleteproduct');
    Route::post('/searchproduct','Api\Productcontroller@searchproduct');
    Route::post('/getproductsbyrole','Api\Productcontroller@getproductsbyrole');

    Route::get('/category/{id}/products','Api\Productcontroller@getproductsbycategory');

    Route::post('/recentproducts','Api\Productcontroller@getrecentproducts');
    Route::post('/highpriceproducts','Api\Productcontroller@getproductsbyhighprice');
    Route::post('/lowpriceproducts','Api\Productcontroller@getproductsbylowprice');

    
    Route::get('/allcities','Api\Productcontroller@getallcities');

    Route::post('/allproductimages','Api\Productcontroller@getallproductsimages');
    Route::get('/allcategoryimages','Api\Productcontroller@getallcategoriesimages');

    Route::post('/addviewproduct','Api\Productcontroller@addviewproducts');
    Route::post('/updateviewproduct','Api\Productcontroller@updateviewtrendingproducts');
    Route::get('/mosttrendingproducts','Api\Productcontroller@getmosttrendingproducts');

    Route::post('/addoffer','Api\Offercontroller@addoffer');
	Route::post('/showalloffers','Api\Offercontroller@showoffers');

    Route::post('/shownewoffers','Api\Offercontroller@shownewoffers');
    
    Route::get('/offer/{id}','Api\Offercontroller@getofferDetails');
    Route::post('/updateoffer/{id}','Api\Offercontroller@updateofferDetails');
    Route::post('/deleteoffer/{id}','Api\Offercontroller@deleteoffer');
    Route::post('/searchoffer','Api\Offercontroller@searchoffer');
    Route::post('/getrecentoffers','Api\Offercontroller@getrecentoffers');
    Route::post('/getrecentoffers','Api\Offercontroller@getrecentoffers');
    Route::post('/getoffersbyhighdiscount','Api\Offercontroller@getoffersbyhighdiscount');
    Route::post('/getoffersbylowdiscount','Api\Offercontroller@getoffersbylowdiscount');

    //Route::get('stripe', 'Api\Stripecontroller@stripe');
    Route::post('stripe', 'Api\Stripecontroller@stripePost');
	
	Route::post('/createorder','Api\Ordercontroller@createorder');
    Route::post('/getallorders','Api\Ordercontroller@getallordersbyrole');
    Route::post('/deleteorder/{id}','Api\Ordercontroller@deleteorder');
    Route::get('/order/{id}','Api\Ordercontroller@getorderDetails');
    Route::post('/cancelorder/{id}','Api\Ordercontroller@cancelorder');
    Route::post('/returnorder/{id}','Api\Ordercontroller@returnorder');
    Route::post('/getrecentorders','Api\Ordercontroller@getrecentorders');
    Route::post('/getordersbyhightotalprice','Api\Ordercontroller@getordersbyhightotalprice');
    Route::post('/getordersbylowtotalprice','Api\Ordercontroller@getordersbylowtotalprice');
    Route::post('/getordersbylastweek','Api\Ordercontroller@getordersbylastweek');
    Route::post('/getordersbylastmonth','Api\Ordercontroller@getordersbylastmonth');
    Route::post('/getordersbylastyear','Api\Ordercontroller@getordersbylastyear');

    Route::post('/inviteuser','Api\Invitecontroller@inviteuser');
    Route::get('/acceptinvitation/{token}','Api\Invitecontroller@acceptinvitation');

    Route::post('/sendnotification','Api\Notificationcontroller@sendnotification');
    Route::post('/addnotification','Api\Notificationcontroller@addnotification');
    Route::post('/getallnotifications','Api\Notificationcontroller@getnotifications');

    Route::post('/addproductincart','Api\Cartcontroller@addproductincart');
    Route::post('/showcartproducts','Api\Cartcontroller@showallcartproductsbyid');
    Route::post('/removecartproduct','Api\Cartcontroller@removeproductincart');
    Route::post('/incrementcartquantity','Api\Cartcontroller@incrementcartquantity');
    Route::post('/decrementcartquantity','Api\Cartcontroller@decrementcartquantity');

    Route::get('/allhostelowners','Api\Usercontroller@getallhostelowners');
    Route::post('/addsplash','Api\Usercontroller@addsplash');
    Route::post('/updatesplash','Api\Usercontroller@updatesplash');
    Route::post('/getsplashdetails','Api\Usercontroller@getsplashdetails');

    Route::post('/totalorderrevenue','Api\Usercontroller@gettotalorderrevenue');
    Route::post('/gettotalusers','Api\Usercontroller@gettotalusers');
});
