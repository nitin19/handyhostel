<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', 'Admincontroller@dashboard');
Route::get('/login', 'Admincontroller@login');
Route::post('/loginadmin', 'Admincontroller@authenticatelogin');
Route::get('/logout', 'Admincontroller@logout');
Route::get('/forgetpassword', 'Admincontroller@forgetpassword');
Route::post('/passwordreset', 'Admincontroller@passwordreset');
Route::get('/changepassword', 'Admincontroller@changepassword');
Route::post('/resetpassword', 'Admincontroller@resetpassword');
Route::get('/hostelowners', 'HostelownerController@getallhostelowners');
Route::get('/guestusers', 'GuestuserController@getallguestusers');
Route::get('/categories', 'CategoryController@getallcategories');
Route::get('/products', 'ProductController@getallproducts');
Route::get('/excelexportproducts', 'ProductController@excel_export_products');
Route::get('/addproduct', 'ProductController@addproduct');
Route::post('/addnewproduct', 'ProductController@addnewproducts');
Route::get('/editproduct/{id}', 'ProductController@editproduct');
Route::post('/updateproduct/{id}', 'ProductController@updateproducts');
Route::get('/deleteproducts/{id}', 'ProductController@delete_products');
Route::get('/offers', 'OfferController@getalloffers');
Route::get('/orders', 'OrderController@getallorders');
Route::get('/excelexportorders', 'OrderController@excel_export_orders');
Route::get('/deleteorders/{id}', 'OrderController@delete_orders');
Route::get('/profile', 'Admincontroller@profiledetails');
Route::post('/updateuser', 'Admincontroller@updateuserdetails');
Route::get('/user/{id}', 'HostelownerController@getuserprofile');
Route::get('/deleteuser/{id}', 'HostelownerController@deleteuserprofile');
Route::get('/addhostelowner', 'HostelownerController@addnewhostelowner');
Route::post('/addnewowner', 'HostelownerController@addnewowner');

Route::post('/updateuserstatus', 'HostelownerController@updateuserstatus');
Route::get('/addcategory', 'CategoryController@addnewcategory');
Route::post('/addnewcategory', 'CategoryController@adminaddcategory');
Route::post('/updatecategory', 'CategoryController@updatecategorydetails');
Route::get('/category/{id}', 'CategoryController@getcategorydetail');
Route::get('/deletecategory/{id}', 'CategoryController@deletecategory');
Route::get('/emailtemplate', 'HostelownerController@emailtemplate');
Route::post('/addemailtext', 'HostelownerController@updateemailtext');