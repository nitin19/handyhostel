<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Config;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;

use Illuminate\Support\Str;
use App\Mail\ForgetPassword;
use App\Mail\Sendresetpassword;
use App\Mail\SendActivationCode;
use App\Classes\ErrorsClass;
use App\Classes\FirebasenotificationClass;
use Mail;
use DB;
use App\Models\User;
use App\Models\Notification;
use App\Models\Splash;

class Usercontroller extends Controller
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /* public function __construct()
    {
        $this->middleware('auth');
    }*/

    public function register(request $request){

        

        $rules = [
            // 'username' => 'unique:users',
            'email' => 'required|unique:user|email',
            'password'=>'required|confirmed',
            'password_confirmation'=>'',
            'role' => 'required',
        ];
 
        $messages = [
            // 'username.unique'   =>  'That user name is already in use.',
            'email.unique'   =>  'That email address is already in use.',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) 
        { 
            return response()->json(['status'=>false,'message'=>'Email address already in use','error'=>$validator->errors(),'data'=>'']);
        }
       

        $Input =  [];
        $Input['owner_name'] = trim($request->name);
        $Input['email'] = trim($request->email);
        $Input['password'] = trim(Hash::make($request->password));
        $Input['phone_code'] = trim($request->phone_code);
        $Input['phone_number'] = trim($request->phone);
        $Input['address'] = "";
        $Input['city'] = trim($request->city);
        $Input['state'] = "";
        $Input['zipcode'] = "";
        $Input['gender'] = "";
        $Input['dob'] = "";
        $Input['otp'] = "";
        $Input['user_img'] = "";
        $Input['hostel_name'] = trim($request->hostel_name);
        $Input['hostel_website'] = trim($request->hostel_website);
        $Input['hostel_location'] = trim($request->hostel_location);
        $Input['number_of_beds'] = trim($request->number_of_beds);
        //$Input['active_code'] = strtolower(Str::random(30));
        $Input['active_code'] = rand(1000,9999);
        $Input['is_invited'] = "0";
        $Input['invited_from'] = "";
        $Input['status'] = "1";
        $Input['isactivation_complete'] = '0';
        $Input['is_deleted'] = '0';
        $Input['role'] = trim($request->role);
        $Input['device_token'] = trim($request->device_token);
         $Input['created_at'] = date('Y-m-d h:i:s');

        $Input['token'] = sha1(time());
          
        $auth_user = User::create($Input); 
        
        if($auth_user) {
            //$email = $request->email;
            $user = User::where('id', $auth_user->id)->first()->toArray();
            $from_email = env('MAIL_FROM_ADDRESS');
            $name = $user['owner_name'];
            $token = $user['token'];
            $email = $request->email;
            $active_code = $user['active_code'];
          $sendemail = Mail::to($email)->send(new SendActivationCode($name,$email,$active_code));

          $notify =  [];
          $notify['notification_type'] = "registered";
          $notify['to_user'] = '1';
          $notify['from_user'] = $auth_user->id;
          $notify['icon'] = "new_registration.png";
          $notify['message'] = "New User Registered Successfully";
          $notify['is_read'] = "0";

          $ndata = Notification::create($notify);

          $touserdata = User::where('id', $ndata->to_user)->first();

          $to_token = $touserdata->device_token;

          $title = 'Registration';
          $type = $ndata->notification_type;
          $message = $ndata->message;


          $notification = array('title' =>$title , 'text' => $message);

          $fields = array(
              'to' => $to_token,
              'data' => array(
                  "type" => $type,
                  "title" => $title,
                  "message" => $message,
                  "notification_id" => $ndata->id,
                  "user_id" => $auth_user->id
              ),
              'notification' => $notification,
              'priority' => 'high',
          );
           

          $notificationClass = new FirebasenotificationClass();
          $notification_response = $notificationClass->sendnotification($fields);

          return response()->json(['status'=>true,'message'=>'User created successfully. Activation Code Is Sent To Your Email to Activate Your Account.','error'=>'','token'=>$token,'data'=>$user,'notification_data' => $notification_response]);
        	
        	//return response()->json(['status'=>true,'message'=>'Registered Successfully','data'=>$auth_user]);
        } else {
        	return response()->json(['status'=>false,'message'=>'Registration Failed! Something Went wrong','error'=>$validator->errors(),'data'=>'']);
        }

    }

    public function login(request $request) {
     
       

      $login_type = filter_var($request->email, FILTER_VALIDATE_EMAIL ) 
      ? 'email' 
      : 'username';
       

     
      $request->merge([ $login_type => $request->email ]);
      $credentials = $request->only($login_type, 'password');

      if ($token = $this->guard()->attempt($credentials)) {
        $auth_user = $this->guard()->user();
        if($auth_user->isactivation_complete == 1) {
          $id = $auth_user->id;
          $Input =  [];
          $Input['device_token'] = $request->device_token;
          $Input['updated_at'] = date('Y-m-d h:i:s');
          $user_data = User::where('id', $id)->update($Input);

        return response()->json(['status'=>true,'message'=>'User login successfully','error'=>'','token'=>$token,'data'=>$auth_user]);
      } else {
        return response()->json(['status'=>false,'message'=>"",'is_activate'=>'0']);
      }
          
      } else{
        return response()->json(['status'=>false,'message'=>'Your Username or Password is Incorrect!','error'=>'Invalid credentials','is_activate'=>'1','data'=>'']);

      }
    
      
      /*$data = User::WHERE('email',$user_email)->WHERE('password',$password)->first();
        
      if($data) {
        
        return response()->json(['status'=>true,'message'=>'Login Successfully','data'=>$data]);
      } else {
        return response()->json(['status'=>false,'message'=>'Login Failed! Something Went wrong','error'=>$validator->errors(),'data'=>'']);
      }*/
       

    }

    public function logout()
    {   
        $auth_user = $this->guard()->user();
        //User::where('id', $auth_user->id)->update(['isonline' => '0']);
        //Loginhistory::where('id', $auth_user->llhid)->update(['logout_time' => Carbon::now()]);
        $this->guard()->logout();
        return response()->json(['status'=>true,'message'=>'Successfully logged out','error'=>'','data'=>''], 200);
    }

    public function userActivation(Request $request){
        try{

            $email = $request->email;
            $active_code = $request->active_code;
            $user = User::where('email',$email)->where('active_code',$active_code)->first();
            if(!empty($user)){
              $Input =  [];
              $Input['isactivation_complete'] = '1';
              $Input['updated_at'] = date('Y-m-d h:i:s');
              $Input['active_code'] = "";
              $update = $user->update($Input);
              if($update){
                $user_data = User::where('id', $user->id)->first()->toArray();
                /*$data['username'] = $user['owner_name'];
                $data['message'] = 'Your Account has been Successfully Activated.';
                return view('useractivate',$data); */

                return response()->json(['status'=>true,'message'=>'Your Account has been Successfully Activated.','data'=>$user_data]); 
             } else {
                /*$data['username'] = $user['owner_name'];
                $data['message'] = 'Your Account has Not been activated';
                return view('useractivate',$data); */
                return response()->json(['status'=>false,'message'=>'Activation Code Not Matched! So,Your Account has Not been activated.']); 
            }

        } else {
                /*$data['username'] = 'User';
                $data['message'] = 'Sorry! User Does not Exist.';
                return view('useractivate',$data);*/
                return response()->json(['status'=>false,'message'=>'Sorry! User Does not Exist.']);

       }

    } catch(\Illuminate\Database\QueryException $e) {
        $errorClass = new ErrorsClass();
        $errors = $errorClass->saveErrors($e);
        return response()->json(['status'=>false,'message'=>'','error'=>'Sql query error','data'=>''], 401); 
    } catch(\Exception $e) {
        $errorClass = new ErrorsClass();
        $errors = $errorClass->saveErrors($e);
        return response()->json(['status'=>false,'message'=>'','error'=>'Undefined variable error','data'=>''], 401);
    }

    }

    public function forgetpassword(Request $request)
    {  
     
          $user_email = $request->email;
          $user_data = User::WHERE('email', $user_email)->WHERE('status', '1')->WHERE('is_deleted','0')->first();
          if(!empty($user_data)){
            $name = $user_data->owner_name;
            $email=$user_email;
            $otp = rand(1000,9999); 
            $sendemail = Mail::to($email)->send(new ForgetPassword($name,$email,$otp));

            $Input =  [];
            $Input['otp'] = $otp;
            $Input['updated_at'] = date('Y-m-d h:i:s');
            $update = $user_data->update($Input);

            return response()->json(['status'=>true,'message'=>'Reset Password Code For Change Password Sent to Your Email!','data'=>$sendemail]); 
        } else {
            return response()->json(['status'=>false,'message'=>'Your email is not on our data base. Please registerd your account clicking on the Sign Up button.']);
        }
     
    }

    public function checkforgetpasswordotp(Request $request)
    {  
     
          $user_email = $request->email;
          $otp = $request->otp;
          $user_data = User::WHERE('email', $user_email)->WHERE('status', '1')->WHERE('is_deleted','0')->first();
          if(!empty($user_data)){
            if($user_data->otp == $otp) {
                $Input =  [];
                $Input['otp'] = "";
                $Input['updated_at'] = date('Y-m-d h:i:s');
                $update = $user_data->update($Input);
                return response()->json(['status'=>true,'message'=>'Reset Password Coe Matched']);
            } else {
                return response()->json(['status'=>false,'message'=>'Reset Password Code Mismatched']);
            }
         
        } else {
            return response()->json(['status'=>false,'message'=>'User does not exist!']);
        }
     
    }

    public function Resetpassword(Request $request)
    {  
        

        $rules = [
            // 'username' => 'unique:users',
            'email' => 'required|email',
            'password'=>'required|confirmed',
            'password_confirmation'=>'',
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) 
        { 
            return response()->json(['status'=>false,'message'=>'Please enter correct details!','error'=>$validator->errors(),'data'=>'']);
        }
        
       /* $user_data = User::WHERE('email', $user_email)->WHERE('status', '1')->WHERE('is_deleted','0')->first();
        if (!Hash::check($request->old_password, $user_data->password)) {
            //return back()->with('error', 'The specified password does not match the database password');
            return response()->json(['status'=>false,'message'=>'The specified password does not match the database password']);
        }*/

        /*if($request->has('old_password')){
          $user_email = $request->email;
          $old_password = $request->old_password;
          $user_data = User::WHERE('email', $user_email)->WHERE('status', '1')->WHERE('is_deleted','0')->first();
          $user_old_password = trim(Hash::make($old_password));
          if($user_data->password != $user_old_password) {
            return response()->json(['status'=>false,'message'=>'Old Password Mismatch!']);
          }
             
        } */

        try{
          $user_email = $request->email;
          $new_password = $request->password;
          $user_dcrypt_password = trim(Hash::make($new_password));
          $update_data = User::where('email', $user_email)->where('is_deleted','0')->update(['password' => $user_dcrypt_password]);
          if($update_data){
            $user_data = User::WHERE('email', $user_email)->first();
            $user_name = $user_data->owner_name;
            $sendemail = Mail::to($user_email)->send(new Sendresetpassword($user_email, $user_name));
            return response()->json(['status'=>true,'message'=>'Your New password has been set.']); 
        } else {
            return response()->json(['status'=>false,'message'=>'Record Not found!']);
        }
    }
     catch(\Illuminate\Database\QueryException $e) {
        $errorClass = new ErrorsClass();
        $errors = $errorClass->saveErrors($e);
        return response()->json(['status'=>false,'message'=>'','error'=>'Sql query error','data'=>''], 401); 
    } catch(\Exception $e) {
        $errorClass = new ErrorsClass();
        $errors = $errorClass->saveErrors($e);
        return response()->json(['status'=>false,'message'=>'','error'=>'Undefined variable error','data'=>''], 401);
    }  
    /*$data['title'] = 'Reset Password';
    return view('passwords.reset', $data);  */

    }

    public function passwordReset(Request $request){
      $rules = [
                // 'username' => 'unique:users',
                'email' => 'required|email',
                'password'=>'required|confirmed',
                'password_confirmation'=>'',
            ];

            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) 
            { 
                return response()->json(['status'=>false,'message'=>'Please enter correct details!','error'=>$validator->errors(),'data'=>'']);
            }
          $user_email = $request->email;
           $user_data = User::WHERE('email', $user_email)->WHERE('status', '1')->WHERE('is_deleted','0')->first();
            if (!Hash::check($request->old_password, $user_data->password)) {
                return response()->json(['status'=>false,'message'=>'The Old password does not match.']);
            } else if($request->old_password == $request->password && strlen($request->old_password) == strlen($request->password)) {
              return response()->json(['status'=>false,'message'=>'The New password match with old password']);
            }

            try{
              //$user_email = $request->email;
              $new_password = $request->password;
              $user_dcrypt_password = trim(Hash::make($new_password));
              $update_data = User::where('email', $user_email)->where('is_deleted','0')->update(['password' => $user_dcrypt_password]);
              if($update_data){
                $user_data = User::WHERE('email', $user_email)->first();
                $user_name = $user_data->owner_name;
                $sendemail = Mail::to($user_email)->send(new Sendresetpassword($user_email, $user_name));
                return response()->json(['status'=>true,'message'=>'Your New password has been set.']); 
            } else {
                return response()->json(['status'=>false,'message'=>'Record Not found!']);
            }
        }
         catch(\Illuminate\Database\QueryException $e) {
            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);
            return response()->json(['status'=>false,'message'=>'','error'=>'Sql query error','data'=>''], 401); 
        } catch(\Exception $e) {
            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);
            return response()->json(['status'=>false,'message'=>'','error'=>'Undefined variable error','data'=>''], 401);
        }  
       
    }

    public function getallusers(request $request){
        
        $user = User::where('is_deleted','0')->where('role','2')->orwhere('role','3')->where('status','1')->orderBy('id', 'DESC')->get();
        //$user = User::where('is_deleted','0')->where('status','1')->orderBy('id', 'DESC')->get();
        if(!empty($user)){
             return response()->json(['status'=>true,'message'=>'Users Details','data'=>$user]); 
        }
        else{
            return response()->json(['status'=>false,'message'=>'Users Details Not Found','data'=>'']);
        }
    }

    /*public function getcurrentuser(){
        $user = Auth::user();
        //$user = $this->guard()->user();
        return $user;
    }*/

    public function getuserDetails(Request $request, $id) {
    
      $isuser = User::find($id);
      
      if($isuser) {
        //$product = Product::where('id',$id)->where('product_status','1')->where('is_deleted','0')->get();
        return response()->json(['status'=>true,'message'=>'User details','error'=>'','data'=>$isuser], 200);
      } else {
        return response()->json(['status'=>false,'message'=>'User not found','error'=>'','data'=>''], 400);
      }
    }

    public function updateuserDetails(Request $request, $id) {
      
      $isuser = User::find($id);

      //$role = $isuser->role;

        $rules = [
            'name' => 'required',
            'email' => 'required|email',
            'phone' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) 
        { 
            return response()->json(['status'=>false,'message'=>'Request Failed','error'=>$validator->errors(),'data'=>'']);
        }
       

        $Input =  [];
        $Input['owner_name'] = trim($request->name);
        $Input['email'] = trim($request->email);
        $Input['phone_code'] = trim($request->phone_code);
        $Input['phone_number'] = trim($request->phone);
        $Input['address'] = trim($request->address);
        $Input['city'] = trim($request->city);
        $Input['state'] = trim($request->state);
        $Input['zipcode'] = trim($request->zipcode);
        $Input['gender'] = trim($request->gender);
        $Input['dob'] = trim($request->dob);

        $Input['hostel_name'] = trim($request->hostel_name);
        $Input['hostel_website'] = trim($request->hostel_website);
        $Input['hostel_location'] = trim($request->hostel_location);
        $Input['number_of_beds'] = trim($request->number_of_beds);
        $Input['updated_at'] = date('Y-m-d h:i:s');

        if($request->hasfile('user_img')) {
             $file = $request->file('user_img');
            $filename = $request->file('user_img')->getClientOriginalName();
       
       //Move Uploaded File
           $destinationPath = 'uploads/images';
            if(!file_exists( 'uploads/images/'.$filename )) {
              $file->move($destinationPath,$file->getClientOriginalName());
            }
           //$file->move($destinationPath,$file->getClientOriginalName());
           $Input['user_img'] = $filename;
        }


       $user_data = User::where('id', $id)->update($Input);
       $user_detail = User::where('id', $id)->first()->toArray();
       if($user_detail) {
            //$from_email = env('MAIL_FROM_ADDRESS');
            //$email = $request->email;

        return response()->json(['status'=>true,'message'=>'Your Account Updated Successfully.','data'=>$user_detail]);
        } else {
        return response()->json(['status'=>false,'message'=>'User Not Updated! Something Went wrong','error'=>$validator->errors(),'data'=>'']);
        } 
      
    }

    public function deleteuser(request $request,$id)
    {
        $isuser = User::find($id);
        if($isuser) {

        $user = User::where('id',$id)->where('status','1')->where('is_deleted','0')->get();
        if($user) {
            $Input =  [];
            $Input['is_deleted'] = "1";

           $user_data = User::where('id', $id)->update($Input);
           $user_detail = User::where('id', $id)->first()->toArray();
           if($user_detail) {
                //$from_email = env('MAIL_FROM_ADDRESS');
                //$email = $request->email;

            return response()->json(['status'=>true,'message'=>'User Deleted Successfully','data'=>$user_detail]);
            } else {
            return response()->json(['status'=>false,'message'=>'User Not Deleted! Something Went wrong','error'=>$validator->errors(),'data'=>'']);
            } 

        
        } 
        
      } else {
        return response()->json(['status'=>false,'message'=>'User not found','error'=>'','data'=>''], 400);
      }
        
    }

    public function searchuser(request $request)
    {
        $srch_user = trim($request->srch_user);
       
        $user_details = User::where('owner_name','LIKE','%'.$srch_user.'%')->where('status','1')->where('is_deleted','0')->orderBy('id', 'DESC')->get();
         if($user_details) {
            return response()->json(['status'=>true,'message'=>'Searched Users','data'=>$user_details]);
        } else {
             return response()->json(['status'=>false,'message'=>'No User found!','data'=>'']);
        }
        
        
        
    }

    public function getallhostelowners(request $request)
    {
       
        $user_details = User::where('role','2')->where('isactivation_complete','1')->where('status','1')->where('is_deleted','0')->orderBy('owner_name', 'ASC')->get();
         if($user_details) {
            return response()->json(['status'=>true,'message'=>'Hostel Owners','data'=>$user_details]);
        } else {
             return response()->json(['status'=>false,'message'=>'Hostel Owner not found!','data'=>'']);
        }
        
        
        
    }

    public function addsplash(request $request)
    {
        $Input =  [];
        $Input['splash_userid'] = trim($request->splash_userid);
        $Input['splash_appname'] = trim($request->splash_appname);
       
        $Input['created_at'] = date('Y-m-d h:i:s');
        
        if ($request->hasfile('splash_img')) {
            $file = $request->file('splash_img');
            $filename = $request->file('splash_img')->getClientOriginalName();
       // Get the contents of the file
       //$contents = $file->openFile()->fread($file->getSize());

       // Store the contents to the database
            $Input['splash_img'] = $filename;

        
   
       //Move Uploaded File
            $destinationPath = 'uploads/images';
            if(!file_exists( 'uploads/images/'.$filename )) {
              $file->move($destinationPath,$file->getClientOriginalName());
            }
        
        }
       

       $splash_data = Splash::create($Input);
       

         if($splash_data) {
            return response()->json(['status'=>true,'message'=>'Splash Added Successfully!','data'=>$splash_data]);
        } else {
             return response()->json(['status'=>false,'message'=>'Splash not Added Successfully!','data'=>'']);
        }
    
    }

    public function updatesplash(request $request)
    {
        $id = $request->id;
        $issplash = Splash::find($id);

        if($issplash) {
        $issplash->splash_userid = $request->splash_userid;
        $issplash->splash_appname = $request->splash_appname;
        

        // Check if a profile image has been uploaded
        if ($request->hasfile('splash_img')) {
             $file = $request->file('splash_img');
            $filename = $request->file('splash_img')->getClientOriginalName();
       
       //Move Uploaded File
           $destinationPath = 'uploads/images';
            if(!file_exists( 'uploads/images/'.$filename )) {
              $file->move($destinationPath,$file->getClientOriginalName());
            }
           //$file->move($destinationPath,$file->getClientOriginalName());
           $issplash->splash_img = $filename;
        } 
        // Persist user record to database
        $issplash->save();

        return response()->json(['status'=>true,'message'=>'Splash Updated Successfully','data'=>$issplash]);
      } else {
        return response()->json(['status'=>false,'message'=>'Splash Not Found','data'=>'']);

      }
        
        
    }
   


    public function guard()
    {
        return Auth::guard();
    }
}
